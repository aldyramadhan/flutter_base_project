import 'package:core/local/shared_pref_helper.dart';
import 'package:core/network/dio_interceptor.dart';
import 'package:core/network/network.dart';
import 'package:dependencies/dependencies.dart';
import 'package:flutter/material.dart';

class CoreModule extends ChildModule {
  final SharedPreferences preferences;

  CoreModule({@required this.preferences});

  @override
  List<Bind> get binds => [
    Bind((_) => ApiConstant()),
    Bind((_) => preferences),
    Bind((_) => Modular.get<DioInterceptor>().dio),
    Bind((_) => DioInterceptor()),
    Bind((_) => SharedPrefHelper(preferences: Modular.get<SharedPreferences>())),
    Bind((_) => SharedPreferences.getInstance()),

  ];

  @override
  List<ModularRouter> get routers => [];
}