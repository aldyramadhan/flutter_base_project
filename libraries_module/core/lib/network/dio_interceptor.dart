import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:dependencies/dependencies.dart';
import 'package:checker/ui/ui.dart';
import 'package:flutter/widgets.dart';
import 'package:shared/common/base/base.dart';
import 'package:shared/widget/widget.dart';

class DioInterceptor with BaseView {
  BuildContext context;

  DioInterceptor({this.context});

  Dio get dio => _getDio();

  static Alice alice =
      Alice(showNotification: true, darkTheme: true, navigatorKey: Modular.navigatorKey);

  Dio _getDio() {
    BaseOptions options = BaseOptions(
      baseUrl: Config.baseUrl,
      connectTimeout: 50000,
      receiveTimeout: 30000,
    );
    Dio dio = Dio(options);
    dio.interceptors.add(alice.getDioInterceptor());
    dio.interceptors.addAll(<Interceptor>[_loggingInterceptor()]);

    return dio;
  }

  Interceptor _loggingInterceptor() {
    return InterceptorsWrapper(onRequest: (RequestOptions options) async{
      options.headers['Authorization'] = 'Bearer ${await pref.accessToken()}';

      // Do something before request is sent
      debugPrint("\n"
          "Request ${options.uri} \n"
          "-- headers --\n"
          "${options.headers.toString()} \n"
          "");

      return options; //continue
      // If you want to resolve the request with some custom data，
      // you can return a `Response` object or return `dio.resolve(data)`.
      // If you want to reject the request with a error message,
      // you can return a `DioError` object or return `dio.reject(errMsg)`
    }, onResponse: (Response response) {
      // Do something with response data
      if (response.statusCode == 200) {
        debugPrint("\n"
            "Response ${response.request.uri} \n"
            "-- headers --\n"
            "${response.headers.toString()} \n"
            "-- payload --\n"
            "${jsonEncode(response.data)} \n"
            "");
      } else {
        debugPrint("Dio Response Status --> ${response.statusCode}");
        _showSnackbar(
            "Dio Response Status --> ${response.statusCode}", "Dio Response Status");
      }
      return response; // continue
    }, onError: (DioError e) {
      // Do something with response error
      debugPrint("Dio Response Error --> $e");
      _showSnackbar("Dio Response Error --> $e", "Dio Response Error");
      return e; //continue
    });
  }

  void _showSnackbar(String message, String header) {
    SimpleNotificationSnackbar(
        duration: Duration(seconds: 6),
        dispose: true,
        child: Card(
          elevation: 2,
          margin: EdgeInsets.only(left: 8, right: 8),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8),
          ),
          color: colorPalettes.whiteSemiTransparent,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                height: 35,
                padding: EdgeInsets.only(left: 6),
                decoration: BoxDecoration(
                    color: colorPalettes.whiteSemiTransparent2,
                    borderRadius: BorderRadius.only(
                        topLeft: const Radius.circular(8),
                        topRight: const Radius.circular(8))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Image.asset(
                      imageAsset.appLogo,
                      width: 20,
                      height: 20,
                    ),
                    SizedBox(width: 8,),
                    Text(header)
                  ],
                ),
              ),
              Container(
                  padding: EdgeInsets.all(8),
                  constraints: BoxConstraints(
                      minHeight: 40
                  ),
                  child: Text(message)
              )
            ],
          ),
        )).show();
  }
}
