class ApiConstant {

  // -- BASE URL
  static String baseUrlDevelopmentApi = "https://private-21f5c-chekrapp.apiary-mock.com/api/";
  static String baseUrlProductionApi = "https://cheker-dashboard.herokuapp.com/api/";
  static String baseUrlUatApi = "https://cheker-dashboard.herokuapp.com/api/";

  // -- KEY
  static const String language = "en-US";
  static const String apiKey = "601246c17babdf3de6d4b2db3214e9ce";

  // -- POST Method
  static String getStartedApi = "users/get_started";
  static String loginApi = "users/auth/login";
  static String loginByGoogleApi = "users/auth/login";
  static String loginByFacebookApi = "users/auth/login";
  static String registerApi = "users/auth/register";
  static String phoneVerificationApi = "users/auth/phone_verifikasi";
  static String forgotPasswoApi = "users/auth/forgot_password";
  static String resetPasswoApi = "users/auth/reset_password";

  // -- GET Method
  static String logoutApi = "users/logout";
  static String refreshTokenApi = "users/logout";
  static String homeApi = "users/home";

  // -- PUT Method

}
