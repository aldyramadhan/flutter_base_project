import 'package:shared/common/common.dart';

List profileMenuJson = [
  {
    "icon": "${ImageAssets().headphones}",
    "title": "Pusat Bantuan",
    "height": 24.0,
    "width": 24.0
  },
  {
    "icon": "${ImageAssets().rating}",
    "title": "Beri Rating",
    "height": 24.0,
    "width": 24.0
  },
  {
    "icon": "${ImageAssets().setting}",
    "title": "Pengaturan",
    "height": 24.0,
    "width": 24.0
  },
  {
    "icon": "${ImageAssets().email}",
    "title": "Verifikasi Email",
    "height": 16.0,
    "width": 24.0
  }
];
