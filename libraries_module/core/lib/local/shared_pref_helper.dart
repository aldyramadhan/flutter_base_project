import 'package:dependencies/dependencies.dart';
import 'package:flutter/material.dart';
import 'dart:convert';

import 'package:shared/common/common.dart';

class SharedPrefHelper {
  static const _isDarkTheme = "isDarkTheme";
  static const _isIndonesian = "isIndonesian";
  static const LAST_CHECKED = "last_checked";
  static const CHECK_INTERVAL = "check_interval";
  static const DATA = "data";
  SharedPreferences preferences;

  SharedPrefHelper({@required this.preferences});

  Future<bool> getValueDarkTheme() async {
    return preferences.getBool(_isDarkTheme) ?? false;
  }

  Future saveValueDarkTheme(bool value) async {
    preferences.setBool(_isDarkTheme, value);
  }

  Future<bool> getValueIndonesianLanguage() async {
    return preferences.getBool(_isIndonesian) ?? false;
  }

  Future saveValueIndonesianLanguage(bool value) async {
    preferences.setBool(_isIndonesian, value);
  }

  // Interval 600000 means handle cache for 600000 milliseconds or 10 minutes
  Future<bool> storeDynamicCache(String key, String json,
      {int lastChecked, int interval = 600000}) {
    if (lastChecked == null) {
      lastChecked = DateTime.now().millisecondsSinceEpoch;
    }
    return SharedPreferences.getInstance().then((prefs) {
      return prefs.setString(
          key,
          jsonEncode({
            LAST_CHECKED: lastChecked,
            CHECK_INTERVAL: interval,
            DATA: json
          }));
    });
  }

  Future<String> getDynamicCache(String key) async {
    Map map = jsonDecode(preferences.getString(key));
    // if outdated, clear and return null
    var lastChecked = map[LAST_CHECKED];
    var interval = map[CHECK_INTERVAL];
    if ((DateTime.now().millisecondsSinceEpoch - lastChecked) > interval) {
      preferences.remove(key);
      return null;
    }
    return map[DATA];
  }

  Future<Map> getFullCache(String key) async {
    Map map = jsonDecode(preferences.getString(key));
    // if outdated, clear and return null
    var lastChecked = map[LAST_CHECKED];
    var interval = map[CHECK_INTERVAL];
    if ((DateTime.now().millisecondsSinceEpoch - lastChecked) > interval) {
      preferences.remove(key);
      return null;
    }
    return map;
  }

  Future<bool> storeCache(String key, String json) {
    print('cache stored : key=$key - data=$json');
    return preferences.setString(key, jsonEncode({DATA: json}));
  }

  Future<String> getCache(String key) async {
    Map map = jsonDecode(preferences.getString(key));
    print('get cache : key=$key - data=${map[DATA]}');
    return map[DATA];
  }

  Future saveLogin(bool value) async {
    preferences.setBool(AppConstant.LOGIN, value);
  }

  Future saveToken(String value) async {
    preferences.setString(AppConstant.TOKEN, value);
  }

  Future<bool> isLogin() async => preferences.getBool(AppConstant.LOGIN) ?? false;

  Future<String> accessToken() async => preferences.getString(AppConstant.TOKEN) ?? null;

  Future<String> account() async {
    Map map = jsonDecode(preferences.getString(AppConstant.ACCOUNT));
    return map[DATA];
  }

  Future doLogout() async {
    preferences.remove(AppConstant.TOKEN);
    preferences.remove(AppConstant.LOGIN);
    preferences.remove(DATA);
  }
}