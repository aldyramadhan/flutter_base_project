import 'package:dependencies/dependencies.dart';
import 'package:shared/common/utils/json_assets.dart';
import 'package:shared/widget/form_field/my_text_field.dart';
import 'package:shared/widget/widget.dart';
import 'common/common.dart';

class SharedModule extends ChildModule {
  @override
  List<Bind> get binds => [
    Bind((_) => ImageAssets()),
    Bind((_) => AudioAssets()),
    Bind((_) => Audio()),
    Bind((_) => JsonAssets()),
    Bind((_) => WidgetStyles()),
    Bind((_) => LocaleKeys()),
    Bind((_) => NamedRoutes()),
    Bind((_) => Navigation()),
    Bind((_) => ColorPalettes()),
    Bind((_) => AppConstant()),
    Bind((_) => ThemeHelper()),
    Bind((_) => Validator()),
    Bind((_) => MyTextField(null, null)),
  ];

  @override
  List<ModularRouter> get routers => [];
}