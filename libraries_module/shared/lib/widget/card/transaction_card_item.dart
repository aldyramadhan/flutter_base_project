import 'package:dependencies/dependencies.dart';
import 'package:flutter/material.dart';
import 'package:shared/common/base/base.dart';
import 'package:shared/common/common.dart';

class TransactionCardItem extends StatelessWidget with BaseView {
  @override
  Widget build(BuildContext context) => Parent(
    style: styles.cardNormalStyle(colorPalettes.white, radius: 10),
    child: Padding(
      padding: EdgeInsets.all(16),
      child: Column(children: [
        Row(children: [
          CachedNetworkImage(
            imageUrl:
            "https://www.longchamp.com/dw/image/v2/BCVX_PRD/on/demandware.static/-/Sites-LC-master-catalog/default/dw48cfd7a3/images/DIS/L1899089556_0.png?sw=500&sh=500&sm=fit",
            width: 120,
            height: 120,
          ),
          SizedBox(width: 20),
          Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Txt("Tas Branded",
                  style: WidgetStyles().textNormalBoldStyle(colorPalettes.primaryText),
                ),
                Txt("Sedang dicek",
                  style: WidgetStyles().textSmallStyle(colorPalettes.textlightGray),
                ),
                SizedBox(height: 10),
                Txt("Tipe Transaksi",
                  style: WidgetStyles().textNormalBoldStyle(colorPalettes.primaryText),
                ),
                Txt("Pengecekan Barang",
                  style: WidgetStyles().textSmallStyle(colorPalettes.textlightGray),
                ),
                SizedBox(height: 12),
                Txt("Rp 700.00",
                  style: WidgetStyles().textNormalBoldStyle(colorPalettes.primaryText),
                ),
              ])
        ]),
        SizedBox(height: 16),
        Row(children: [
          SvgPicture.asset(ImageAssets().location,
            width: 14,
          ),
          SizedBox(width: 4,),
          Expanded(
              child: Txt("Jakarta",
                style: WidgetStyles().textSmallStyle(colorPalettes.secondaryText),
              )),
          GestureDetector(
            onTap: () {},
            child: Container(
                height: 22,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    color: ColorPalettes().lightPrimary,
                    borderRadius: BorderRadius.circular(100)),
                padding: EdgeInsets.only(left: 8, right: 8),
                child: Txt(AppConstant().monitor,
                  style: WidgetStyles().textTinyBoldStyle(colorPalettes.textWhite),
                )),
          )
        ])
      ])
    )
  );

}
