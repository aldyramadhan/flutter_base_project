import 'package:dependencies/dependencies.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared/common/base/base.dart';

class CategoryFilterCardItem extends StatelessWidget with BaseView{
  @override
  Widget build(BuildContext context) {
    return Parent(
      style: styles.cardNormalStyle(colorPalettes.grey12, radius: 10),
      child: Container(
        width: MediaQuery.of(context).size.width / 5.2,
        alignment: Alignment.bottomCenter,
        padding: EdgeInsets.all(8),
        child: Text(appConstant.category,
          style: TextStyle(fontSize: 12),
        )
      ),
    );
  }

}