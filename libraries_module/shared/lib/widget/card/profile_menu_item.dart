import 'package:dependencies/dependencies.dart';
import 'package:flutter/material.dart';
import 'package:shared/common/base/base.dart';

class ProfileMenuItem extends StatelessWidget with BaseView{
  final String image;
  final String title;
  final double height;
  final double width;

  ProfileMenuItem(
      {
        Key key,
        this.image,
        this.title,
        this.height,
        this.width
      }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        InkWell(
          onTap: (){},
          child: Container(
            height: 52,
            child: Row(
              children: [
                SizedBox(
                  height: height,
                  width: width,
                  child: SvgPicture.asset(image, fit: BoxFit.fill,),
                ),
                SizedBox(width: 12),
                Expanded(
                  child: Txt(title,
                    style: styles.textNormalBoldStyle(colorPalettes.primaryText),
                  ),
                ),
                SvgPicture.asset(imageAsset.arrowRight, width: 24, height: 24),
              ],
            ),
          ),
        ),
        Divider(height: .6)
      ],
    );
  }

}