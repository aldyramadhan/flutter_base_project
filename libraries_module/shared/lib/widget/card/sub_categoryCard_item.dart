import 'package:dependencies/dependencies.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared/common/base/base.dart';
import 'package:shared/widget/shimmer/shimmer_container.dart';

import '../widget.dart';

class SubCategoryCardItem extends StatelessWidget with BaseView{
  final Function detail;

  SubCategoryCardItem({Key key, this.detail}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Parent(
        style: styles.cardNormalStyle(colorPalettes.white, mV: 8, radius: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ClipRRect(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(12),
                    topRight: Radius.circular(12)
                ),
                child: SizedBox(
                    height: 150,
                    child: CachedNetworkImage(
                      imageUrl: getString("https://image.cermati.com/q_70,w_1200,h_800,c_fit/ltm2cjaldbigxtxltuv9"),
                      fit: BoxFit.fill,
                      width: double.infinity,
                      placeholder: (context, url) => ShimmerContainer(),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                    )
                )
            ),
            SizedBox(height: 10),
            Txt("Burung",
              style: styles.textSmallBoldStyle(colorPalettes.primaryText),
            ),
            SizedBox(height: 30),
            SizedBox(
                height: 22,
                child: SimpleButton(
                  onPress: detail,
                  buttonColor: colorPalettes.lightPrimary,
                  buttontype: 1,
                  borderColor: colorPalettes.lightPrimary,
                  child: Txt(appConstant.detail.toUpperCase(),
                    style: styles.textTinyStyle(colorPalettes.lightPrimary),
                  ),
                  borderRadius: 100,
                )),
          ],
        ),
    );
  }

}