import 'package:dependencies/dependencies.dart';
import 'package:flutter/material.dart';
import 'package:shared/common/base/base.dart';
import 'package:shared/widget/shimmer/shimmer_container.dart';

class CardItem extends StatelessWidget with BaseView{
  final String imgUrl;
  final String title;
  final String titleDesc;
  final bool withBorder;

  CardItem({
    Key key,
    this.imgUrl,
    this.title,
    this.titleDesc,
    this.withBorder = false
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Parent(
      style: withBorder
          ? styles.cardBorderNormalStyle(colorPalettes.white, colorPalettes.grey12)
          : styles.cardNormalStyle(colorPalettes.white),
      child: Container(
          width: MediaQuery.of(context).size.width / 2.5,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _image,
              SizedBox(height: 10.h),
              _title,
              SizedBox(height: 6.h),
              _titleDesc
            ],
          ),
      ),
    );
  }

  Widget get _image => ClipRRect(
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(12), topRight: Radius.circular(12)
      ),
      child: SizedBox(
          height: 124,
          child: CachedNetworkImage(
            imageUrl: getString(imgUrl),
            fit: BoxFit.fill,
            width: double.infinity,
            placeholder: (context, url) => ShimmerContainer(),
            errorWidget: (context, url, error) => Icon(Icons.error),
          )
      ));

  Widget get _title => Padding(
    padding: EdgeInsets.symmetric(horizontal: 8),
    child: Txt(
        getString(title),
        style: styles.textMediumBoldStyle(colorPalettes.primaryText, mL: 2),
      ),
  );

  Widget get _titleDesc => Padding(
    padding: EdgeInsets.symmetric(horizontal: 8),
    child: Txt(
      getString(titleDesc),
      style: styles.textNormalStyle(colorPalettes.textlightGray, mL: 1),
    ),
  );

}