import 'package:dependencies/dependencies.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared/common/base/base.dart';

class CategoryCardItem extends StatelessWidget with BaseView{
  final Function seeAll;

  CategoryCardItem({Key key, this.seeAll}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Parent(
      style: styles.cardNormalStyle(colorPalettes.white, mV: 8),
      child: Padding(
        padding: EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Expanded(
                    child: Txt("Burung",
                      style: styles.textNormalBoldStyle(colorPalettes.primaryText),
                    )
                ),
                GestureDetector(
                  onTap: seeAll,
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 3, horizontal: 5),
                    child: Txt(appConstant.seeAll,
                      style: styles.textTinyStyle(colorPalettes.lightPrimary),
                    ),
                  ),
                )
              ],
            ),
            SizedBox(height: 3),
            Txt(appConstant.checkYourExpectation,
              style: styles.textTinyStyle(colorPalettes.secondaryText),
            ),
            SizedBox(height: 10),
            Container(
              height: 140,
              color: colorPalettes.lightPrimary,
            )
          ],
        ),
      )
    );
  }

}