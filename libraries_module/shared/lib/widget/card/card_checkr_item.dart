import 'package:dependencies/dependencies.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared/common/base/base.dart';

class CardCheckrItem extends StatelessWidget with BaseView{
  final String image;
  final String title;
  final double imageWidth;
  final double imageHeight;

  CardCheckrItem({Key key,
    this.image,
    this.title,
    this.imageWidth = 28,
    this.imageHeight = 28
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Parent(
    style: styles.cardBorderNormalStyle(
      colorPalettes.white,
      colorPalettes.grey12,
      mH: 6, mV: 5
    ),
    child: Container(
      width: double.infinity,
      padding: EdgeInsets.all(12),
      child: Wrap(
        alignment: WrapAlignment.start,
        direction: Axis.horizontal,
        children: [
          Container(
            height: imageHeight,
            margin: EdgeInsets.only(bottom: 12),
            child: ClipRRect(
                borderRadius: BorderRadius.circular(90),
                child: CachedNetworkImage(imageUrl: image,
                  fit: BoxFit.fill, width: imageWidth, height: imageHeight,
                )),
          ),
          Text(title,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
          )
        ],
      ),
    ),
  );

}