import 'package:dependencies/dependencies.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared/common/base/base.dart';
import 'package:shared/common/common.dart';

class AppbarHome extends StatelessWidget with BaseView {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: colorPalettes.lightPrimary,
      padding: EdgeInsets.all(14),
      child: Row(
        children: [
          Expanded(child: _searchBar()),
          SizedBox(width: 18),
          GestureDetector(
            onTap: () => navigation.to(Modular.get<NamedRoutes>().notificationPage.home),
            child: SvgPicture.asset(imageAsset.notification, width: 22,),
          ),
          SizedBox(width: 18),
          GestureDetector(
            onTap: () {} ,
            child: SvgPicture.asset(imageAsset.help, width: 22,),
          )
        ],
      ),
    );
  }

  Widget _searchBar() {
    return GestureDetector(
      onTap: () => navigation.to(Modular.get<NamedRoutes>().searchPage.home),
      child: Container(
        height: 40,
        padding: EdgeInsets.symmetric(horizontal: 14),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(56)),
            border: Border.all(color: colorPalettes.white, width: 2)),
        child: Row(
          children: [
            Expanded(
                child: Text(
                  "Cek aja dulu...",
                  style: TextStyle(fontSize: 14, color: colorPalettes.white),
                )),
            SvgPicture.asset(
              imageAsset.search,
              color: colorPalettes.white,
              width: 18,
            ),
          ],
        ),
      ),
    );
  }
}
