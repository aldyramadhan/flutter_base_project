import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SimpleAppBar extends StatelessWidget {
  final Widget title;
  final Color color;
  final Widget leading;
  final double elevation;
  final double height;
  final bool centerTitle;

  const SimpleAppBar(
      {
        Key key,
        this.title,
        this.color,
        this.leading,
        this.elevation = 0,
        this.height,
        this.centerTitle = false
      })
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: centerTitle,
      elevation: this.elevation,
      backgroundColor: color,
      toolbarHeight: height,
      title: title,
      leading: this.leading,
    );
  }
}
