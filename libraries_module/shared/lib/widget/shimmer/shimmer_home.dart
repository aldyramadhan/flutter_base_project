import 'package:dependencies/dependencies.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared/common/base/base.dart';

import 'shimmer_container.dart';

class ShimmerHome extends StatelessWidget with BaseView {
  @override
  Widget build(BuildContext context) => Container(
      width: double.infinity,
      padding: EdgeInsets.only(left: 16, right: 16, top: 16),
      decoration: BoxDecoration(
          color: colorPalettes.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20), topRight: Radius.circular(20)
          )
      ),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _boxText(MediaQuery.of(context).size.width / 2.6),
            Expanded(
                child: Container(
                  margin: EdgeInsets.only(top: 12, bottom: 12),
                  child: ShimmerContainer()
                )
            ),
            _boxText(MediaQuery.of(context).size.width / 2),
            Expanded(
                child: Container(
                  margin: EdgeInsets.only(top: 12, bottom: 6),
                    child: ShimmerContainer()
                )
            ),
          ]
      )
    );

  Widget _boxText(double width) => Container(
    height: 20,
    width: width,
    child: ShimmerContainer(),
  );

}
