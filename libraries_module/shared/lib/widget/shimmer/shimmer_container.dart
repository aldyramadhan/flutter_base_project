import 'package:dependencies/dependencies.dart';
import 'package:flutter/material.dart';
import 'package:shared/common/base/base.dart';

class ShimmerContainer extends StatelessWidget with BaseView{

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10))),
        child: Shimmer.fromColors(
            baseColor: Colors.grey[300],
            highlightColor: Colors.grey[100],
            child:  Container(
              decoration: BoxDecoration(
                  color: colorPalettes.fieldBackground,
                  borderRadius: BorderRadius.all(Radius.circular(10))),
            ))
    );
  }

}