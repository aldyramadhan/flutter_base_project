import 'package:dependencies/dependencies.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared/common/base/base.dart';

class EmptyData extends StatelessWidget with BaseView{
  final String image;
  final String title;
  final String desc;

  EmptyData({Key key, this.image, this.title, this.desc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        padding: EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(image, height: 160),
            SizedBox(height: 40),
            Txt(title,
              style: styles.textLargeStyle(colorPalettes.primaryText, bold: true),
            ),
            SizedBox(height: 6),
            Txt(desc,
              style: styles.textSmallStyle(colorPalettes.secondaryText),
            )
          ],
        ),
      ),
    );
  }

}