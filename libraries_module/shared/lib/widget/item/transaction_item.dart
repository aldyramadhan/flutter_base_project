import 'package:dependencies/dependencies.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared/common/base/base.dart';

class TransactionItem extends StatelessWidget with BaseView{
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Expanded(
                    child: Txt(appConstant.transactionHistory,
                      style: styles.textMediumBoldStyle(colorPalettes.primaryText),
                    ),
                  ),
                  Text("Menunggu pembayaran",
                    style: TextStyle(fontSize: 12, color: colorPalettes.yellow),
                  ),
                ],
              ),
              SizedBox(height: 6),
              Row(
                children: [
                  SvgPicture.asset(imageAsset.location, color: colorPalettes.lightPrimary),
                  SizedBox(width: 6),
                  Txt('Jakarta',
                    style: styles.textSmallBoldStyle(colorPalettes.secondaryText),
                  ),
                ],
              ),
              SizedBox(height: 6),
              Row(
                children: [
                  Expanded(
                    child: Txt('Order ID #12143563124',
                      style: styles.textSmallBoldStyle(colorPalettes.secondaryText),
                    ),
                  ),
                  Txt('14 Nov 2020, 16.00',
                    style: styles.textSmallBoldStyle(colorPalettes.textlightGray),
                  ),
                ],
              ),
              SizedBox(width: 6),
            ],
          ),
        ),
        Divider(height: .6)
      ],
    );
  }

}