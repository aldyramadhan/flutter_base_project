import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared/common/base/base.dart';

class BaseBottomSheet extends BaseView {
  final Widget child;
  final bool isScrollController;
  final BuildContext context;
  final double topLeft;
  final double bottomLeft;
  final double topRight;
  final double bottomRight;

  BaseBottomSheet(
      {this.child,
      this.isScrollController,
      this.context,
      this.topLeft = 0,
      this.bottomLeft = 0,
      this.topRight = 0,
      this.bottomRight = 0});

  void showBaseBottomSheet() {
    showModalBottomSheet(
        context: this.context,
        isScrollControlled: this.isScrollController,
        backgroundColor: Colors.transparent,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(topLeft),
              topRight: Radius.circular(topRight),
              bottomLeft: Radius.circular(bottomLeft),
              bottomRight: Radius.circular(bottomRight)),
        ),
        builder: (BuildContext context) => SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.only(
                bottom: MediaQuery.of(context).viewInsets.bottom),
            child: child,
          ),
        ));
  }
}
