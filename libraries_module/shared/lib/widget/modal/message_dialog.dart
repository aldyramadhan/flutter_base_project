import 'package:dependencies/dependencies.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared/common/base/base.dart';

class MessageDialog extends StatelessWidget with BaseView{
  final String title;
  final String message;

  MessageDialog({Key key, this.title, this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      elevation: 0,
      child: _content(context),
    );
  }

  Widget _content(context) => Container(
    padding: EdgeInsets.all(12),
    child: Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          child: title != null ? _titleWording : null,
        ),
        SizedBox(height: 6),
        Container(
          child: message != null ? _messageWording : null,
        ),
        SizedBox(height: 16),
        _dismisButton
      ],
    ),
  );

  Widget get _titleWording => Txt(
    getString(title),
    style: styles.textMediumBoldStyle(colorPalettes.lightPrimary),
  );

  Widget get _messageWording => Txt(
    getString(message),
    style: styles.textNormalStyle(colorPalettes.primaryText),
  );

  Widget get _dismisButton => Txt(
    appConstant.ok,
    gesture: buttonTap..onTap(() => navigation.toPop()),
    style: styles.buttonStyle(true, color: colorPalettes.lightPrimary, radius: 10),
  );
}