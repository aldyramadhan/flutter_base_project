import 'package:flutter/material.dart';
import "package:intl/intl.dart";

class Validator {
  bool eightChars = false;
  bool specialChar = false;
  bool upperCaseChar = false;
  bool number = false;

  static FormFieldValidator<String> emailValidator({String errorMessage}) {
    return (text) {
      bool emailValid = RegExp(
              r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)")
          .hasMatch(text);

      if (!emailValid) {
        errorMessage = errorMessage == null
            ? "Bidang ini harus dalam format email"
            : errorMessage;
        return errorMessage;
      }
      return null;
    };
  }

  static FormFieldValidator<String> numberOnlyValidator({String errorMessage}) {
    return (text) {
      bool isValid = RegExp(r"^[0-9]*$").hasMatch(text);
      if (!isValid) {
        errorMessage = errorMessage == null
            ? "Bidang ini hanya menerima karakter numerik"
            : errorMessage;
        return errorMessage;
      }
      return null;
    };
  }

  static FormFieldValidator<String> numberPhoneValidator(
      {String errorMessage,
      int minLength,
      maxLength,
      String minLengthMessage}) {
    return (text) {
      bool isValid = RegExp(r"^([8])([0-9])*$").hasMatch(text);
      if (!isValid) {
        errorMessage = errorMessage == null
            ? "Tidak valid (Mis: 8xxxxxxxxxx)"
            : errorMessage;
        return errorMessage;
      } else if (minLength != null && text.length < minLength) {
        minLengthMessage = minLengthMessage == null
            ? "Terlalu pendek (minimal $minLength digit)"
            : minLengthMessage;
        return minLengthMessage;
      }
      if (minLength != null && text.length > maxLength) {
        minLengthMessage = minLengthMessage == null
            ? "Terlalu panjang (maximal $maxLength digit)"
            : minLengthMessage;
        return minLengthMessage;
      }
      return null;
    };
  }

  static FormFieldValidator<String> textOnlyValidator({String errorMessage}) {
    return (text) {
      bool isValid = RegExp(r"^[a-zA-Z.]+").hasMatch(text);
      if (!isValid) {
        errorMessage = errorMessage == null
            ? "Bidang ini hanya menerima karakter teks"
            : errorMessage;
        return errorMessage;
      }
      return null;
    };
  }

  static FormFieldValidator<String> passwordValidator(
      {int minLength,
      String minLengthMessage,
      bool isUniques = false,
      String uniqueMessage}) {
    return (text) {
      if (minLength != null && text.length < minLength) {
        minLengthMessage = minLengthMessage == null
            ? "Bidang ini terlalu pendek (panjang teks minimal $minLength)"
            : minLengthMessage;
        return minLengthMessage;
      } else if (isUniques) {
        String message;
        List<String> messages = List();

        var capitalLetterRegex = new RegExp("^(?=.*?[A-Z]).{6,}");
        var numberRegex = new RegExp("^(?=.*?[0-9]).{6,}");
        // var specialRegex = new RegExp("^(?=.*?[!@#%\$&*~^_<>()]).{6,}");

        bool isTextCapitalLetter = capitalLetterRegex.hasMatch(text);
        bool isTextNumber = numberRegex.hasMatch(text);
        // bool isTextSpecial = specialRegex.hasMatch(text);

        if (!isTextCapitalLetter) {
          messages.add("huruf kapital");
        }

        if (!isTextNumber) {
          messages.add("angka");
        }

        // if (!isTextSpecial) {
        //   messages.add("karakter spesial");
        // }

        int len = messages.length;

        if (len > 0) {
          message = "Terlalu lemah, tambahkan";
          for (int i = 0; i < len; i++) {
            message += " " + messages[i];
            if (i != (len - 1)) {
              message += " dan";
            }
          }
        }
        uniqueMessage = uniqueMessage == null ? message : uniqueMessage;
        return uniqueMessage;
      }
      return null;
    };
  }

  static FormFieldValidator<String> nameValidator(
      {int minLength,
      String minLengthMessage,
      String errorMessage,
      bool isUniques = false}) {
    return (text) {
      bool isValid = RegExp("[^A-Za-z\\s]+").hasMatch(text);
      if (minLength != null && text.length < minLength) {
        minLengthMessage = minLengthMessage == null
            ? "This field is too short (min text length is $minLength)"
            : minLengthMessage;
        return minLengthMessage;
      } else if (isValid) {
        errorMessage = errorMessage == null
            ? "This field only accept text character"
            : errorMessage;
        return errorMessage;
      }
      return null;
    };
  }

  static FormFieldValidator<String> usenameValidator(
      {int minLength,
      String minLengthMessage,
      String errorMessage,
      bool isUniques = false}) {
    return (text) {
      bool isValid = RegExp(r"[^A-Za-z0-9]+").hasMatch(text);
      if (minLength != null && text.length < minLength) {
        minLengthMessage = minLengthMessage == null
            ? "This field is too short (min text length is $minLength)"
            : minLengthMessage;
        return minLengthMessage;
      } else if (isValid) {
        errorMessage = errorMessage == null
            ? "This field only accept alphanumeric character"
            : errorMessage;
        return errorMessage;
      }
      return null;
    };
  }

  static FormFieldValidator<String> matchingTextValidator(
      TextEditingController otherField,
      {String errorMessage}) {
    return (text) {
      if (otherField.text != text) {
        errorMessage =
            errorMessage == null ? "This field is not match" : errorMessage;
        return errorMessage;
      }
      return null;
    };
  }

  static FormFieldValidator<String> birthdayValidator(String dateFormat,
      {String errorMessage}) {
    return (text) {
      DateFormat formater = DateFormat(dateFormat);
      DateTime initial;
      try {
        initial = formater.parse(text);
        if (initial.year > DateTime.now().year - 17) {
          print('datetime : ${initial.year}');
          errorMessage = errorMessage == null
              ? "This field must be 17 years lower than today"
              : errorMessage;
          return errorMessage;
        }
      } catch (error) {}

      return null;
    };
  }
}
