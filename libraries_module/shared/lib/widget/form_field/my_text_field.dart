import 'package:dependencies/dependencies.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared/common/base/base.dart';
import 'package:shared/common/common.dart';

enum FieldType { UNDERLINE, WITHOUTBORDER, BORDER }

class MyTextField extends StatefulWidget {
  final bool allCaps;
  final int maxLines;
  final int minLines;
  final int maxLength;
  final bool required;
  final bool isSecure;
  final bool isEnabled;
  final FieldType type;
  final String message;
  final FocusNode nodes;
  final double fontSize;
  final TextAlign textAlign;
  final TextInputType keyboard;
  final String placeholder;
  final Color enableBorderColor;
  final Color focusBorderColor;
  final Color hintTextColor;
  final ValueChanged<String> onChanged;
  final GlobalKey<MyTextFieldState> key;
  final TextEditingController controller;
  final ValueChanged<String> onValueChanged;
  final Function suffixTap;
  final String suffixIcon;
  VoidCallback onEditingComplete;
  final FormFieldValidator<String> validator;

  MyTextField(this.placeholder, this.controller,
      {this.allCaps = false,
      this.maxLines,
      this.minLines = 1,
      this.maxLength,
      this.required = true,
      this.isSecure = false,
      this.isEnabled = true,
      this.type = FieldType.UNDERLINE,
      this.message = "",
      this.nodes,
      this.fontSize,
      this.textAlign = TextAlign.left,
      this.keyboard = TextInputType.text,
      this.enableBorderColor = Colors.grey,
      this.focusBorderColor = Colors.grey,
      this.hintTextColor = Colors.grey,
      this.onChanged,
      this.key,
      this.onValueChanged,
      this.suffixTap,
      this.suffixIcon,
      this.validator})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => MyTextFieldState();
}

class MyTextFieldState extends State<MyTextField> with BaseView {
  final textFieldKey = GlobalKey<FormFieldState<String>>();
  bool isFirstTime = true;
  bool _secureText = true;
  bool isProcessing = false;
  String valText = "";

  @override
  void initState() {
    super.initState();
    widget.controller.addListener(_listener);
  }

  @override
  void dispose() {
    super.dispose();
    widget.controller.removeListener(_listener);
  }

  showHidePassword() {
    setState(() {
      _secureText = !_secureText;
    });
  }

  _listener() {
    if (isFirstTime) {
      isFirstTime = false;
    }
    textFieldKey.currentState.validate();
  }

  void validate() {
    textFieldKey.currentState.validate();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Flexible(
              child: TextFormField(
            key: textFieldKey,
            style: TextStyle(fontSize: widget.fontSize),
            textAlign: widget.textAlign,
            keyboardType: widget.keyboard,
            controller: widget.controller,
            focusNode: widget.nodes,
            maxLines: widget.maxLines,
            minLines: widget.minLines,
            maxLength: widget.maxLength,
            enabled: widget.isEnabled,
            autocorrect: false,
            obscureText: !widget.isSecure ? widget.isSecure : _secureText,
            onEditingComplete: widget.onEditingComplete,
            decoration: inputDecoration,
            onFieldSubmitted: (String value) {
              if (widget.onValueChanged != null) {
                widget.onValueChanged(value);
              }
            },
            onChanged: (String value) {
              if (widget.onChanged != null) {
                widget.onChanged(value);
              } else {
                return widget.onChanged("");
              }
            },
            validator: (String value) {
              value = widget.controller.text;
              valText = value;
              if (value.isEmpty && widget.required) {
                return widget.placeholder + ' harus diisi';
              }
              if (widget.validator != null) {
                return widget.validator(value);
              }
              return null;
            },
          ))
        ],
      ),
    );
  }

  InputDecoration get inputDecoration {
    switch (widget.type) {
      case FieldType.UNDERLINE:
        return InputDecoration(
          labelText: widget.placeholder,
          errorText: widget.message == '' ? null : widget.message,
          alignLabelWithHint: true,
          hintStyle: TextStyle(color: widget.hintTextColor),
          counterStyle: TextStyle(
            height: double.minPositive,
          ),
          counterText: "",
          errorStyle: TextStyle(fontSize: 9),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: widget.focusBorderColor),
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: widget.enableBorderColor),
          ),
          suffixIcon: widget.isSecure != true
              ? null
              : IconButton(
                  color: Colors.grey,
                  icon: Icon(
                    _secureText
                        ? Icons.visibility_off_outlined
                        : Icons.visibility_outlined,
                    size: 20,
                    color: _secureText ? colorPalettes.grey6 : colorPalettes.grey3,
                  ),
                  onPressed: showHidePassword),
        );
      case FieldType.WITHOUTBORDER:
        return InputDecoration(
            enabled: widget.isEnabled,
            hintText: widget.placeholder,
            border: InputBorder.none,
            isDense: true,
            counterText: "",
            contentPadding: EdgeInsets.only(bottom: 3, top: 6),
            suffixIconConstraints: BoxConstraints(maxHeight: 34, minHeight: 34),
            suffixIcon: widget.suffixIcon != null ? _suffixIcons : null);
      case FieldType.BORDER:
        return InputDecoration(
            enabled: widget.isEnabled,
            hintText: widget.placeholder,
            border: InputBorder.none,
            isDense: true,
            counterText: "",
            contentPadding: EdgeInsets.only(bottom: -20),
            suffixIconConstraints: BoxConstraints(maxHeight: 10, minHeight: 10),
            suffixIcon: widget.suffixIcon != null ? _suffixIcons : null);
      default:
        return InputDecoration(
          labelText: widget.placeholder,
          errorText: widget.message == '' ? null : widget.message,
          alignLabelWithHint: true,
          hintStyle: TextStyle(color: widget.hintTextColor),
          counterStyle: TextStyle(
            height: double.minPositive,
          ),
          counterText: "",
          errorStyle: TextStyle(fontSize: 9),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: widget.focusBorderColor),
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: widget.enableBorderColor),
          ),
          suffixIcon: widget.suffixIcon != null ? _suffixIcons : null,
        );
    }
  }

  Widget get _suffixIcons {
    switch (widget.isSecure) {
      case true:
        return IconButton(
            color: Colors.grey,
            icon: Icon(
              _secureText ? Icons.visibility_off_outlined : Icons.visibility_outlined,
              size: 20,
              color: _secureText ? colorPalettes.grey6 : colorPalettes.grey3,
            ),
            onPressed: showHidePassword);
      case false:
        return IconButton(
            color: Colors.grey,
            icon: SvgPicture.asset(
              widget.suffixIcon,
              width: 20,
              height: 20,
              color: valText.isEmpty ? colorPalettes.grey6 : colorPalettes.grey3,
            ),
            onPressed: widget.suffixTap);
      default:
        return IconButton(
            color: Colors.grey,
            icon: Icon(
              _secureText ? Icons.visibility_off_outlined : Icons.visibility_outlined,
              size: 20,
              color: _secureText ? colorPalettes.grey6 : colorPalettes.grey3,
            ),
            onPressed: widget.suffixTap);
    }
  }
}
