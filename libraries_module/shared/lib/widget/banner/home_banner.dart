import 'package:dependencies/dependencies.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeBanner extends StatefulWidget {
  final List<Widget> item;
  final double height;
  final double ratio;
  final bool autoPlay;
  final Function(int index, CarouselPageChangedReason reason) onPageChanged;

  HomeBanner(
      {Key key,
        this.item,
        this.height,
        this.ratio = 16 / 9,
        this.autoPlay = true,
        this.onPageChanged})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => HomeBannerState();
}

class HomeBannerState extends State<HomeBanner> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: CarouselSlider(
        items: widget.item,
        options: CarouselOptions(
            autoPlay: widget.autoPlay,
            viewportFraction: 1,
            aspectRatio: widget.ratio,
            initialPage: 1,
            height: widget.height,
            enableInfiniteScroll: true,
            onPageChanged: widget.onPageChanged),
      ),
    );
  }
}
