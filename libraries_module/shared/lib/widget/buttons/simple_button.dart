import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SimpleButton extends StatefulWidget {
  final EdgeInsets padding;
  final EdgeInsets margin;
  final double borderRadius;
  final double elevation;
  final Widget child;
  final int buttontype;
  final Color buttonColor;
  final Color borderColor;
  final Color disabledColor;
  final Color disabledTextColor;
  final Function onPress;

  const SimpleButton({Key key,
    this.padding,
    this.margin,
    this.borderRadius = 2,
    this.elevation = 0,
    this.buttontype = 0,
    this.borderColor = Colors.grey,
    this.buttonColor = Colors.blue,
    this.disabledColor = Colors.grey,
    this.disabledTextColor = Colors.grey,
    this.onPress,
    this.child})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => SimpleButtonState();
}

class SimpleButtonState extends State<SimpleButton> {
  @override
  Widget build(BuildContext context) {
    if (widget.buttontype == 0)
      return _raiseButton();
    else if (widget.buttontype == 1)
      return _outlineButton();
    else
      return _raiseButton();
  }

  Widget _outlineButton() {
    return OutlineButton(
      onPressed: widget.onPress,
      child: widget.child,
      borderSide: BorderSide(
          width: 1,
          style: BorderStyle.solid,
          color: widget.borderColor
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(widget.borderRadius),
      ),
    );
  }

  Widget _raiseButton() {
    return RaisedButton(
      elevation: widget.elevation,
      onPressed: widget.onPress,
      color: widget.buttonColor,
      child: widget.child,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(widget.borderRadius),
      ),
    );
  }
}
