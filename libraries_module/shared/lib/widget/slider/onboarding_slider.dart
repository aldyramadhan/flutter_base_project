import 'package:authentication/data/entity/entity.dart';
import 'package:dependencies/dependencies.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OnboardingSlider extends StatefulWidget {
  final List<GetstartedData> item;
  final double height;
  final double ratio;
  final Function(int index, CarouselPageChangedReason reason) onPageChanged;

  OnboardingSlider(
      {Key key,
      this.item,
      this.height,
      this.ratio = 16 / 9,
      this.onPageChanged})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => OnboardingSliderState();
}

class OnboardingSliderState extends State<OnboardingSlider> {
  @override
  Widget build(BuildContext context) {
    return CarouselSlider(
      options: CarouselOptions(
          autoPlay: false,
          enlargeCenterPage: true,
          viewportFraction: 1.0,
          aspectRatio: widget.ratio,
          initialPage: 1,
          height: widget.height,
          enableInfiniteScroll: true,
          onPageChanged: widget.onPageChanged),
      items: widget.item.map((item) {
        return Builder(
            builder: (context) {
              return Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                      padding: EdgeInsets.only(left: 64.w, right: 64.w),
                      child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(180)),
                        child: CachedNetworkImage(
                          imageUrl: item.image,
                          width: MediaQuery.of(context).size.width.w * 1.4,
                          height: MediaQuery.of(context).size.width.w * 1.4,
                          fit: BoxFit.fill,
                        ),
                      )
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 36.h),
                    child: Text(item.title,
                      style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 22.h),
                    child: Text(item.description,
                      style: TextStyle(fontSize: 14),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              );
            });
      }).toList(),
    );
  }
}
