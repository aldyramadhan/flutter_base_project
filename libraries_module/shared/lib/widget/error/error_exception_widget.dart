import 'package:core/network/network.dart';
import 'package:dependencies/dependencies.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared/widget/error/no_internet.dart';

import 'error_code.dart';

class ErrorExceptionWidget extends StatelessWidget {
  final NetworkExceptions exceptions;
  final Gestures tapReload;

   ErrorExceptionWidget({Key key, this.exceptions, this.tapReload}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return exceptions.maybeWhen(
      noInternetConnection: () => NoInternet(onTap: tapReload),
      orElse: () => ErrorCode(
        message: NetworkExceptions.getErrorMessage(exceptions),
        onTap: tapReload,
      ),
    );
  }

}