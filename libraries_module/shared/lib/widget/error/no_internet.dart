import 'package:dependencies/dependencies.dart';
import 'package:flutter/material.dart';
import 'package:shared/common/base/base.dart';

class NoInternet extends StatelessWidget with BaseView{
  final Gestures onTap;

  NoInternet({Key key, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.symmetric(horizontal: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Txt(appConstant.noInternet,
            style: styles.textMediumBoldStyle(colorPalettes.lightPrimary),
          ),
          SizedBox(height: 10),
          Txt(appConstant.noInternetDesc,
            style: styles.textNormalStyle(colorPalettes.textlightGray, textAlign: true),
          ),
          SizedBox(height: 30),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 56),
            child: Txt(appConstant.tryAgain,
              style: styles.buttonStyle(true),
              gesture: onTap,
            ),
          )
        ],
      ),
    );
  }
}
