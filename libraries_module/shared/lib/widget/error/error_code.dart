import 'package:flutter/material.dart';
import 'package:dependencies/dependencies.dart';
import 'package:shared/common/base/base.dart';
import '../../common/common.dart';

class ErrorCode extends StatelessWidget with BaseView{
  final String message;
  final Gestures onTap;

  ErrorCode({Key key, this.message, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      color: colorPalettes.white,
      padding: EdgeInsets.symmetric(horizontal: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(height: 10),
          Txt(getString(message),
            style: styles.textMediumBoldStyle(colorPalettes.lightPrimary),
          ),
          SizedBox(height: 30),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 56),
            child: Txt(appConstant.tryAgain,
              style: styles.buttonStyle(true),
              gesture: onTap,
            ),
          )
        ],
      ),
    );
  }
}
