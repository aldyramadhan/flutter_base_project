import 'package:dependencies/dependencies.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared/common/base/base.dart';

class SnackbarCard extends StatelessWidget with BaseView {
  final Color backgroundColor;
  final String icon;
  final String message;

  SnackbarCard({Key key, this.backgroundColor, this.icon, this.message})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: backgroundColor,
      height: 42,
      padding: EdgeInsets.only(left: 16, right: 12),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SvgPicture.asset(
            icon,
            height: 20,
            width: 20,
          ),
          SizedBox(width: 16),
          Text(
            message,
            style: TextStyle(fontSize: 14, color: colorPalettes.white),
          )
        ],
      ),
    );
  }
}
