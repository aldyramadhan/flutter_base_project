
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:dependencies/dependencies.dart';
import 'package:shared/common/common.dart';

class SimpleNotificationSnackbar {
  final Duration duration;
  final bool dispose;
  final Widget child;
  Function cancel;

  SimpleNotificationSnackbar(
      {this.duration, this.dispose = true, this.child, this.cancel});

  show() {
    Modular.get<Audio>().playLocalAssetNotif();
    BotToast.showCustomNotification(
        duration: this.duration,
        toastBuilder: (cancel) {
          this.cancel = cancel;
          return SimpleNotificationSnackbarWidget(
            cancelFunc: cancel,
            dispose: this.dispose,
            child: child,
          );
        });
  }
}

class SimpleNotificationSnackbarWidget extends StatelessWidget {
  final CancelFunc cancelFunc;
  final bool dispose;
  final Widget child;

  SimpleNotificationSnackbarWidget({
    Key key,
    this.cancelFunc,
    this.dispose = false,
    this.child,
  }) : super(key: key);

  void _cancel() {
    cancelFunc();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(onTap: () => _cancel(), child: child);
  }
}
