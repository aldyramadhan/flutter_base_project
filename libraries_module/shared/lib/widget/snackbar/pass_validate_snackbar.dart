import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared/common/base/base.dart';
import 'package:dependencies/dependencies.dart';

import '../widget.dart';

class PassValidateSnackBar with BaseView {
  final Duration duration;
  final bool isValidate1;
  final bool isValidate2;
  final bool dispose;

  PassValidateSnackBar(
      {this.duration, this.isValidate1, this.isValidate2, this.dispose = false});

  show() {
    BotToast.showCustomNotification(
        duration: this.duration,
        toastBuilder: (cancel) => CustomWidget(
              cancelFunc: cancel,
              isValidate1: this.isValidate1,
              isValidate2: this.isValidate2,
              dispose: this.dispose,
            ));
  }
}

class CustomWidget extends StatelessWidget with BaseView {
  final CancelFunc cancelFunc;
  final bool isValidate1;
  final bool isValidate2;
  final bool dispose;

  CustomWidget(
      {Key key,
      this.cancelFunc,
      this.isValidate1,
      this.isValidate2,
      this.dispose = false})
      : super(key: key);

  void _cancel() {
    print("cancel : ${dispose}");
    if (dispose) {
      cancelFunc();
    } else {
      if (isValidate1 && isValidate2) Timer(Duration(seconds: 1), cancelFunc);
    }
  }

  @override
  Widget build(BuildContext context) {
    _cancel();
    return GestureDetector(
      onTap: () => _cancel(),
      child: Wrap(
        children: [
          Column(
            children: [
              SnackbarCard(
                backgroundColor:
                    isValidate1 ? colorPalettes.lightPrimary : colorPalettes.red2,
                icon: isValidate1
                    ? imageAsset.ceklisCircleOutline
                    : imageAsset.wrongCircleOutline,
                message: appConstant.passCapitalValidate,
              ),
              SnackbarCard(
                backgroundColor:
                    isValidate2 ? colorPalettes.lightPrimary : colorPalettes.red2,
                icon: isValidate2
                    ? imageAsset.ceklisCircleOutline
                    : imageAsset.wrongCircleOutline,
                message: appConstant.passNumberValidate,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
