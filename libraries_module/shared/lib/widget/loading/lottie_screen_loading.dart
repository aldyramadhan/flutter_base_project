import 'package:dependencies/dependencies.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared/common/base/base.dart';

class LottieScreenLoading extends StatelessWidget with BaseView {
  final String lottieAssets;
  final double height;
  final double width;
  final Color backgroundColor;

  LottieScreenLoading(
      {Key key,
        this.lottieAssets = "assets/json/lottie_blue_ripples.json",
        this.height = 65,
        this.width = 65,
        this.backgroundColor = Colors.black
      })
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      color: backgroundColor,
      child: Center(
        child: Lottie.asset(lottieAssets, width: width, height: height),
      ),
    );
  }
}
