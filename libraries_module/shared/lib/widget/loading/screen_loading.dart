import 'dart:ui';
import 'package:flutter/material.dart';

mixin ScreenLoading<T extends StatefulWidget> on State<T> {
  bool isLoading = false;
  static Widget _globalLoading;
  static double _globalLoadingBgBlur = 5.0;

  startLoading() {
    this.setState(() {
      isLoading = true;
    });
  }

  stopLoading() {
    this.setState(() {
      isLoading = false;
    });
  }

  Future<T> performFuture<T>(Function futureCallback) async {
    this.startLoading();
    T data = await futureCallback();
    this.stopLoading();
    return data;
  }

  double loadingBgBlur() {
    return null;
  }

  double _loadingBgBlur() {
    return this.loadingBgBlur() ?? ScreenLoading._globalLoadingBgBlur ?? 5.0;
  }

  Widget loader() {
    return null;
  }

  Widget _loader() {
    return this.loader() ?? ScreenLoading._globalLoading ?? CircularProgressIndicator();
  }

  Widget _buildLoader() {
    if (this.isLoading) {
      return Container(
        color: Colors.transparent,
        child: Center(
          child: this._loader(),
        ),
      );
    } else {
      return Container();
    }
  }

  Widget screen(BuildContext context);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        screen(context),
        BackdropFilter(
          child: _buildLoader(),
          filter: ImageFilter.blur(
            sigmaX: this._loadingBgBlur(),
            sigmaY: this._loadingBgBlur(),
          ),
        ),
      ],
    );
  }
}

class ScreenLoadingApp extends StatelessWidget {
  final MaterialApp app;
  final Widget globalLoading;
  final double globalLoadingBgBlur;

  ScreenLoadingApp({
    @required this.app,
    this.globalLoading,
    this.globalLoadingBgBlur,
  });

  @override
  Widget build(BuildContext context) {
    ScreenLoading._globalLoading = this.globalLoading;
    ScreenLoading._globalLoadingBgBlur = this.globalLoadingBgBlur;
    return app;
  }
}
