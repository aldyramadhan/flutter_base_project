import 'package:flutter/material.dart';

extension SizeContext on BuildContext {

  MediaQueryData get _mq => MediaQuery.of(this);

  Size get size => _mq.size;

  double get width => size.width;

  double get height => size.height;

}