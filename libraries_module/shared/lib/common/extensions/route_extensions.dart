extension RouteExtension on String {
  String get dashboard {
    return '/dasboard$this';
  }

  String get home {
    return '/home$this';
  }

  String get category {
    return '/category$this';
  }
}
