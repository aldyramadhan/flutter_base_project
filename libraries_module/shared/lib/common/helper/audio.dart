import 'dart:io';

import 'package:dependencies/dependencies.dart';
import 'package:shared/common/utils/utils.dart';

class Audio {
  Future<AudioPlayer> playLocalAssetNotif() async {
    AudioCache cache = new AudioCache();
    if (Platform.isAndroid) {
      return await cache.play(Modular.get<AudioAssets>().iphoneMessage);
    } else {
      return await cache.play(Modular.get<AudioAssets>().iphoneMessage);
    }
  }
}
