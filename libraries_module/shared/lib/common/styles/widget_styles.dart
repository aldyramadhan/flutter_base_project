import 'package:dependencies/dependencies.dart';
import 'package:flutter/material.dart';
import 'package:shared/common/styles/styles.dart';

class WidgetStyles {

  // -- VeryTiny Text
  final textVeryTinyStyle = (color, {double pH = 0, double pV = 0}) => TxtStyle()
    ..textColor(color)
    ..fontSize(8)
    ..background.color(Colors.transparent)
    ..padding(horizontal: pH, vertical: pV)
    ..textAlign.start();

  // -- Tiny Text
  final textTinyStyle = (color, {double pH = 0, double pV = 0, int mL}) => TxtStyle()
    ..textColor(color)
    ..fontSize(12)
    ..maxLines(mL)
    ..textOverflow(TextOverflow.ellipsis)
    ..background.color(Colors.transparent)
    ..padding(horizontal: pH, vertical: pV)
    ..textAlign.start();

  final textTinyBoldStyle = (color, {double pH = 0, double pV = 0, int mL}) => TxtStyle()
    ..textColor(color)
    ..fontSize(12)
    ..maxLines(mL)
    ..textOverflow(TextOverflow.ellipsis)
    ..background.color(Colors.transparent)
    ..padding(horizontal: pH, vertical: pV)
    ..bold()
    ..textAlign.start();

  // -- Small Text
  final textSmallStyle = (color, {double pH = 0, double pV = 0}) => TxtStyle()
    ..textColor(color)
    ..fontSize(14)
    ..background.color(Colors.transparent)
    ..padding(horizontal: pH, vertical: pV)
    ..textAlign.start();

  final textSmallBoldStyle = (color, {double pH = 0, double pV = 0}) => TxtStyle()
    ..textColor(color)
    ..fontSize(14)
    ..bold(true)
    ..background.color(Colors.transparent)
    ..padding(horizontal: pH, vertical: pV)
    ..textAlign.start();

  // -- Normal Text
  final textNormalStyle = (color, {double pH = 0, double pV = 0, bool textAlign = false, int mL}) => TxtStyle()
    ..textColor(color)
    ..fontSize(16)
    ..maxLines(mL)
    ..textAlign.center(textAlign)
    ..textOverflow(TextOverflow.ellipsis)
    ..background.color(Colors.transparent)
    ..padding(horizontal: pH, vertical: pV);

  final textNormalBoldStyle = (color, {double pH = 0, double pV = 0, int mL}) => TxtStyle()
    ..textColor(color)
    ..fontSize(16)
    ..bold()
    ..maxLines(mL)
    ..padding(horizontal: pH, vertical: pV)
    ..textAlign.start();

  // -- Medium Text
  final textMediumStyle = (color, {double pH = 0, double pV = 0}) => TxtStyle()
    ..textColor(color)
    ..fontSize(18)
    ..background.color(Colors.transparent)
    ..padding(horizontal: pH, vertical: pV)
    ..textAlign.start();

  final textMediumBoldStyle = (color, {double pH = 0, double pV = 0, int mL}) => TxtStyle()
    ..textColor(color)
    ..fontSize(18)
    ..bold(true)
    ..maxLines(mL)
    ..textOverflow(TextOverflow.ellipsis)
    ..background.color(Colors.transparent)
    ..padding(horizontal: pH, vertical: pV)
    ..textAlign.start();

  // -- Large Text
  final textLargeStyle = (color, {double pH = 0, double pV = 0, bool bold = false}) => TxtStyle()
    ..textColor(color)
    ..fontSize(20)
    ..bold(bold)
    ..textOverflow(TextOverflow.ellipsis)
    ..background.color(Colors.transparent)
    ..padding(horizontal: pH, vertical: pV)
    ..textAlign.start();

  // -- Huge Text
  final textHugeStyle = (color, {double pH = 0, double pV = 0}) => TxtStyle()
    ..textColor(color)
    ..fontSize(22)
    ..textOverflow(TextOverflow.ellipsis)
    ..background.color(Colors.transparent)
    ..padding(horizontal: pH, vertical: pV)
    ..textAlign.start();

  final textHugeBoldStyle = (color, {double pH = 0, double pV = 0, int mL}) => TxtStyle()
    ..textColor(color)
    ..fontSize(22)
    ..bold()
    ..maxLines(mL)
    ..textOverflow(TextOverflow.ellipsis)
    ..textOverflow(TextOverflow.ellipsis)
    ..background.color(Colors.transparent)
    ..padding(horizontal: pH, vertical: pV)
    ..textAlign.start();

  final textVeryHugeBoldStyle = (color, {double pH = 0, double pV = 0}) => TxtStyle()
    ..textColor(color)
    ..fontSize(28)
    ..bold()
    ..background.color(Colors.transparent)
    ..padding(horizontal: pH, vertical: pV)
    ..textAlign.start();

  // -- button
  final buttonStyle = (bool pressed, {Color color, Color txtColor,
    double width, double height = 45, double fontSize = 12, double radius = 100}) => TxtStyle()
    ..background.color(
        color != null ? color :
        pressed ? ColorPalettes().lightPrimary : ColorPalettes().inActiveButton)
    ..textColor(
        txtColor != null ? txtColor :
        pressed ? ColorPalettes().textWhite : ColorPalettes().inActiveText)
    ..borderRadius(all: radius)
    ..height(height)
    ..width(width)
    ..fontSize(fontSize)
    ..padding(horizontal: 15)
    ..ripple(true)
    ..bold(true)
    ..alignmentContent.center(true)
    ..animate(150, Curves.easeOut);

  final mediumPrimaryButtonStyle = (
      bool pressed, {Color color, Color txtColor, double width = double.infinity}) => TxtStyle()
    ..background.color(
        color != null ? color :
        pressed ? ColorPalettes().lightPrimary : ColorPalettes().inActiveButton)
    ..textColor(
        txtColor != null ? txtColor :
        pressed ? ColorPalettes().textWhite : ColorPalettes().inActiveText)
    ..borderRadius(all: 100)
    ..height(45)
    ..width(width)
    ..fontSize(18)
    ..padding(horizontal: 15)
    ..ripple(true)
    ..bold(true)
    ..alignmentContent.center(true)
    ..animate(150, Curves.easeOut);

  final mediumOutlineButtonStyle = (pressed) => TxtStyle()
    ..background.color(pressed ? ColorPalettes().lightPrimary : ColorPalettes().inActiveButton)
    ..textColor(pressed ? ColorPalettes().textWhite : ColorPalettes().inActiveText)
    ..borderRadius(all: 100)
    ..height(45)
    ..width(double.infinity)
    ..fontSize(18)
    ..padding(horizontal: 15)
    ..ripple(true)
    ..bold(true)
    ..alignmentContent.center(true)
    ..animate(150, Curves.easeOut);

  // -- parent
  final normalParent = ({background = Colors.white}) => ParentStyle()
    ..background.color(background)
    ..padding(left: 16, right: 16, top: 8, bottom: 20);

  final mediumParent = ({background = Colors.white}) => ParentStyle()
    ..background.color(background)
    ..padding(left: 18, right: 18, top: 8, bottom: 20);

  final largeParent = ({background = Colors.white}) => ParentStyle()
    ..background.color(background)
    ..padding(left: 20, right: 20, top: 10, bottom: 20);

  final hugeParent = ({background = Colors.white}) => ParentStyle()
    ..background.color(background)
    ..padding(left: 24, right: 24, top: 12, bottom: 25);

  // -- input field
  final inputFieldStyle = (bool isActive, TxtStyle activeStyle) => TxtStyle()
    ..textColor(ColorPalettes().primaryText)
    ..textAlign.left()
    ..fontSize(16)
    ..minHeight(48)
    ..padding(horizontal: 15)
    ..borderRadius(all: 6)
    ..alignment.center()
    ..alignmentContent.center(true)
    ..background.color(ColorPalettes().fieldBackground)
    ..animate(300, Curves.easeOut)
    ..add(isActive ? activeStyle : null, override: true);

  final TxtStyle inputFieldActiveStyle = TxtStyle()
    ..background.color(ColorPalettes().lightPrimary)
    ..bold(true)
    ..textColor(Colors.white);

  // -- card
  final cardNormalStyle = (color, {double elevetion = .05, Color elevetionColor = Colors.grey,
    double radius = 15, double mH = 5, double mV = 2}) => ParentStyle()
    ..borderRadius(all: radius)
    ..animate(200, Curves.easeOut)
    ..margin(horizontal: mH, vertical: mV)
    ..background.color(color)
    ..elevation(elevetion, color: elevetionColor);

  final cardBorderNormalStyle = (color, borderColor,
      {double elevetion = .05, double mH = 5, double mV = 2}) => ParentStyle()
    ..alignment.center()
    ..borderRadius(all: 15)
    ..margin(horizontal: mH, vertical: mV)
    ..animate(200, Curves.easeOut)
    ..background.color(color)
    ..elevation(elevetion)
    ..border(all: .8, color: borderColor);

  // -- container
  final containerNormalStyle = (color, {double radius = 15, double mH = 5, double mV = 2}) => ParentStyle()
    ..borderRadius(all: radius)
    ..animate(200, Curves.easeOut)
    ..margin(horizontal: mH, vertical: mV)
    ..background.color(color);

}