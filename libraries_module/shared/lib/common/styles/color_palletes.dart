import 'dart:ui';

import 'package:flutter/material.dart';

class ColorPalettes {

  //Colors for dynamic theme
  Color lightPrimary = Color(0xff279EEE);
  Color darkPrimary = Color(0xff212121);
  Color lightAccent = Color(0xfffcfcff);
  Color darkAccent = Color(0xffFFBB3B);
  Color lightBG = Color(0xfffcfcff);
  Color darkBG = Color(0xff000000);

  // -- text
  Color primaryText = Color(0xff333333);
  Color secondaryText = Color(0xff52575C);
  Color textGray = Color(0xff52575C);
  Color textlightGray = Color(0xffA0A4A8);
  Color textWhite = Color(0xffF9F9FA);
  Color textBluePrimary = Color(0xff279EEE);
  Color inActiveText = Color(0xff52575C);

  // -- background
  Color primaryBackground = Colors.white;
  Color fieldBackground = Color(0xffF2F2F2);

  // -- button
  Color inActiveButton = Color(0xffA0A4A8);
  Color secondaryButton = Color(0xffF9F9FA);

  Color white = Color(0xffffffff);
  Color whiteSemiTransparent = Colors.white70;
  Color whiteSemiTransparent2 = Colors.white30;
  Color black = Colors.black;
  Color black54 = Colors.black54;
  Color black2 = Color(0xff52575C);
  Color grey = Colors.grey;
  Color grey3 = Color(0xff52575C);
  Color grey4 = Color(0xffE8E8E8);
  Color grey5 = Color(0xffA0A4A8);
  Color grey6 = Color(0xff929292);
  Color grey7 = Color(0xffF2F2F2);
  Color grey8 = Color(0xffAAAAAA);
  Color grey10 = Color(0xff9A9A9B);
  Color grey11 = Color(0xff9A9A9B);
  Color grey12 = Color(0xffF1F1F1);
  Color red = Colors.red;
  Color red2 = Color(0xffE06379);
  Color pink = Color(0xffFF1169);
  Color yellow = Colors.yellow;
  Color green = Colors.green;
  Color setActive = Colors.grey[500];
  Color transparent = Color(0x00000000);

}