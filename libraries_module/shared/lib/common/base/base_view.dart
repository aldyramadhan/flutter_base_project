import 'package:core/local/local.dart';
import 'package:dependencies/dependencies.dart';
import 'package:flutter/material.dart';
import 'package:shared/common/common.dart';
import 'package:shared/common/utils/json_assets.dart';
import 'package:shared/widget/widget.dart';

abstract class BaseView {

  final appConstant = Modular.get<AppConstant>();
  final colorPalettes = Modular.get<ColorPalettes>();
  final imageAsset = Modular.get<ImageAssets>();
  final jsonAsset = Modular.get<JsonAssets>();
  final navigation = Modular.get<Navigation>();
  final styles = Modular.get<WidgetStyles>();
  final pref = Modular.get<SharedPrefHelper>();

  Gestures get buttonTap => Gestures();
  // var analytics = lr<FirebaseAnalyticsService>();
  // var pref = lr<PrefHelper>();
  // // final FacebookAnalyticsService fbAnalytics = locator<FacebookAnalyticsService>();
  // // var fireNotification = lr<FirebasePushNotificationService>();

  int getInt( int data ) {
    if ( data == null )
      return 0;
    else
      return data;
  }

  bool getBool( bool data ) {
    if ( data == null )
      return false;
    else
      return data;
  }

  double getDouble( double data ) {
    if ( data == null )
      return 0;
    else
      return data;
  }

  String getString( String data ) {
    if ( data == null )
      return "";
    else
      return data;
  }

  List getList( List data) {
    if ( data == null)
      return [];
    else
      return data;
  }

  void showToast( String message, BuildContext ctx, {int duration = 3} ) {
    return Toast.show(message.toString(), ctx, duration: duration,
        gravity: Toast.BOTTOM);
  }

  void showMessage(BuildContext context, {String title, String message}) {
    showDialog(
        context: context,
        builder: (BuildContext context) => MessageDialog(
          title: title,
          message: message,
        )
    );
  }

  void showConfirmation( String message, {String title = ""} ){

  }

  void doLogout(){}

  void saveLogin(String token) {
    pref.saveLogin(true);
    pref.saveToken(token);
  }

  // -- Widget

}
