import 'package:dependencies/dependencies.dart';
import 'package:flutter/material.dart';

class Navigation {
  /// class for navigation between pages

  // GlobalKey<NavigatorState> _navigationKey = GlobalKey<NavigatorState>();
  //
  // GlobalKey<NavigatorState> get navigationKey => _navigationKey;
  //
  // void pop() {
  //   return _navigationKey.currentState.pop();
  // }
  //
  // Future<dynamic> navigateTo(String routeName, {dynamic arguments}) {
  //   return _navigationKey.currentState.pushNamed(routeName, arguments: arguments);
  // }

  void toPop() {
    Modular.to.pop();
  }

  void toPopData(Object result) {
    Modular.to.pop(result);
  }

  void to(String nameRouted, {Object argumentClass}) {
    Modular.to.pushNamed(nameRouted, arguments: argumentClass);
  }

  void linkTo(String nameRouted, {Object argumentClass}) {
    Modular.link.pushNamed(nameRouted, arguments: argumentClass);
  }

  void toOff(String nameRouted, {Object argumentClass}) {
    Modular.to.pushReplacementNamed(nameRouted, arguments: argumentClass);
  }

  void linkToOff(String nameRouted, {Object argumentClass}) {
    Modular.link.pushReplacementNamed(nameRouted, arguments: argumentClass);
  }

  void toOffAll(String nameRouted, {Object argumentClass}) {
    Modular.to.pushNamedAndRemoveUntil(nameRouted, (Route<dynamic> route) => false,
        arguments: argumentClass);
  }

  void linkToOffAll(String nameRouted, {Object argumentClass}) {
    Modular.link.pushNamedAndRemoveUntil(nameRouted, (Route<dynamic> route) => false,
        arguments: argumentClass);
  }

  void launchURL(url) async {
    if (await canLaunch(url)) {
      final bool nativeAppLaunchSucceeded = await launch(
        url,
        forceSafariVC: false,
        forceWebView: false,
        universalLinksOnly: true,
      );
      if (!nativeAppLaunchSucceeded) {
        await launch(
          url,
          forceSafariVC: true,
        );
      }
    } else {
      throw 'Could not launch $url';
    }
  }
}
