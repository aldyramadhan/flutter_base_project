class AppConstant {

  /// common
  String appName = "Checkr";
  String appNameDev = "Base App Development";
  String noInternetConnection = "No Internet Connection";

  String countryCode = "+62";
  String branda = "Beranda";
  String category = "Kategory";
  String account = "Akun";
  String startCheck = "Mulai Cek";
  String popular = "Popular";
  String noData = "Not Found";
  String transaction = "Transaksi";
  String commingSoon = "Comming Soon";
  String favouriteMovies = "Favourite Movies";
  String pleaseWait = "please wait..";
  String signinWithGoogle = "Sign in with google";
  String signinWithApple = "Sign in with apple";
  String signinWithFacebook = "Sign in with facebook";
  String signinWithTwitter = "Sign in with twitter";
  String socialSignin = "Social Signin";
  String name = "Nama";
  String email = "Email";
  String password = "Password";
  String daftar = "Daftar";
  String masuk = "Masuk";
  String send = "Kirim";
  String openEmail = "Buka Email";
  String sendOtp = "Kirim OTP";
  String skip = "Skip";
  String loginWith = "masuk dengan";
  String registerWith = "daftar dengan";
  String google = "Google";
  String facebook = "Facebook";
  String forgotPassword = "Lupa password?";
  String forgotPasswordDesc = "Masukkan email kamu yang terdaftar\ndi aplikasi dikolom dibawah";
  String enterWaNumber = "Masukkan nomor whatsapp";
  String enterWaDescription = "Tambahkan nomor ponsel kamu dan kami\nakan mengirim kode verifikasi ke whatsapp";
  String verificationOtp = "Verification OTP";
  String otpDescription = "Masukkan kode OTP kamu dibawah yang\ntelah kami kirim ke nomor +6288980095";
  String changePhone = "Ubah nomor ponsel";
  String passCapitalValidate = "Password harus ada huruf kapital";
  String passNumberValidate = "Password harus ada angka";
  String signinSuccess = "Berhasil masuk";
  String monitorTransaction = "Pantau transaksimu";
  String seeAll = "Lihat semua";
  String whyCheckr = "Kenapa harus checkr app?";
  String whyCheckrDesc = "Mari pelajari kenapa kamu harus menggunakannya";
  String frequentlySeen = "Barang yang sering dicek?";
  String doYouKnow = "Tahukah kamu?";
  String monitor = "Pantau";
  String rememberSandi = "Sudah ingat kata sandi?";
  String emailSend = "Email terkirim, silahkan cek email anda";
  String success = "Sukses!";
  String successForgotPass = "Silahkan cek email kamu untuk membuat\npassword baru";
  String notGeetingEmail = "Belum mendapatkan email?";
  String resend = "Kirim ulang";
  String letsCheck = "Yuk, cek status barang kamu dibawah ini";
  String retry = "Coba lagi";
  String ok = "Ok";
  String noInternet = "Tidak ada koneksi internet!";
  String noInternetDesc = "Silahkan periksa koneksi internetr anda dan coba lagi...";
  String tryAgain = "Coba lagi";
  String checkYourExpectation = "Cek sesuai ekspektasi kamu";
  String subCategory = "Sub Kategory";
  String detail = "Detail";
  String checkgoods = "Cek Barang";
  String description = "Deskripsi";
  String recentSearch = "Pencarian terbaru";
  String justCheckFirst = "Cek aja dulu...";
  String notification = "Notifikasi";
  String noNotification = "Belum ada notifikasi";
  String noNotificationDesc = "Gak usah khawatir, kita akan selalu ngabarin kamu kok";
  String myProfile = "Profilku";
  String transactionHistory = "Riwayat Transaksi";
  String pending = "Pending";
  String finish = "Selesai";

  // -- cache key
  String getStartedKey = "/get-started.json";
  String movieCommingSoonKey = "/movie-comming-soon.json";
  String movieDetailKey = "/movie-detail.json";
  String deviceInfo = "/device-info.json";

  static const LOGIN = "isLogin";
  static const TOKEN = "token";
  static const ACCOUNT = "account.json";

  // -- Deeplink key
  final String launchEmail = "https://mail.google.com";
  // final String launchEmail = "message://com.google.android.gm";

}
