class NamedRoutes {
  String splashPage = "/splash";
  String onBoardingPage = "/onBoarding";
  String loginPage = "/login";
  String registerPage = "/register";
  String forgotPasswordPage = "/forgotPassword";
  String dashboardPage = "/dasboard";
  String waFormPage = "/waForm";
  String otpVerificationPage = "/otp";
  String changePasswordSuccessPage = "/changePasswordSuccess";
  String categoryPage = "/category";
  String homePage = "/home";
  String subCategoryPage = "/subCategory";
  String detailPage = "/detailPage";
  String searchPage = "/search";
  String notificationPage = "/notification";
}
