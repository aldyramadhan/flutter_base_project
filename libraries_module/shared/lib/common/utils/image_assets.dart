import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ImageAssets {
  static const String baseImageString = "assets/images";
  String appLogo = '$baseImageString/ic_app_logo.png';
  String baseAppLogo = '$baseImageString/ic_base_app_logo.svg';
  String splashLogo = '$baseImageString/ic_splash_logo.png';
  String profile = '$baseImageString/ic_profile.svg';
  String category = '$baseImageString/ic_category.svg';
  String branda = '$baseImageString/ic_branda.svg';
  String search = '$baseImageString/ic_search.svg';
  String transaction = '$baseImageString/ic_transaction.svg';
  String google = '$baseImageString/ic_google.svg';
  String apple = '$baseImageString/ic_apple.png';
  String facebook = '$baseImageString/ic_fb.svg';
  String ceklisCircleOutline = '$baseImageString/ic_ceklis_circle_outline.svg';
  String wrongCircleOutline = '$baseImageString/ic_wrong_circle_outline.svg';
  String notification = '$baseImageString/ic_notification.svg';
  String emptyNotification = '$baseImageString/ic_empty_notification.svg';
  String help = '$baseImageString/ic_help.svg';
  String location = '$baseImageString/ic_location.svg';
  String passIllustration = '$baseImageString/ic_password_ilustration.png';
  String successIllustration = '$baseImageString/ic_success_illustration.png';
  String noNotificationIllustration = '$baseImageString/ic_no_notification_illustration.png';
  String arrowRight = '$baseImageString/ic_arrow_right.svg';
  String setting = '$baseImageString/ic_settings.svg';
  String headphones = '$baseImageString/ic_headphones.svg';
  String rating = '$baseImageString/ic_rating.svg';
  String email = '$baseImageString/ic_email.svg';

  IconData get leadingIcon{
    if(Platform.isAndroid){
      return Icons.arrow_back;
    } else {
      return CupertinoIcons.back;
    }
  }
}
