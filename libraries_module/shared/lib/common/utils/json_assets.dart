class JsonAssets {

  /// use this class to call assets
  static const String baseJsonString = "assets/json";
  final String covidLoading = '$baseJsonString/covidLoading.json';
  final String googleLoading = '$baseJsonString/lottie_google.json';
  final String appleLoading = '$baseJsonString/lottie_apple.json';
  final String facebookLoading = '$baseJsonString/lottie_facebook.json';
  final String loadingIndicator = '$baseJsonString/lottie_circle_indicator.json';

}