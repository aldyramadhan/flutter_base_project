# Base app

## Modularization Structure 🔥

    # Root Project
    .
    ├── features_module        # Name of directory
    |   ├── feature A          # Feature module with a clean architecture inside it.
    |   ├── feature B
    |   └── feature etc
    |
    ├── lib                    # Name of module (default from Flutter)
    |
    └── libraries_module       # Name of directory
        ├── core               # Core module.
        ├── dependencies       # Handle dependency version updates.
        └── shared             # Handle common utility class and custom widget.

## Clean Architecture Flow (Feature Module) 🔥
<pre>
<img src="screenshot/architecture.png">
</pre>

If you want to create a `* .g.dart` file, you can use this command in the terminal. But before you run it, make sure you enter the [core] module, using the `cd` command. For example `cd core`. Then you can run the below command.

### Run Flutter app between Flavor for development and production with command prompt

Development
```console
flutter run --flavor development --target=lib/ui/launcher/main-dev.dart
```
UAT
```console
flutter run --flavor development --target=lib/ui/launcher/main-uat.dart
```
Production
```console
flutter run --flavor production --target=lib/ui/launcher/main-prod.dart
```

### Flutter build app between Flavor for development and production with command prompt

Development
```console
flutter build apk --flavor development --target=lib/ui/launcher/main-dev.dart

flutter build apk --target-platform android-arm,android-arm64,android-x64 --split-per-abi --flavor development --target=lib/ui/launcher/main-dev.dart

flutter build appbundle --target-platform android-arm,android-arm64,android-x64 --flavor development --target=lib/ui/launcher/main-dev.dart
```
UAT
```console
flutter build apk --flavor development --target=lib/ui/launcher/main-uat.dart

flutter build apk --target-platform android-arm,android-arm64,android-x64 --split-per-abi --flavor development --target=lib/ui/launcher/main-uat.dart

flutter build appbundle --target-platform android-arm,android-arm64,android-x64 --flavor development --target=lib/ui/launcher/main-uat.dart
```
Production
```console
flutter build apk --flavor development --target=lib/ui/launcher/main-prod.dart

flutter build apk --target-platform android-arm,android-arm64,android-x64 --split-per-abi --flavor development --target=lib/ui/launcher/main-prod.dart

flutter build appbundle --target-platform android-arm,android-arm64,android-x64 --flavor development --target=lib/ui/launcher/main-prod.dart
```

One time build:
```console
flutter pub run build_runner build

flutter packages pub run build_runner build --delete-conflicting-outputs

```
or you can watch for changes and rebuild automatically
```console
flutter pub run build_runner watch
```

create flutter module
```console
flutter create -t module --org com.example my_flutter
```

run package flutter_launcher_icon
```console
flutter pub run flutter_launcher_icons:main
```
