import 'package:authentication/data/response/response.dart';
import 'package:checker/data/datasource/local_datasource.dart';
import 'package:checker/data/datasource/remote_datasource.dart';
import 'package:checker/domain/repositories/launch_repository.dart';
import 'package:shared/common/common.dart';

class LaunchRepositoryImpl extends LaunchRepository {
  final RemoteDataSource remote;
  final LocalDataSource local;

  LaunchRepositoryImpl(this.remote, this.local);

  @override
  Future<ResponseGetstarted> geStarted({Source source = Source.REMOTE}) async{
    if (source == Source.REMOTE)
      return await remote.geStarted();
    else
      return await local.geStarted();
  }
}