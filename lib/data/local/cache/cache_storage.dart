import 'dart:convert';

import 'package:authentication/data/response/response_getstarted.dart';
import 'package:checker/data/datasource/local_datasource.dart';
import 'package:core/local/local.dart';
import 'package:shared/common/base/base.dart';

class CacheStorage extends LocalDataSource with BaseView{
  final SharedPrefHelper pref;

  CacheStorage(this.pref);

  @override
  Future<ResponseGetstarted> geStarted() async{
    var fromCache = await pref.getCache(appConstant.getStartedKey);
    if (fromCache != null) {
      Map json = jsonDecode(fromCache);
      return ResponseGetstarted.fromJson(json);
    }
    return null;
  }

}