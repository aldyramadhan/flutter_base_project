import 'package:authentication/data/response/response.dart';

abstract class LocalDataSource {
  Future<ResponseGetstarted> geStarted();
}