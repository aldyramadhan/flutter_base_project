import 'package:authentication/data/response/response_getstarted.dart';

abstract class RemoteDataSource {
  Future<ResponseGetstarted> geStarted();
}