import 'dart:convert';

import 'package:authentication/data/response/response_getstarted.dart';
import 'package:checker/data/datasource/remote_datasource.dart';
import 'package:core/local/local.dart';
import 'package:core/network/network.dart';
import 'package:dependencies/dependencies.dart';
import 'package:shared/common/base/base.dart';

class LaunchApi extends RemoteDataSource with BaseView{
  final Dio dio;
  final SharedPrefHelper pref;

  LaunchApi(this.dio, this.pref);

  @override
  Future<ResponseGetstarted> geStarted() async{
    try {
      Response response = await dio.get(ApiConstant.getStartedApi);
      pref.storeCache(appConstant.getStartedKey, json.encode(response.data));
      return ResponseGetstarted.fromJson(response.data);
    } on DioError catch (e) {
      return null;
    }
  }

}