import 'package:authentication/data/entity/entity.dart';
import 'package:authentication/data/response/response.dart';
import 'package:dependencies/dependencies.dart';

abstract class LaunchState extends Equatable {
  const LaunchState();

  @override
  List<Object> get props => [];
}

class Initial extends LaunchState {}

// Launch state
class LaunchLoading extends LaunchState {}

class GetStartedHasData extends LaunchState {
  final ResponseGetstarted result;

  GetStartedHasData(this.result);

  @override
  List<Object> get props => [result];
}

class GetStartedNoData extends LaunchState {
  final String message;

  const GetStartedNoData(this.message);

  @override
  List<Object> get props => [message];
}

class NoInternetConnection extends LaunchState {}

class LaunchError extends LaunchState {
  final String errorMessage;

  LaunchError(this.errorMessage);

  @override
  List<Object> get props => [errorMessage];
}

// -- end
