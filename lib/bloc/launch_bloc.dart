import 'dart:convert';

import 'package:checker/domain/usecases/launch_usecases.dart';
import 'package:dependencies/dependencies.dart';
import 'package:flutter/material.dart';
import 'package:shared/common/base/base.dart';
import 'package:shared/common/common.dart';

import 'bloc.dart';

class LaunchBloc extends Bloc<LaunchEvent, LaunchState> with BaseView{
  final LaunchUseCase useCase;

  LaunchBloc(this.useCase) : super(Initial());

  @override
  Stream<LaunchState> mapEventToState(LaunchEvent event) async* {
    if (event is GetStarted) {
      yield* _mapGetStartedToState(source: event.source);
    }
  }

  //-- get started State
  Stream<LaunchState> _mapGetStartedToState({Source source}) async* {
    try {
      yield LaunchLoading();
      var getStartedService = await useCase.geStarted(source: source);
      if (getStartedService  != null) {
        yield GetStartedHasData(getStartedService);
      } else {
        yield LaunchError(null);
      }
    } on DioError catch (e) {
      if (e.type == DioErrorType.CONNECT_TIMEOUT ||
          e.type == DioErrorType.RECEIVE_TIMEOUT) {
        yield NoInternetConnection();
      } else if (e.type == DioErrorType.DEFAULT) {
        yield NoInternetConnection();
      } else {
        yield LaunchError(e.toString());
      }
    }
  }

}
