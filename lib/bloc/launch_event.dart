import 'package:dependencies/dependencies.dart';
import 'package:shared/common/common.dart';

abstract class LaunchEvent extends Equatable {
  const LaunchEvent();

  @override
  List<Object> get props => [];
}

class GetStarted extends LaunchEvent {
  final Source source;

  GetStarted({this.source});
}

