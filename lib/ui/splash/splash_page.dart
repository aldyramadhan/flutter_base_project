import 'dart:async';
import 'dart:io';
import 'package:authentication/data/response/response.dart';
import 'package:checker/bloc/bloc.dart';
import 'package:checker/ui/launcher/launcher.dart';
import 'package:dependencies/dependencies.dart';
import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';
import 'package:shared/common/base/base.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> with BaseView {
  ResponseGetstarted startedResult;

  Future<String> _getVersion() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    return info.version;
  }

  _startSplashScreen() async {
    var _duration = Duration(seconds: 2);
    return Timer(_duration, _navigationPage);
  }

  void _navigationPage() async{
    var isLogin = await pref.isLogin();
    if(isLogin) {
      navigation.toOffAll(Modular.get<NamedRoutes>().dashboardPage);
    } else {
      navigation.toOffAll(Modular.get<NamedRoutes>().onBoardingPage, argumentClass: startedResult);
    }
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, designSize: Size(750, 1334), allowFontScaling: true);
    return SafeArea(top: false, bottom: false,
        child: BlocProvider(
            create: (context) => Modular.get<LaunchBloc>()
              ..add(GetStarted(source: Source.REMOTE)),
            child: _buildPage()
        )
    );
  }

  Widget _buildPage() {
    return BlocListener<LaunchBloc, LaunchState>(
      listener: (context, state){
        if (state is GetStartedHasData) {
          startedResult = state.result;
          _startSplashScreen();
        }
      },
      child: BlocBuilder<LaunchBloc, LaunchState>(
        builder: (context, state) {
          return Scaffold(
            backgroundColor: colorPalettes.primaryBackground,
            body: Stack(
              children: [
                _splashLogo(state),
                _version()
              ],
            ),
          );
        },
      ),
    );
  }

  Widget _splashLogo(LaunchState state) => Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            Modular.get<ImageAssets>().splashLogo,
            fit: BoxFit.fill,
            height: 240.h,
          ),
          SizedBox(height: 36.h,),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 146.w),
            child: state is LaunchError ? null :
            LinearProgressIndicator(
              minHeight: 1.2,
              backgroundColor: colorPalettes.grey4,
            ),
          ),
          SizedBox(height: 70.h,),
          Container(
            child: state is LaunchError ? _buttonRefresh() : null,
          )
        ],
      )
  );

  Widget _version() => Align(
    alignment: Alignment.bottomCenter,
    child: FutureBuilder<String>(
      future: _getVersion(),
      builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
        var verInfo = "";
        if (snapshot.hasData) {
          if(Platform.isIOS){
            if(Config.isDebug) verInfo = "V ${snapshot.data}-Dev";
            else verInfo = "V ${snapshot.data}";
          } else verInfo = "V ${snapshot.data}";
        }
        return Container(
          margin: EdgeInsets.only(bottom: 30),
          child: Text(
            verInfo,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                color: colorPalettes.lightPrimary),
          ),
        );
      },
    ),
  );

  Widget _buttonRefresh() => Txt(appConstant.retry,
      style: styles.mediumPrimaryButtonStyle(
          true, width: MediaQuery.of(context).size.width / 2),
      gesture: buttonTap..onTap(() =>
      Modular.get<LaunchBloc>()..add(GetStarted()))
  );
}
