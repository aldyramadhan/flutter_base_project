import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'init.dart';
import 'launcher.dart';
import 'package:dependencies/dependencies.dart';

void main() async {
  Init.initBloc();
  Init.initModule();
  Init.initConfig(".prod");
  Init.initFirebase();
  Init.initPref();

  var _isDark;
  await ThemeHelper().getTheme().then((value) => _isDark = value);
  // SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
  //     statusBarBrightness: _isDark ? Brightness.dark : Brightness.light));

  runApp(
    CustomTheme(
      initialThemeKey: _isDark ? ThemesKeys.DARK : ThemesKeys.LIGHT,
      child: ModularApp(
        module: AppModul(),
      ),
    ),
  );
}
