import 'package:category/category.dart';
import 'package:checker/ui/splash/splash_page.dart';
import 'package:dashboard/dashboard.dart';
import 'package:dependencies/dependencies.dart';
import 'package:home/home.dart';
import 'package:shared/common/utils/named_routes.dart';
import 'package:authentication/authentication.dart';

var routersList = [
  ModularRouter(
    Modular.get<NamedRoutes>().splashPage,
    child: (context, args) => SplashPage(),
  ),
  ModularRouter(
    Modular.get<NamedRoutes>().onBoardingPage,
    module: FeatureAuthenticationModule(),
  ),
  ModularRouter(
    Modular.get<NamedRoutes>().dashboardPage,
    module: FeatureDashboardModule(),
  ),
  ModularRouter(
    Modular.get<NamedRoutes>().homePage,
    module: HomeModule(),
  ),
  ModularRouter(
    Modular.get<NamedRoutes>().categoryPage,
    module: CategoryModule(),
  ),
];