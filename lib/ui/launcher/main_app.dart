import 'package:checker/bloc/bloc.dart';
import 'package:checker/data/datasource/local_datasource.dart';
import 'package:checker/data/datasource/remote_datasource.dart';
import 'package:checker/data/local/cache/cache_storage.dart';
import 'package:checker/data/remote/network/launch_api.dart';
import 'package:checker/data/repository/launch_repository_impl.dart';
import 'package:checker/domain/repositories/launch_repository.dart';
import 'package:checker/domain/usecases/launch_usecases.dart';
import 'package:checker/ui/launcher/routers.dart';
import 'package:core/local/local.dart';
import 'package:dependencies/dependencies.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared/common/base/base.dart';
import 'package:shared/widget/loading/loading.dart';
import 'launcher.dart';
import 'package:shared/common/common.dart';
import 'package:authentication/im_authentication.dart';
import 'package:home/im_home.dart';

class AppModul extends MainModule {
  @override
  List<Bind> get binds => [
    Bind((_) => LaunchApi(Modular.get<Dio>(), Modular.get<SharedPrefHelper>())),
    Bind((_) => CacheStorage(Modular.get<SharedPrefHelper>())),
    Bind((_) => LaunchRepositoryImpl(
        Modular.get<RemoteDataSource>(), Modular.get<LocalDataSource>())),
    Bind((_) => LaunchUseCaseImpl(Modular.get<LaunchRepository>())),
    Bind((_) => LaunchBloc(Modular.get<LaunchUseCase>())),

    // -- auth
    Bind((_) => AuthenticationApi(dio: Modular.get<Dio>())),
    Bind((_) => AuthenticationRepositoryImpl(dataSource: Modular.get<AuthenticationDataSource>())),
    Bind((_) => AuthenticationUseCaseImpl(repository: Modular.get<AuthenticationRepository>())),
    Bind((_) => AuthenticationCubit(useCase: Modular.get<AuthenticationUseCase>())),

    // -- home
    Bind((_) => HomeApi(dio: Modular.get<Dio>())),
    Bind((_) => HomeRepositoryImpl(dataSource: Modular.get<HomeDataSource>())),
    Bind((_) => HomeUseCaseImpl(repository: Modular.get<HomeRepository>())),
    Bind((_) => HomeCubit(useCase: Modular.get<HomeUseCase>())),

  ];

  @override
  Widget get bootstrap => EasyLocalization(
      path: 'assets/languages',
      supportedLocales: [Locale('en', 'US'), Locale('id', 'ID')],
      child: MainApp());

  @override
  List<ModularRouter> get routers => routersList;
}

class MainApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MainAppState();
}

class MainAppState extends State<MainApp> with BaseView {
  @override
  void initState() {
    super.initState();
    pref.saveLogin(false);
    // RestManager.initAPIManager(context);
    // DeviceHelper().setDeviceInfo();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarBrightness: Brightness.light,
        statusBarIconBrightness: Brightness.dark,
        statusBarColor: Colors.transparent));
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => Modular.get<AuthenticationCubit>()),
        BlocProvider(create: (context) => Modular.get<HomeCubit>()),
      ],
      child: materialApp(),
    );
  }

  Widget materialApp() {
    return GlobalLoaderOverlay(
        useDefaultLoading: false,
        overlayOpacity: 0.5,
        overlayWidget: LottieScreenLoading(
          lottieAssets: jsonAsset.loadingIndicator,
        ),
        child: MaterialApp(
          title: Config.title,
          builder: BotToastInit(),
          debugShowCheckedModeBanner: Config.isDebug,
          // theme: CustomTheme.of(context), // use this if you want make dynamic theme
          theme: ThemeData(fontFamily: 'Nunito-font'),
          initialRoute: Modular.get<NamedRoutes>().splashPage,
          onGenerateRoute: Modular.generateRoute,
          navigatorKey: Modular.navigatorKey,
          navigatorObservers: [BotToastNavigatorObserver()],
          // navigatorObservers: [analytics.getAnalyticsObserver()],
          localizationsDelegates: context.localizationDelegates,
          supportedLocales: context.supportedLocales,
          locale: context.locale,
        ));
  }
}
