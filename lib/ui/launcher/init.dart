import 'package:authentication/authentication.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../ui.dart';
import 'package:core/core.dart';
import 'package:dependencies/dependencies.dart';

class Init {
  static void initBloc() async {
    WidgetsFlutterBinding.ensureInitialized();
    Bloc.observer = AppBlocObserver();
  }

  static void initModule() async {
    final preferences = await SharedPreferences.getInstance();
    Modular.init(SharedModule());
    Modular.init(CoreModule(preferences: preferences));
    // Modular.init(FeatureAuthenticationModule());
  }

  static void initFirebase() async {
    // Firebase.initializeApp();
  }

  static void initConfig(String config) {
    switch (config) {
      case ".dev":
        Config.appFlavor = Flavor.DEVELOPMENT;
        return;
      case ".uat":
        Config.appFlavor = Flavor.UAT;
        return;
      case ".prod":
        Config.appFlavor = Flavor.RELEASE;
        return;
    }
  }

  static void initPref() async {
    // await PrefHelper.init();
  }

  void initStatusBar(bool isDark) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: Colors.blue,
        statusBarBrightness: isDark ? Brightness.dark : Brightness.light));
  }
}
