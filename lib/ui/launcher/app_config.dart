import 'package:shared/common/common.dart';
import 'package:core/network/network.dart';

enum Flavor {
  DEVELOPMENT,
  RELEASE,
  UAT
}

class Config {
  static Flavor appFlavor;

  static String get title {
    switch (appFlavor) {
      case Flavor.RELEASE:
        return AppConstant().appName;
      case Flavor.DEVELOPMENT:
        return AppConstant().appNameDev;
      case Flavor.UAT:
        return AppConstant().appNameDev;
      default:
        return AppConstant().appNameDev;
    }
  }

  static String get baseUrl {
    switch (appFlavor) {
      case Flavor.DEVELOPMENT:
        return ApiConstant.baseUrlDevelopmentApi;
      case Flavor.RELEASE:
        return ApiConstant.baseUrlProductionApi;
      case Flavor.UAT:
        return ApiConstant.baseUrlUatApi;
      default:
        return ApiConstant.baseUrlDevelopmentApi;
    }
  }

  static bool get isDebug {
    switch (appFlavor) {
      case Flavor.RELEASE:
        return false;
      case Flavor.DEVELOPMENT:
      default:
      return true;
    }
  }
}
