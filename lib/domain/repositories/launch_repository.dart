import 'package:authentication/data/response/response.dart';
import 'package:shared/common/common.dart';

abstract class LaunchRepository {
  Future<ResponseGetstarted> geStarted({Source source = Source.REMOTE});
}