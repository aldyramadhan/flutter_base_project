import 'package:authentication/data/response/response.dart';
import 'package:checker/domain/repositories/launch_repository.dart';
import 'package:shared/common/common.dart';

abstract class LaunchUseCase {
  Future<ResponseGetstarted> geStarted({Source source = Source.REMOTE});
}

class LaunchUseCaseImpl extends LaunchUseCase {
  final LaunchRepository launchRepository;

  LaunchUseCaseImpl(this.launchRepository);

  @override
  Future<ResponseGetstarted> geStarted({Source source = Source.REMOTE}) =>
      launchRepository.geStarted(source: source);

}