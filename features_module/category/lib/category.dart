import 'package:category/presentation/ui/detail/detail_page.dart';
import 'package:category/presentation/ui/subcategory/subcategory_page.dart';
import 'package:dependencies/dependencies.dart';
import 'package:shared/common/common.dart';

class CategoryModule extends ChildModule {
  @override
  List<Bind> get binds => [];

  @override
  List<ModularRouter> get routers => [
    ModularRouter(
      Modular.get<NamedRoutes>().subCategoryPage,
      child: (context, arg) => SubCategoryPage(),
    ),
    ModularRouter(
      Modular.get<NamedRoutes>().detailPage,
      child: (context, arg) => DetailPage(),
    ),
  ];
}