import 'package:dependencies/dependencies.dart';
import 'package:flutter/material.dart';
import 'package:shared/common/base/base.dart';
import 'package:shared/common/common.dart';
import 'package:shared/widget/appbar/simple_app_bar.dart';
import 'package:shared/widget/card/card.dart';

class SubCategoryPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SubCategoryPageState();
}

class SubCategoryPageState extends State<SubCategoryPage> with BaseView{

  void goTo(String route) {
    navigation.linkTo(route);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          backgroundColor: colorPalettes.white,
          appBar: _appBar,
          body: _content(),
        )
    );
  }

  Widget get _appBar => PreferredSize(
      preferredSize: Size.fromHeight(60),
      child: SimpleAppBar(
        color: colorPalettes.white,
        centerTitle: true,
        leading: InkWell(
            onTap: () => navigation.toPop(),
            child: Icon(
              imageAsset.leadingIcon,
              color: colorPalettes.black,
            )
        ),
        title: Txt(appConstant.subCategory,
          style: styles.textLargeStyle(colorPalettes.primaryText, bold: true),
        ),
      )
  );

  Widget _content() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(padding: EdgeInsets.symmetric(horizontal: 16),
          child: Txt(appConstant.subCategory,
            style: styles.textNormalBoldStyle(colorPalettes.primaryText),
          ),
        ),
        _subCategoryProduct()
      ],
    );
  }

  Widget _subCategoryProduct() {
    return Expanded(
      child: GridView.builder(
        itemCount: 10,
        physics: BouncingScrollPhysics(),
        padding: EdgeInsets.symmetric(horizontal: 12, vertical: 14),
        gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 1 / 1.4
        ),
        itemBuilder: (BuildContext context, int index) => SubCategoryCardItem(
          detail: () => goTo(Modular.get<NamedRoutes>().detailPage),
        ),
      ),
    );
  }
}