import 'package:dependencies/dependencies.dart';
import 'package:flutter/material.dart';
import 'package:shared/common/base/base.dart';
import 'package:shared/widget/appbar/simple_app_bar.dart';
import 'package:shared/widget/card/card.dart';

class DetailPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => DetailPageState();
}

class DetailPageState extends State<DetailPage> with BaseView{
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          backgroundColor: colorPalettes.white,
          appBar: _appBar,
          body: _content(),
          bottomSheet: _bottomBarButton(),
        )
    );
  }

  Widget get _appBar => PreferredSize(
      preferredSize: Size.fromHeight(60),
      child: SimpleAppBar(
        color: colorPalettes.white,
        centerTitle: true,
        leading: InkWell(
            onTap: () => navigation.toPop(),
            child: Icon(
              imageAsset.leadingIcon,
              color: colorPalettes.black,
            )
        ),
        title: Txt(appConstant.detail,
          style: styles.textLargeStyle(colorPalettes.primaryText, bold: true),
        ),
      )
  );

  Widget _content() {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _detailSlider(),
          _summeryWording()
        ],
      ),
    );
  }

  Widget _detailSlider() {
    return Container(
      height: 350.h,
      color: colorPalettes.textBluePrimary,
    );
  }

  Widget _summeryWording() {
    return Padding(
      padding: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Txt('Burung beo',
            style: styles.textNormalBoldStyle(colorPalettes.primaryText),
          ),
          Txt('Hewan - Burung',
            style: styles.textSmallStyle(colorPalettes.secondaryText),
          ),
          SizedBox(height: 24),
          Txt(appConstant.description,
            style: styles.textNormalBoldStyle(colorPalettes.primaryText),
          ),
          SizedBox(height: 6),
          Txt('Bisa Fasih Bicara Jika Terus Berlatih Beo termasuk hewan cerdas dan bisa semakin fasih berbicara jika terus-menerus dilatih oleh manusia. Lidah beo dapat membedakan beberapa huruf vokal, sehingga juga dapat membuatnya mengucapkan berbagai kata yang mirip seperti yang diucapkan manusia.',
            style: styles.textSmallStyle(colorPalettes.secondaryText),
          ),
        ],
      ),
    );
  }

  Widget _bottomBarButton() {
    return Container(
      height: 100.h,
      padding: EdgeInsets.symmetric(horizontal: 24, vertical: 8),
      decoration: new BoxDecoration(
        color: colorPalettes.white,
        boxShadow: [
          BoxShadow(
            color: colorPalettes.grey,
            blurRadius: 25.0,
            spreadRadius: 5.0,
            offset: Offset(15.0, 15.0,
            ),
          )
        ],
      ),
      child: Txt(appConstant.checkgoods,
          gesture: buttonTap..onTap(() {}),
          style: styles.mediumPrimaryButtonStyle(true)),
    );
  }
}