import 'package:dependencies/dependencies.dart';
import 'package:flutter/material.dart';
import 'package:shared/common/base/base.dart';
import 'package:shared/common/common.dart';
import 'package:shared/widget/appbar/simple_app_bar.dart';
import 'package:shared/widget/card/card.dart';

class CategoryPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => CategoryPageState();
}

class CategoryPageState extends State<CategoryPage> with BaseView, AutomaticKeepAliveClientMixin{

  void goTo(String route) {
    navigation.to(route);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          backgroundColor: colorPalettes.white,
          appBar: _appBar,
          body: _content(),
        )
    );
  }

  Widget get _appBar => PreferredSize(
      preferredSize: Size.fromHeight(60),
      child: SimpleAppBar(
        color: colorPalettes.white,
        title: Txt(appConstant.category,
          style: styles.textLargeStyle(colorPalettes.primaryText, bold: true),
        ),
      )
  );
  
  Widget _content() {
    return Column(
      children: [
        SizedBox(height: 10),
        _categoryFilter(),
        SizedBox(height: 10),
        _categoryProduct()
      ],
    );
  }

  Widget _categoryFilter() {
    return ConstrainedBox(
      constraints: BoxConstraints(maxHeight: 126.h, minHeight: 70.h),
      child: ListView.builder(
          itemCount: 10,
          physics: BouncingScrollPhysics(),
          scrollDirection: Axis.horizontal,
          padding: EdgeInsets.symmetric(horizontal: 12),
          itemBuilder: (BuildContext context, int index) => CategoryFilterCardItem()
      ),
    );
  }

  Widget _categoryProduct() {
    return Expanded(
      child: ListView.builder(
          itemCount: 10,
          physics: BouncingScrollPhysics(),
          padding: EdgeInsets.symmetric(horizontal: 12),
          itemBuilder: (BuildContext context, int index) => CategoryCardItem(
            seeAll: () => goTo(Modular.get<NamedRoutes>().subCategoryPage.category),
          )
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}