import 'package:core/local/local.dart';
import 'package:dependencies/dependencies.dart';
import 'package:flutter/material.dart';
import 'package:shared/common/base/base.dart';
import 'package:shared/widget/card/profile_menu_item.dart';
import 'package:shared/widget/shimmer/shimmer_container.dart';

class AccountPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => AccountPageState();
}

class AccountPageState extends State<AccountPage> with BaseView{

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          backgroundColor: colorPalettes.white,
          body: _content(),
        )
    );
  }

  Widget _content() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 60),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Txt(appConstant.myProfile,
            style: styles.textLargeStyle(colorPalettes.primaryText, bold: true),
          ),
          SizedBox(height: 20),
          _profileMenu()
        ],
      ),
    );
  }

  Widget _profileMenu() {
    return Container(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _profileImage(),
              SizedBox(height: 26),
              ListView.builder(
                  shrinkWrap: true,
                  itemCount: profileMenuJson.length,
                  physics: BouncingScrollPhysics(),
                  padding: EdgeInsets.symmetric(horizontal: 12),
                  itemBuilder: (BuildContext context, int index) => ProfileMenuItem(
                      image: profileMenuJson[index]['icon'],
                      title: profileMenuJson[index]['title'],
                      height: profileMenuJson[index]['height'],
                      width: profileMenuJson[index]['width']
                  )
              ),
            ],
          ),
        )
    );
  }

  Widget _profileImage() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(90)),
            child: SizedBox(
                height: 65,
                width: 65,
                child: CachedNetworkImage(
                  imageUrl: getString("https://image.cermati.com/q_70,w_1200,h_800,c_fit/ltm2cjaldbigxtxltuv9"),
                  fit: BoxFit.fill,
                  width: double.infinity,
                  placeholder: (context, url) => ShimmerContainer(),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                )
            )
        ),
        SizedBox(width: 12),
        Expanded(
            child: Txt('Muhammad',
              style: styles.textMediumBoldStyle(colorPalettes.primaryText),
            ),
        ),
        SvgPicture.asset(imageAsset.arrowRight, width: 24, height: 24)
      ],
    );
  }
}