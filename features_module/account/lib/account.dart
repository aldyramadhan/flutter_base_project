import 'package:dependencies/dependencies.dart';

class AccountModule extends ChildModule {
  @override
  List<Bind> get binds => [];

  @override
  List<ModularRouter> get routers => [];
}