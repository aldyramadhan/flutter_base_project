import 'package:flutter/material.dart';
import 'package:dependencies/dependencies.dart';
import 'package:home/im_home.dart';
import 'package:shared/common/base/base.dart';
import 'package:shared/common/common.dart';
import 'package:shared/widget/widget.dart';
import 'package:category/presentation/ui/category/category_page.dart';
import 'package:account/presentation/ui/account/account_page.dart';
import 'package:transaction/presentation/ui/transaction/transaction_page.dart';

class CheckrDashBoard extends StatefulWidget {
  @override
  CheckrDashBoardState createState() => CheckrDashBoardState();
}

class CheckrDashBoardState extends State<CheckrDashBoard> with BaseView {
  PageController _pageController;
  int page = 0;

  void navigationTapped(int page) {
    _pageController.jumpToPage(page);
    setState(() => this.page = page);
  }

  @override
  void initState() {
    super.initState();
    _pageController = PageController(initialPage: 0);
  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }

  @override
  Widget build(BuildContext context) => Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            stops: [0.9,  0.1],
            colors: [
              colorPalettes.white,
              colorPalettes.fieldBackground
            ],
          )),
      child: SafeArea(
          child: Scaffold(
              resizeToAvoidBottomInset: false,
              body: PageView(
                  physics: NeverScrollableScrollPhysics(),
                  controller: _pageController,
                  onPageChanged: navigationTapped,
                  children: <Widget>[
                    HomePage(),
                    CategoryPage(),
                    TransactionPage(),
                    AccountPage()
                  ]
              ),
              floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
              floatingActionButton: Container(
                  height: 50,
                  width: 50,
                  child: FloatingActionButton(
                    onPressed: () {},
                    backgroundColor: colorPalettes.lightPrimary,
                    child: SvgPicture.asset(
                      Modular.get<ImageAssets>().baseAppLogo,
                      width: 38.w,
                    ),
                    elevation: 1,
                  )),
              bottomNavigationBar: SizedBox(
                  height: 70,
                  child: ClipRRect(
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(20),
                        topLeft: Radius.circular(20),
                      ),
                      child: FABBottomAppBar(
                          centerItemText: Modular.get<AppConstant>().startCheck,
                          color: Colors.grey,
                          height: 70,
                          backgroundColor: colorPalettes.fieldBackground,
                          selectedColor: colorPalettes.lightPrimary,
                          onTabSelected: navigationTapped,
                          iconSize: 23,
                          items: [
                            FABBottomAppBarItem(
                                icon: Modular.get<ImageAssets>().branda,
                                text: Modular.get<AppConstant>().branda),
                            FABBottomAppBarItem(
                                icon: Modular.get<ImageAssets>().category,
                                text: Modular.get<AppConstant>().category),
                            FABBottomAppBarItem(
                                icon: Modular.get<ImageAssets>().transaction,
                                text: Modular.get<AppConstant>().transaction),
                            FABBottomAppBarItem(
                                icon: Modular.get<ImageAssets>().profile,
                                text: Modular.get<AppConstant>().account),
                          ])
                  ))
          )
      )
  );
}
