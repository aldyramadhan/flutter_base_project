import 'package:dashboard/presentation/ui/checkr_dashboard.dart';
import 'package:dependencies/dependencies.dart';
import 'package:shared/common/common.dart';


class FeatureDashboardModule extends ChildModule {
  @override
  List<Bind> get binds => [];

  @override
  List<ModularRouter> get routers => [
    ModularRouter(
      Modular.get<NamedRoutes>().dashboardPage,
      child: (context, arg) => CheckrDashBoard(),
    ),
  ];
}
