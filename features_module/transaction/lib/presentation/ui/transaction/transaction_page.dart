import 'package:dependencies/dependencies.dart';
import 'package:flutter/material.dart';
import 'package:shared/common/base/base.dart';
import 'package:transaction/presentation/ui/transaction/transaction_finish.dart';
import 'package:transaction/presentation/ui/transaction/transaction_pending.dart';

class TransactionPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => TransactionPageState();
}

class TransactionPageState extends State<TransactionPage>
    with BaseView, SingleTickerProviderStateMixin{
  TabController controller;

  @override
  void initState() {
    super.initState();
    controller = new TabController(vsync: this, length: 2);
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          backgroundColor: colorPalettes.white,
          body: _content(),
        )
    );
  }

  Widget _content() {
    return Container(
      padding: EdgeInsets.only(top: 60),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: Txt(appConstant.transactionHistory,
              style: styles.textLargeStyle(colorPalettes.primaryText, bold: true),
            ),
          ),
          SizedBox(height: 24),
          _tab(),
          _pageView()
        ],
      ),
    );
  }

  Widget _tab(){
    return Container(
      child: TabBar(
        controller: controller,
        labelColor: colorPalettes.lightPrimary,
        unselectedLabelColor: colorPalettes.grey,
        tabs: [
          Tab(
            text: appConstant.pending,
          ),
          Tab(
            text: appConstant.finish,
          )
        ],
      ),
    );
  }

  Widget _pageView() {
    return Expanded(
        child: TabBarView(
          controller: controller,
          children: [
            TransactionPending(),
            TransactionFinish()
          ],
        )
    );
  }

}