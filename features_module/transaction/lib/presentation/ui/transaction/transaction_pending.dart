import 'package:flutter/cupertino.dart';
import 'package:shared/common/base/base.dart';
import 'package:shared/widget/widget.dart';

class TransactionPending extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => TransactionPendingState();
}

class TransactionPendingState extends State<TransactionPending>
    with BaseView, AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
          itemCount: 10,
          physics: BouncingScrollPhysics(),
          padding: EdgeInsets.only(top: 12, bottom: 40),
          itemBuilder: (BuildContext context, int index) => TransactionItem()
      ),
    );
  }

  @override
  bool get wantKeepAlive => false;
}
