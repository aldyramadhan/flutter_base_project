import 'package:dependencies/dependencies.dart';

class CategoryModule extends ChildModule {
  @override
  List<Bind> get binds => [];

  @override
  List<ModularRouter> get routers => [];
}