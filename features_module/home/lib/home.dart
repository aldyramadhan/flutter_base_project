import 'package:dependencies/dependencies.dart';
import 'package:home/presentation/ui/notification/notification_page.dart';
import 'package:home/presentation/ui/search/search_page.dart';
import 'package:shared/common/common.dart';

class HomeModule extends ChildModule {
  @override
  List<Bind> get binds => [];

  @override
  List<ModularRouter> get routers => [
    ModularRouter(
      Modular.get<NamedRoutes>().searchPage,
      child: (context, arg) => SearchPage(),
    ),
    ModularRouter(
      Modular.get<NamedRoutes>().notificationPage,
      child: (context, arg) => NotificationPage(),
    )
  ];
}