import 'package:home/data/entity/entity.dart';
import 'package:json_annotation/json_annotation.dart';

part 'response_home.g.dart';

@JsonSerializable()
class ResponseHome  {
  @JsonKey(name: 'success')
  bool success;

  @JsonKey(name: 'message')
  String message;

  @JsonKey(name: 'data')
  final HomePageData data;

  ResponseHome([this.success, this.message, this.data]);

  factory ResponseHome.fromJson(Map<String, dynamic> json) =>
      _$ResponseHomeFromJson(json);

  Map<String, dynamic> toJson() => _$ResponseHomeToJson(this);
}
