// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'response_home.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ResponseHome _$ResponseHomeFromJson(Map<String, dynamic> json) {
  return ResponseHome(
    json['success'] as bool,
    json['message'] as String,
    json['data'] == null
        ? null
        : HomePageData.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ResponseHomeToJson(ResponseHome instance) =>
    <String, dynamic>{
      'success': instance.success,
      'message': instance.message,
      'data': instance.data,
    };
