// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'advertisement.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Advertisement _$AdvertisementFromJson(Map<String, dynamic> json) {
  return Advertisement(
    json['title'] as String,
    json['description'] as String,
    (json['items'] as List)
        ?.map((e) => e == null
            ? null
            : AdvertisementItem.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$AdvertisementToJson(Advertisement instance) =>
    <String, dynamic>{
      'title': instance.title,
      'description': instance.description,
      'items': instance.items,
    };
