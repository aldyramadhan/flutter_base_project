// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'advertisement_items.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AdvertisementItem _$AdvertisementItemFromJson(Map<String, dynamic> json) {
  return AdvertisementItem(
    json['icon'] as String,
    json['title'] as String,
  );
}

Map<String, dynamic> _$AdvertisementItemToJson(AdvertisementItem instance) =>
    <String, dynamic>{
      'icon': instance.icon,
      'title': instance.title,
    };
