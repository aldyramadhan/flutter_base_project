import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

import 'advertisement_items.dart';

part 'advertisement.g.dart';

@JsonSerializable()
class Advertisement extends Equatable {
  @JsonKey(name: 'title')
  String title;

  @JsonKey(name: 'description')
  String description;

  @JsonKey(name: 'items')
  List<AdvertisementItem> items;

  Advertisement([this.title, this.description, this.items]);

  @override
  List<Object> get props => [title, description, items];

  factory Advertisement.fromJson(Map<String, dynamic> json) => _$AdvertisementFromJson(json);

  Map<String, dynamic> toJson() => _$AdvertisementToJson(this);
}
