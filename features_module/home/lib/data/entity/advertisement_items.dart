import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'advertisement_items.g.dart';

@JsonSerializable()
class AdvertisementItem extends Equatable {
  @JsonKey(name: 'icon')
  String icon;

  @JsonKey(name: 'title')
  String title;

  AdvertisementItem([this.icon, this.title]);

  @override
  List<Object> get props => [title, title];

  factory AdvertisementItem.fromJson(Map<String, dynamic> json) => _$AdvertisementItemFromJson(json);

  Map<String, dynamic> toJson() => _$AdvertisementItemToJson(this);
}
