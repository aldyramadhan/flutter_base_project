import 'package:home/data/entity/advertisement.dart';
import 'package:home/data/entity/article.dart';
import 'package:home/data/entity/popular_item.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:core/network/network.dart';

import 'banner.dart';

part 'home_page_data.g.dart';

@JsonSerializable()
class HomePageData {
  @JsonKey(name: 'users')
  Users users;

  @JsonKey(name: 'banner')
  List<Banner> banner;

  @JsonKey(name: 'advertisement')
  Advertisement advertisement;

  @JsonKey(name: 'popular_items')
  List<PopularItem> popularItems;

  @JsonKey(name: 'article')
  List<Article> article;

  HomePageData(
      [this.users, this.banner, this.advertisement, this.popularItems, this.article]);

  factory HomePageData.fromJson(Map<String, dynamic> json) =>
      _$HomePageDataFromJson(json);

  Map<String, dynamic> toJson() => _$HomePageDataToJson(this);
}
