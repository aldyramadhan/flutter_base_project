// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_page_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HomePageData _$HomePageDataFromJson(Map<String, dynamic> json) {
  return HomePageData(
    json['users'] == null
        ? null
        : Users.fromJson(json['users'] as Map<String, dynamic>),
    (json['banner'] as List)
        ?.map((e) =>
            e == null ? null : Banner.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['advertisement'] == null
        ? null
        : Advertisement.fromJson(json['advertisement'] as Map<String, dynamic>),
    (json['popular_items'] as List)
        ?.map((e) =>
            e == null ? null : PopularItem.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    (json['article'] as List)
        ?.map((e) =>
            e == null ? null : Article.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$HomePageDataToJson(HomePageData instance) =>
    <String, dynamic>{
      'users': instance.users,
      'banner': instance.banner,
      'advertisement': instance.advertisement,
      'popular_items': instance.popularItems,
      'article': instance.article,
    };
