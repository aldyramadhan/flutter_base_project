import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:core/network/network.dart';

part 'banner.g.dart';

@JsonSerializable()
class Banner extends Equatable {
  @JsonKey(name: 'image')
  String image;

  Banner([this.image]);

  @override
  List<Object> get props => [image];

  factory Banner.fromJson(Map<String, dynamic> json) => _$BannerFromJson(json);

  Map<String, dynamic> toJson() => _$BannerToJson(this);
}
