// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'popular_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PopularItem _$PopularItemFromJson(Map<String, dynamic> json) {
  return PopularItem(
    json['image'] as String,
    json['name'] as String,
    json['sub_category'] as String,
  );
}

Map<String, dynamic> _$PopularItemToJson(PopularItem instance) =>
    <String, dynamic>{
      'image': instance.image,
      'name': instance.name,
      'sub_category': instance.subCategory,
    };
