import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'popular_item.g.dart';

@JsonSerializable()
class PopularItem extends Equatable {
  @JsonKey(name: 'image')
  String image;

  @JsonKey(name: 'name')
  String name;

  @JsonKey(name: 'sub_category')
  String subCategory;

  PopularItem([this.image, this.name, this.subCategory]);

  @override
  List<Object> get props => [image, name, subCategory];

  factory PopularItem.fromJson(Map<String, dynamic> json) => _$PopularItemFromJson(json);

  Map<String, dynamic> toJson() => _$PopularItemToJson(this);
}
