import 'package:core/network/network.dart';
import 'package:home/data/response/response_home.dart';

abstract class HomeDataSource {
  Future<ApiResult<ResponseHome>> getHome();
}