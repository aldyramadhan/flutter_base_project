
import 'package:core/network/api_result.dart';
import 'package:flutter/cupertino.dart';

import 'package:home/data/response/response_home.dart';

import '../../domain/repositories/home_repository.dart';
import '../datasource/home_datasource.dart';

class HomeRepositoryImpl extends HomeRepository {
  final HomeDataSource dataSource;

  HomeRepositoryImpl({@required this.dataSource});

  @override
  Future<ApiResult<ResponseHome>> getHome() async{
    return await dataSource.getHome();
  }


}
