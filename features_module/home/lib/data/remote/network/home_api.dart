import 'package:core/network/api_result.dart';
import 'package:core/network/network.dart';
import 'package:dependencies/dependencies.dart';
import 'package:home/data/datasource/home_datasource.dart';
import 'package:home/data/response/response_home.dart';

class HomeApi extends HomeDataSource {
  final Dio dio;

  HomeApi({this.dio});

  @override
  Future<ApiResult<ResponseHome>> getHome() async{
    try {
      Response response = await dio.get(ApiConstant.homeApi);
      return ApiResult.success(data: ResponseHome.fromJson(response.data));
    } on DioError catch (e) {
      return ApiResult.failure(error: NetworkExceptions.getDioException(e));
    }
  }

}