import 'package:core/network/network.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'home_state.freezed.dart';

@freezed
abstract class HomeState<T> with _$HomeState<T> {
  const factory HomeState.idle() = Idle<T>;

  //-- home
  const factory HomeState.homeLoading() = HomeLoading<T>;
  const factory HomeState.homeHashData({@required T data}) = HomeHashData<T>;
  const factory HomeState.errorHome({@required NetworkExceptions error}) = ErrorHome<T>;
}