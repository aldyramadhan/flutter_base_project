
import 'package:core/network/network.dart';
import 'package:dependencies/dependencies.dart';
import 'package:home/data/response/response_home.dart';
import 'package:home/domain/usecases/home_usecases.dart';

import 'home_state.dart';

class HomeCubit extends Cubit<HomeState> {
  final HomeUseCase useCase;

  HomeCubit({this.useCase}): assert(useCase != null), super(Idle());

  getHome() async {
    emit(HomeState.homeLoading());
    var homeService = await useCase.getHome();
    homeService.when(success: (ResponseHome data) {
      emit(HomeState.homeHashData(data: data));
    }, failure: (NetworkExceptions error) {
      emit(HomeState.errorHome(error: error));
    });
  }

}