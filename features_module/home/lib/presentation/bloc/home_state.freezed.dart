// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'home_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$HomeStateTearOff {
  const _$HomeStateTearOff();

  Idle<T> idle<T>() {
    return Idle<T>();
  }

  HomeLoading<T> homeLoading<T>() {
    return HomeLoading<T>();
  }

  HomeHashData<T> homeHashData<T>({@required T data}) {
    return HomeHashData<T>(
      data: data,
    );
  }

  ErrorHome<T> errorHome<T>({@required NetworkExceptions error}) {
    return ErrorHome<T>(
      error: error,
    );
  }
}

// ignore: unused_element
const $HomeState = _$HomeStateTearOff();

mixin _$HomeState<T> {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result idle(),
    @required Result homeLoading(),
    @required Result homeHashData(T data),
    @required Result errorHome(NetworkExceptions error),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result idle(),
    Result homeLoading(),
    Result homeHashData(T data),
    Result errorHome(NetworkExceptions error),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result idle(Idle<T> value),
    @required Result homeLoading(HomeLoading<T> value),
    @required Result homeHashData(HomeHashData<T> value),
    @required Result errorHome(ErrorHome<T> value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result idle(Idle<T> value),
    Result homeLoading(HomeLoading<T> value),
    Result homeHashData(HomeHashData<T> value),
    Result errorHome(ErrorHome<T> value),
    @required Result orElse(),
  });
}

abstract class $HomeStateCopyWith<T, $Res> {
  factory $HomeStateCopyWith(
          HomeState<T> value, $Res Function(HomeState<T>) then) =
      _$HomeStateCopyWithImpl<T, $Res>;
}

class _$HomeStateCopyWithImpl<T, $Res> implements $HomeStateCopyWith<T, $Res> {
  _$HomeStateCopyWithImpl(this._value, this._then);

  final HomeState<T> _value;
  // ignore: unused_field
  final $Res Function(HomeState<T>) _then;
}

abstract class $IdleCopyWith<T, $Res> {
  factory $IdleCopyWith(Idle<T> value, $Res Function(Idle<T>) then) =
      _$IdleCopyWithImpl<T, $Res>;
}

class _$IdleCopyWithImpl<T, $Res> extends _$HomeStateCopyWithImpl<T, $Res>
    implements $IdleCopyWith<T, $Res> {
  _$IdleCopyWithImpl(Idle<T> _value, $Res Function(Idle<T>) _then)
      : super(_value, (v) => _then(v as Idle<T>));

  @override
  Idle<T> get _value => super._value as Idle<T>;
}

class _$Idle<T> implements Idle<T> {
  const _$Idle();

  @override
  String toString() {
    return 'HomeState<$T>.idle()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is Idle<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result idle(),
    @required Result homeLoading(),
    @required Result homeHashData(T data),
    @required Result errorHome(NetworkExceptions error),
  }) {
    assert(idle != null);
    assert(homeLoading != null);
    assert(homeHashData != null);
    assert(errorHome != null);
    return idle();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result idle(),
    Result homeLoading(),
    Result homeHashData(T data),
    Result errorHome(NetworkExceptions error),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (idle != null) {
      return idle();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result idle(Idle<T> value),
    @required Result homeLoading(HomeLoading<T> value),
    @required Result homeHashData(HomeHashData<T> value),
    @required Result errorHome(ErrorHome<T> value),
  }) {
    assert(idle != null);
    assert(homeLoading != null);
    assert(homeHashData != null);
    assert(errorHome != null);
    return idle(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result idle(Idle<T> value),
    Result homeLoading(HomeLoading<T> value),
    Result homeHashData(HomeHashData<T> value),
    Result errorHome(ErrorHome<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (idle != null) {
      return idle(this);
    }
    return orElse();
  }
}

abstract class Idle<T> implements HomeState<T> {
  const factory Idle() = _$Idle<T>;
}

abstract class $HomeLoadingCopyWith<T, $Res> {
  factory $HomeLoadingCopyWith(
          HomeLoading<T> value, $Res Function(HomeLoading<T>) then) =
      _$HomeLoadingCopyWithImpl<T, $Res>;
}

class _$HomeLoadingCopyWithImpl<T, $Res>
    extends _$HomeStateCopyWithImpl<T, $Res>
    implements $HomeLoadingCopyWith<T, $Res> {
  _$HomeLoadingCopyWithImpl(
      HomeLoading<T> _value, $Res Function(HomeLoading<T>) _then)
      : super(_value, (v) => _then(v as HomeLoading<T>));

  @override
  HomeLoading<T> get _value => super._value as HomeLoading<T>;
}

class _$HomeLoading<T> implements HomeLoading<T> {
  const _$HomeLoading();

  @override
  String toString() {
    return 'HomeState<$T>.homeLoading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is HomeLoading<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result idle(),
    @required Result homeLoading(),
    @required Result homeHashData(T data),
    @required Result errorHome(NetworkExceptions error),
  }) {
    assert(idle != null);
    assert(homeLoading != null);
    assert(homeHashData != null);
    assert(errorHome != null);
    return homeLoading();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result idle(),
    Result homeLoading(),
    Result homeHashData(T data),
    Result errorHome(NetworkExceptions error),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (homeLoading != null) {
      return homeLoading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result idle(Idle<T> value),
    @required Result homeLoading(HomeLoading<T> value),
    @required Result homeHashData(HomeHashData<T> value),
    @required Result errorHome(ErrorHome<T> value),
  }) {
    assert(idle != null);
    assert(homeLoading != null);
    assert(homeHashData != null);
    assert(errorHome != null);
    return homeLoading(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result idle(Idle<T> value),
    Result homeLoading(HomeLoading<T> value),
    Result homeHashData(HomeHashData<T> value),
    Result errorHome(ErrorHome<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (homeLoading != null) {
      return homeLoading(this);
    }
    return orElse();
  }
}

abstract class HomeLoading<T> implements HomeState<T> {
  const factory HomeLoading() = _$HomeLoading<T>;
}

abstract class $HomeHashDataCopyWith<T, $Res> {
  factory $HomeHashDataCopyWith(
          HomeHashData<T> value, $Res Function(HomeHashData<T>) then) =
      _$HomeHashDataCopyWithImpl<T, $Res>;
  $Res call({T data});
}

class _$HomeHashDataCopyWithImpl<T, $Res>
    extends _$HomeStateCopyWithImpl<T, $Res>
    implements $HomeHashDataCopyWith<T, $Res> {
  _$HomeHashDataCopyWithImpl(
      HomeHashData<T> _value, $Res Function(HomeHashData<T>) _then)
      : super(_value, (v) => _then(v as HomeHashData<T>));

  @override
  HomeHashData<T> get _value => super._value as HomeHashData<T>;

  @override
  $Res call({
    Object data = freezed,
  }) {
    return _then(HomeHashData<T>(
      data: data == freezed ? _value.data : data as T,
    ));
  }
}

class _$HomeHashData<T> implements HomeHashData<T> {
  const _$HomeHashData({@required this.data}) : assert(data != null);

  @override
  final T data;

  @override
  String toString() {
    return 'HomeState<$T>.homeHashData(data: $data)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is HomeHashData<T> &&
            (identical(other.data, data) ||
                const DeepCollectionEquality().equals(other.data, data)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(data);

  @override
  $HomeHashDataCopyWith<T, HomeHashData<T>> get copyWith =>
      _$HomeHashDataCopyWithImpl<T, HomeHashData<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result idle(),
    @required Result homeLoading(),
    @required Result homeHashData(T data),
    @required Result errorHome(NetworkExceptions error),
  }) {
    assert(idle != null);
    assert(homeLoading != null);
    assert(homeHashData != null);
    assert(errorHome != null);
    return homeHashData(data);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result idle(),
    Result homeLoading(),
    Result homeHashData(T data),
    Result errorHome(NetworkExceptions error),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (homeHashData != null) {
      return homeHashData(data);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result idle(Idle<T> value),
    @required Result homeLoading(HomeLoading<T> value),
    @required Result homeHashData(HomeHashData<T> value),
    @required Result errorHome(ErrorHome<T> value),
  }) {
    assert(idle != null);
    assert(homeLoading != null);
    assert(homeHashData != null);
    assert(errorHome != null);
    return homeHashData(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result idle(Idle<T> value),
    Result homeLoading(HomeLoading<T> value),
    Result homeHashData(HomeHashData<T> value),
    Result errorHome(ErrorHome<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (homeHashData != null) {
      return homeHashData(this);
    }
    return orElse();
  }
}

abstract class HomeHashData<T> implements HomeState<T> {
  const factory HomeHashData({@required T data}) = _$HomeHashData<T>;

  T get data;
  $HomeHashDataCopyWith<T, HomeHashData<T>> get copyWith;
}

abstract class $ErrorHomeCopyWith<T, $Res> {
  factory $ErrorHomeCopyWith(
          ErrorHome<T> value, $Res Function(ErrorHome<T>) then) =
      _$ErrorHomeCopyWithImpl<T, $Res>;
  $Res call({NetworkExceptions error});

  $NetworkExceptionsCopyWith<$Res> get error;
}

class _$ErrorHomeCopyWithImpl<T, $Res> extends _$HomeStateCopyWithImpl<T, $Res>
    implements $ErrorHomeCopyWith<T, $Res> {
  _$ErrorHomeCopyWithImpl(
      ErrorHome<T> _value, $Res Function(ErrorHome<T>) _then)
      : super(_value, (v) => _then(v as ErrorHome<T>));

  @override
  ErrorHome<T> get _value => super._value as ErrorHome<T>;

  @override
  $Res call({
    Object error = freezed,
  }) {
    return _then(ErrorHome<T>(
      error: error == freezed ? _value.error : error as NetworkExceptions,
    ));
  }

  @override
  $NetworkExceptionsCopyWith<$Res> get error {
    if (_value.error == null) {
      return null;
    }
    return $NetworkExceptionsCopyWith<$Res>(_value.error, (value) {
      return _then(_value.copyWith(error: value));
    });
  }
}

class _$ErrorHome<T> implements ErrorHome<T> {
  const _$ErrorHome({@required this.error}) : assert(error != null);

  @override
  final NetworkExceptions error;

  @override
  String toString() {
    return 'HomeState<$T>.errorHome(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ErrorHome<T> &&
            (identical(other.error, error) ||
                const DeepCollectionEquality().equals(other.error, error)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(error);

  @override
  $ErrorHomeCopyWith<T, ErrorHome<T>> get copyWith =>
      _$ErrorHomeCopyWithImpl<T, ErrorHome<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result idle(),
    @required Result homeLoading(),
    @required Result homeHashData(T data),
    @required Result errorHome(NetworkExceptions error),
  }) {
    assert(idle != null);
    assert(homeLoading != null);
    assert(homeHashData != null);
    assert(errorHome != null);
    return errorHome(error);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result idle(),
    Result homeLoading(),
    Result homeHashData(T data),
    Result errorHome(NetworkExceptions error),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (errorHome != null) {
      return errorHome(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result idle(Idle<T> value),
    @required Result homeLoading(HomeLoading<T> value),
    @required Result homeHashData(HomeHashData<T> value),
    @required Result errorHome(ErrorHome<T> value),
  }) {
    assert(idle != null);
    assert(homeLoading != null);
    assert(homeHashData != null);
    assert(errorHome != null);
    return errorHome(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result idle(Idle<T> value),
    Result homeLoading(HomeLoading<T> value),
    Result homeHashData(HomeHashData<T> value),
    Result errorHome(ErrorHome<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (errorHome != null) {
      return errorHome(this);
    }
    return orElse();
  }
}

abstract class ErrorHome<T> implements HomeState<T> {
  const factory ErrorHome({@required NetworkExceptions error}) = _$ErrorHome<T>;

  NetworkExceptions get error;
  $ErrorHomeCopyWith<T, ErrorHome<T>> get copyWith;
}
