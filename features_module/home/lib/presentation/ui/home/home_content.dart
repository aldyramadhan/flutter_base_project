import 'package:dependencies/dependencies.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:home/data/response/response_home.dart';
import 'package:shared/common/base/base.dart';
import 'package:shared/widget/widget.dart';

import 'article_highlight.dart';
import 'frequently_seen_items.dart';
import 'monitor_transactions.dart';
import 'why_checkr.dart';

class HomeContent extends StatefulWidget {
  final ResponseHome response;

  HomeContent({Key key, this.response}) : super(key: key);

  @override
  State<StatefulWidget> createState() => HomeContentState();

}
class HomeContentState extends State<HomeContent> with BaseView {
  int current = 0;
  bool isAutoPlay = true;
  double bannerHeight = 455.h;

  @override
  Widget build(BuildContext context) => GestureDetector(
    child: Stack(
      children: <Widget>[
        Positioned(top: 0, left: 0, right: 0,
          child: Container(
            height: bannerHeight,
            child: Stack(children: [
              HomeBanner(
                  height: bannerHeight,
                  autoPlay: true,
                  item: widget.response.data.banner.map((item) => ClipRRect(
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(20), topLeft: Radius.circular(20)),
                    child: CachedNetworkImage(imageUrl: item.image,
                      fit: BoxFit.fill,
                    ),
                  )).toList(),
                  onPageChanged: (index, reason) {
                    if (!isAutoPlay) return;
                    setState(() {
                      current = index;
                    });
                  }
              ),
              Positioned(top: bannerHeight / 1.36, left: 22, right: 22,
                  child: DotsIndicator(
                      dotsCount: widget.response.data.banner.length,
                      position: current.toDouble(),
                      decorator: DotsDecorator(
                        spacing: EdgeInsets.only(left: 6),
                        color: colorPalettes.grey6,
                        size: const Size.square(8),
                        activeSize: const Size(20, 8),
                        activeColor: colorPalettes.pink,
                        activeShape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5.0)),
                      )
                  )
              )
            ])
          )
        ),
        DraggableScrollableSheet(
          initialChildSize: 0.60,
          minChildSize: 0.60,
          maxChildSize: 1,
          builder: (BuildContext context, controller) {
            return NotificationListener<DraggableScrollableNotification>(
                onNotification: (notification) {
                  if (notification.extent >= notification.maxExtent) {
                    isAutoPlay = false;
                  } else if (notification.extent < notification.maxExtent) {
                    isAutoPlay = true;
                  }
                  return;
                },
                child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20),
                          topRight: Radius.circular(20)),
                    ),
                    child: _content(controller)
                )
            );
          }
        )
      ]
    )
  );

  Widget _content(ScrollController controller) => Container(
      decoration: BoxDecoration(
          color: colorPalettes.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20), topRight: Radius.circular(20))),
      child: ListView(
        controller: controller,
        physics: BouncingScrollPhysics(),
        children: [
          _user,
          MonitorTransaction(),
          WhyCheckr(advertisement: widget.response.data.advertisement),
          FrequentlySeenItems(popular: widget.response.data.popularItems),
          ArticleHighlight(article: widget.response.data.article)
        ])
    );

  Widget get _user => Container(
      padding: EdgeInsets.only(left: 16, right: 16, top: 30, bottom: 26),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(child: Txt("Hi, Muhammad",
                style: styles.textSmallStyle(colorPalettes.textlightGray))),
              Container(
                alignment: Alignment.center,
                child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(180)),
                    child: Container(
                      width: 40,
                      height: 40,
                      color: colorPalettes.grey,
                    )
                )
              )
            ]
          ),
          Txt(appConstant.letsCheck,
            style: styles.textHugeBoldStyle(colorPalettes.primaryText)),
          SizedBox(height: 16),
          Txt(appConstant.startCheck,
              gesture: buttonTap..onTap((){}),
              style: styles.mediumPrimaryButtonStyle(true)),
        ]
      )
  );

}
