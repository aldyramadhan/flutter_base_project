import 'package:dependencies/dependencies.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:home/data/entity/popular_item.dart';
import 'package:shared/common/base/base.dart';
import 'package:shared/widget/widget.dart';

class FrequentlySeenItems extends StatelessWidget with BaseView {
  final List<PopularItem> popular;

  FrequentlySeenItems({Key key, this.popular}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(bottom: 26),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: [
            Padding(padding: EdgeInsets.only(left: 16),
                child: Text(appConstant.frequentlySeen,
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                )
            ),
            SizedBox(height: 16),
            ConstrainedBox(
              constraints: BoxConstraints(maxHeight: 345.h, minHeight: 305.h),
              child: ListView.builder(
                  itemCount: popular.length,
                  scrollDirection: Axis.horizontal,
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  itemBuilder: (BuildContext context, int index) => CardItem(
                    imgUrl: popular[index].image,
                    title: popular[index].name,
                    titleDesc: popular[index].subCategory,
                    withBorder: true,
                  )
              ),
            )
          ],
        )
    );
  }
}
