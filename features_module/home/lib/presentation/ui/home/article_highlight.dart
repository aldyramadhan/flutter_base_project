import 'package:home/data/entity/article.dart';
import 'package:dependencies/dependencies.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared/common/base/base.dart';
import 'package:shared/widget/card/card.dart';

class ArticleHighlight extends StatelessWidget with BaseView {
  final List<Article> article;

  ArticleHighlight({Key key, this.article}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(bottom: 26),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(padding: EdgeInsets.symmetric(horizontal: 16),
              child: Row(children: [
                Expanded(
                    child: Text(
                      appConstant.doYouKnow,
                      style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    )),
                InkWell(
                  onTap: (){
                  },
                  child: Padding(
                    padding: EdgeInsets.only(left: 8, right: 8, top: 3, bottom: 3),
                    child: Text(appConstant.seeAll, style: TextStyle(fontSize: 14),),
                  ),
                )
              ]),
            ),
            SizedBox(height: 16),
            ConstrainedBox(
              constraints: BoxConstraints(maxHeight: 345.h, minHeight: 305.h),
              child: ListView.builder(
                  itemCount: article.length,
                  scrollDirection: Axis.horizontal,
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  itemBuilder: (BuildContext context, int index) => CardItem(
                    imgUrl: article[index].image,
                    title: article[index].title,
                    titleDesc: article[index].time,
                  )
              ),
            )
          ],
        )
    );
  }
}

