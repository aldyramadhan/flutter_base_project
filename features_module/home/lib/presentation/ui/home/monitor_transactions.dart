import 'package:dependencies/dependencies.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared/common/base/base.dart';
import 'package:shared/widget/card/transaction_card_item.dart';

class MonitorTransaction extends StatelessWidget with BaseView {

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 26, left: 16, right: 16),
      child: Column(children: [
        Row(children: [
          Expanded(
              child: Txt(appConstant.monitorTransaction,
                style: styles.textMediumBoldStyle(colorPalettes.primaryText),
              )),
          InkWell(
              onTap: (){
              },
              child: Padding(
                padding: EdgeInsets.only(left: 8, right: 8, top: 3, bottom: 3),
                child: Txt(appConstant.seeAll,
                    style: styles.textSmallStyle(colorPalettes.primaryText)),
              ))
        ]),
        SizedBox(height: 16),
        TransactionCardItem()
      ]),
    );
  }
}

