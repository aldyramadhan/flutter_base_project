import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:home/data/response/response_home.dart';
import 'package:home/presentation/bloc/bloc.dart';
import 'package:home/presentation/ui/home/home_content.dart';
import 'package:shared/common/base/base.dart';
import 'package:shared/widget/error/error.dart';
import 'package:shared/widget/widget.dart';
import 'package:dependencies/dependencies.dart';

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => HomePageState();
}

class HomePageState extends State<HomePage>
    with BaseView, AutomaticKeepAliveClientMixin {
  ResponseHome result;
  RefreshController refreshController = RefreshController(initialRefresh: false);

  @override
  void initState() {
    super.initState();
    _getHomeData();
  }

  _onRefresh() => Modular.get<HomeCubit>().getHome();

  void _getHomeData() {
    Modular.get<HomeCubit>().getHome();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              stops: [0.9,  0.1],
              colors: [
                colorPalettes.lightPrimary,
                colorPalettes.white
              ],
            )),
        child: Scaffold(
            backgroundColor: colorPalettes.transparent,
            appBar: PreferredSize(
                preferredSize: Size.fromHeight(300.h),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    _appBar,
                    AppbarHome()
                  ],
                )
            ),
            body: BlocListener<HomeCubit, HomeState>(
                listener: (context, state) {},
                child: BlocBuilder<HomeCubit, HomeState>(
                  builder: (context, state) {
                    if (state is HomeLoading && result == null) {
                      return ShimmerHome();
                    } else if (state is HomeHashData) {
                      context.hideLoaderOverlay();
                      result = state.data;
                    } else if (state is ErrorHome) {
                      refreshController.refreshFailed();
                      return ErrorExceptionWidget(
                        exceptions: state.error,
                        tapReload: buttonTap..onTap(() => _onRefresh()),
                      );
                    }
                    return _content();
                  },
                )
            )
        )
    );
  }

  Widget _content() {
    return SmartRefresher(
        controller: refreshController,
        enablePullUp: false,
        enablePullDown: true,
        header: WaterDropMaterialHeader(
          distance: 30,
        ),
        onRefresh: _onRefresh,
        child: HomeContent(response: result)
    );
  }

  Widget get _appBar => SimpleAppBar(
    height: 45.h,
    color: colorPalettes.fieldBackground,
    title: Row(children: [
      Image.asset(imageAsset.appLogo,
        width: 28.w,
        height: 28.h,
      ),
      SizedBox(width: 5),
      Txt(appConstant.appName,
        style: styles.textSmallBoldStyle(colorPalettes.primaryText),
      )
    ]),
  );

  @override
  bool get wantKeepAlive => true;

}
