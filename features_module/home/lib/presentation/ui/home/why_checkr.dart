import 'package:dependencies/dependencies.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:home/data/entity/advertisement.dart';
import 'package:shared/common/base/base.dart';
import 'package:shared/widget/card/card.dart';

class WhyCheckr extends StatelessWidget with BaseView {
  final Advertisement advertisement;

  WhyCheckr({Key key, @required this.advertisement}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 26, left: 16, right: 16),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(children: [
              Expanded(
                  child: Txt(getString(advertisement.title),
                    style: styles.textMediumBoldStyle(colorPalettes.primaryText),
                  )),
              InkWell(
                onTap: () {},
                child: Padding(
                    padding: EdgeInsets.all(6),
                    child: SvgPicture.asset(
                      imageAsset.wrongCircleOutline,
                      color: colorPalettes.black54,
                      width: 20,
                    )),
              )
            ]),
            Txt(getString(advertisement.description),
              style: styles.textSmallStyle(colorPalettes.textlightGray),
            ),
            SizedBox(height: 16),
            Container(
              child: GridView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: advertisement.items.length,
                  gridDelegate:
                  SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      childAspectRatio: 2.h / 1.1.h
                  ),
                  itemBuilder: (BuildContext context, int index) => CardCheckrItem(
                    image: advertisement.items[index].icon,
                    title: advertisement.items[index].title,
                  )
              ),
            )
          ]
      ),
    );
  }
}
