import 'package:dependencies/dependencies.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared/common/base/base.dart';

class RecentSearch extends StatelessWidget with BaseView {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Txt(appConstant.recentSearch,
            style: styles.textLargeStyle(colorPalettes.primaryText, bold: true),
          )
        ],
      ),
    );
  }
  
}