import 'package:dependencies/dependencies.dart';
import 'package:flutter/material.dart';
import 'package:home/presentation/ui/search/recent_searches.dart';
import 'package:shared/common/base/base.dart';
import 'package:shared/widget/widget.dart';

class SearchPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SearchPageState();
}

class SearchPageState extends State<SearchPage> with BaseView{
  final _searchController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        bottom: false,
        child: Scaffold(
          body: Padding(
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 40),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _searchBar(),
                SizedBox(height: 48),
                RecentSearch()
              ],
            ),
          )
        )
    );
  }

  Widget _searchBar() {
    return Container(
      height: 46,
      padding: EdgeInsets.symmetric(horizontal: 14),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(56)),
          border: Border.all(color: colorPalettes.inActiveButton, width: 1.5)
      ),
      child: Row(
        children: [
          Expanded(
              child: MyTextField(appConstant.justCheckFirst, _searchController,
                  fontSize: 16,
                  maxLines: 1,
                  required: false,
                  focusBorderColor: colorPalettes.lightPrimary,
                  type: FieldType.WITHOUTBORDER,
                  onValueChanged: (value) => FocusScope.of(context).unfocus()),
          ),
          SvgPicture.asset(
            imageAsset.search,
            color: colorPalettes.black2,
            width: 18,
          ),
        ],
      ),
    );
  }

}