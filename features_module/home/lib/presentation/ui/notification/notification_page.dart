import 'package:dependencies/dependencies.dart';
import 'package:flutter/material.dart';
import 'package:shared/common/base/base.dart';
import 'package:shared/common/common.dart';
import 'package:shared/widget/appbar/simple_app_bar.dart';
import 'package:shared/widget/card/card.dart';
import 'package:shared/widget/no_data/no_data.dart';

class NotificationPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => NotificationPageState();
}

class NotificationPageState extends State<NotificationPage> with BaseView{

  void goTo(String route) {
    navigation.linkTo(route);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          backgroundColor: colorPalettes.white,
          appBar: _appBar,
          body: _content(),
        )
    );
  }

  Widget get _appBar => PreferredSize(
      preferredSize: Size.fromHeight(60),
      child: SimpleAppBar(
        color: colorPalettes.white,
        centerTitle: true,
        leading: InkWell(
            onTap: () => navigation.toPop(),
            child: Icon(
              imageAsset.leadingIcon,
              color: colorPalettes.black,
            )
        ),
      )
  );

  Widget _content() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(padding: EdgeInsets.symmetric(horizontal: 16),
          child: Txt(appConstant.notification,
            style: styles.textLargeStyle(colorPalettes.primaryText, bold: true),
          ),
        ),
        Expanded(
            child: EmptyData(
              image: imageAsset.noNotificationIllustration,
              title: appConstant.noNotification,
              desc: appConstant.noNotificationDesc,
            )
        )
      ],
    );
  }

}