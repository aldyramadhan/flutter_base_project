import 'package:core/network/network.dart';
import 'package:home/data/response/response_home.dart';

abstract class HomeRepository {
  Future<ApiResult<ResponseHome>> getHome();
}