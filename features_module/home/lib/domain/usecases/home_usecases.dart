import 'package:core/network/network.dart';
import 'package:flutter/material.dart';
import 'package:home/data/response/response_home.dart';
import 'package:home/domain/repositories/home_repository.dart';

abstract class HomeUseCase {
  Future<ApiResult<ResponseHome>> getHome();
}

class HomeUseCaseImpl extends HomeUseCase {
  final HomeRepository repository;

  HomeUseCaseImpl({@required this.repository});

  @override
  Future<ApiResult<ResponseHome>> getHome() => repository.getHome();
}
