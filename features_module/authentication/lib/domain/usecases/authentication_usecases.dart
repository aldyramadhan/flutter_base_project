import 'package:authentication/data/request/request.dart';
import 'package:authentication/data/response/response.dart';
import 'package:authentication/data/response/response_forgot_password.dart';
import 'package:authentication/domain/repositories/authentication_repository.dart';
import 'package:core/network/network.dart';
import 'package:flutter/material.dart';

abstract class AuthenticationUseCase {
  Future<ApiResult<ResponseSignup>> doRegister({@required RegisterRequest request});
  Future<ApiResult<ResponseSignin>> doLogin({@required SigninRequest request});
  Future<String> doLoginByGoogle();
  Future<String> doLoginByFacebook();
  Future<String> doLoginByApple();
  Future<ApiResult<ResponseForgotPassword>> doForgotPassword({@required String email});
}

class AuthenticationUseCaseImpl extends AuthenticationUseCase {
  final AuthenticationRepository repository;

  AuthenticationUseCaseImpl({@required this.repository});

  @override
  Future<ApiResult<ResponseSignin>> doLogin({@required SigninRequest request}) =>
      repository.doLogin(request: request);

  @override
  Future<String> doLoginByApple() =>
      repository.doLoginByApple();

  @override
  Future<String> doLoginByFacebook() =>
      repository.doLoginByFacebook();

  @override
  Future<String> doLoginByGoogle() =>
      repository.doLoginByGoogle();

  @override
  Future<ApiResult<ResponseSignup>> doRegister({RegisterRequest request}) =>
      repository.doRegister(request: request);

  @override
  Future<ApiResult<ResponseForgotPassword>> doForgotPassword({@required String email}) =>
    repository.doForgotPassword(email: email);

}
