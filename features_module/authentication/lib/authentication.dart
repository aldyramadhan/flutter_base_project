import 'package:authentication/presentation/ui/email_sent_page.dart';
import 'package:authentication/presentation/ui/onboarding_page.dart';
import 'package:authentication/presentation/ui/wa_form_page.dart';
import 'package:dependencies/dependencies.dart';
import 'package:shared/common/common.dart';
import 'presentation/ui/forgot_password_page.dart';
import 'presentation/ui/otp_verification_page.dart';

class FeatureAuthenticationModule extends ChildModule {
  @override
  List<Bind> get binds => [];

  @override
  List<ModularRouter> get routers => [
    ModularRouter(
      Modular.get<NamedRoutes>().onBoardingPage,
      child: (context, arg) => OnBoardingPage(startedResult: arg.data,),
    ),
    ModularRouter(
      Modular.get<NamedRoutes>().forgotPasswordPage,
      child: (context, arg) => ForgotPasswordPage(),
    ),
    ModularRouter(
      Modular.get<NamedRoutes>().waFormPage,
      child: (context, arg) => WaFormPage(),
    ),
    ModularRouter(
      Modular.get<NamedRoutes>().otpVerificationPage,
      child: (context, arg) => OtpVerificationPage(),
    ),
    ModularRouter(
      Modular.get<NamedRoutes>().changePasswordSuccessPage,
      transition: TransitionType.downToUp,
      duration: Duration(milliseconds: 350),
      child: (context, arg) => EmailSentPage(),
    ),
  ];
}