// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'signin_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SigninData _$SigninDataFromJson(Map<String, dynamic> json) {
  return SigninData(
    json['access_token'] as String,
    json['token_type'] as String,
    json['expires_in'] as int,
  );
}

Map<String, dynamic> _$SigninDataToJson(SigninData instance) =>
    <String, dynamic>{
      'access_token': instance.accessToken,
      'token_type': instance.tokenType,
      'expires_in': instance.expiresIn,
    };
