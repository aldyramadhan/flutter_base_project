import 'package:json_annotation/json_annotation.dart';

part 'picture_data.g.dart';

@JsonSerializable()
class PictureData {
  @JsonKey(name: 'height')
  final int height;

  @JsonKey(name: 'url')
  final String url;

  @JsonKey(name: 'width')
  final int width;

  const PictureData([this.height, this.url, this.width]);

  factory PictureData.fromJson(Map<String, dynamic> json) =>
      _$PictureDataFromJson(json);

  Map<String, dynamic> toJson() => _$PictureDataToJson(this);
}
