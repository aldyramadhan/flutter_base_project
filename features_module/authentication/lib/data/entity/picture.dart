import 'package:json_annotation/json_annotation.dart';

import 'picture_data.dart';

part 'picture.g.dart';

@JsonSerializable()
class Picture {
  @JsonKey(name: 'data')
  final PictureData data;

  const Picture([this.data]);

  factory Picture.fromJson(Map<String, dynamic> json) =>
      _$PictureFromJson(json);

  Map<String, dynamic> toJson() => _$PictureToJson(this);
}
