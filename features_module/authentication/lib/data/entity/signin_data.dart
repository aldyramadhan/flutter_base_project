import 'package:json_annotation/json_annotation.dart';

part 'signin_data.g.dart';

@JsonSerializable()
class SigninData {
  @JsonKey(name: 'access_token')
  String accessToken;

  @JsonKey(name: 'token_type')
  String tokenType;

  @JsonKey(name: 'expires_in')
  int expiresIn;

  SigninData([this.accessToken, this.tokenType, this.expiresIn]);

  factory SigninData.fromJson(Map<String, dynamic> json) => _$SigninDataFromJson(json);

  Map<String, dynamic> toJson() => _$SigninDataToJson(this);
}
