import 'package:json_annotation/json_annotation.dart';

part 'getstarted_data.g.dart';

@JsonSerializable()
class GetstartedData {
  @JsonKey(name: 'image')
  final String image;

  @JsonKey(name: 'title')
  final String title;

  @JsonKey(name: 'description')
  final String description;

  const GetstartedData([this.image, this.title, this.description]);

  factory GetstartedData.fromJson(Map<String, dynamic> json) =>
      _$GetstartedDataFromJson(json);

  Map<String, dynamic> toJson() => _$GetstartedDataToJson(this);
}
