import 'package:json_annotation/json_annotation.dart';

part 'signup_data.g.dart';

@JsonSerializable()
class SignupData {
  @JsonKey(name: 'id')
  int id;

  @JsonKey(name: 'fullName')
  String fullName;

  @JsonKey(name: 'email')
  String email;

  @JsonKey(name: 'password')
  String password;

  @JsonKey(name: 'updated_at')
  String updatedAt;

  @JsonKey(name: 'created_at')
  String createdAt;

  SignupData(
      [this.id,
      this.fullName,
      this.email,
      this.password,
      this.updatedAt,
      this.createdAt]);

  factory SignupData.fromJson(Map<String, dynamic> json) => _$SignupDataFromJson(json);

  Map<String, dynamic> toJson() => _$SignupDataToJson(this);
}
