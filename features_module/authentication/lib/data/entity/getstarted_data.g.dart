// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'getstarted_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetstartedData _$GetstartedDataFromJson(Map<String, dynamic> json) {
  return GetstartedData(
    json['image'] as String,
    json['title'] as String,
    json['description'] as String,
  );
}

Map<String, dynamic> _$GetstartedDataToJson(GetstartedData instance) =>
    <String, dynamic>{
      'image': instance.image,
      'title': instance.title,
      'description': instance.description,
    };
