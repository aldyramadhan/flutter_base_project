// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'picture.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Picture _$PictureFromJson(Map<String, dynamic> json) {
  return Picture(
    json['data'] == null
        ? null
        : PictureData.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$PictureToJson(Picture instance) => <String, dynamic>{
      'data': instance.data,
    };
