// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'signup_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SignupData _$SignupDataFromJson(Map<String, dynamic> json) {
  return SignupData(
    json['id'] as int,
    json['fullName'] as String,
    json['email'] as String,
    json['password'] as String,
    json['updated_at'] as String,
    json['created_at'] as String,
  );
}

Map<String, dynamic> _$SignupDataToJson(SignupData instance) =>
    <String, dynamic>{
      'id': instance.id,
      'fullName': instance.fullName,
      'email': instance.email,
      'password': instance.password,
      'updated_at': instance.updatedAt,
      'created_at': instance.createdAt,
    };
