// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'picture_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PictureData _$PictureDataFromJson(Map<String, dynamic> json) {
  return PictureData(
    json['height'] as int,
    json['url'] as String,
    json['width'] as int,
  );
}

Map<String, dynamic> _$PictureDataToJson(PictureData instance) =>
    <String, dynamic>{
      'height': instance.height,
      'url': instance.url,
      'width': instance.width,
    };
