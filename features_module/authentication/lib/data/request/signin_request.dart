import 'package:flutter/cupertino.dart';

class SigninRequest {
  final String email;
  final String password;

  SigninRequest({@required this.email, @required this.password});
}