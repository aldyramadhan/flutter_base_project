class RegisterRequest {
  final String fullName;
  final String email;
  final String password;

  RegisterRequest(this.fullName, this.email, this.password);
}