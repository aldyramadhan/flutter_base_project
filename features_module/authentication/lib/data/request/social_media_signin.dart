import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class SocialMediaSignin {
  final String accessToken;
  final String deviceModel;
  final String deviceType;
  final String imei;
  final String id;
  final String deviceId;
  final String fullName;
  final String email;
  final String picture;

  SocialMediaSignin(
      {this.accessToken,
      this.deviceModel,
      this.deviceType,
      this.imei,
      this.id,
      this.deviceId,
      this.fullName,
      this.email,
      this.picture});
}
