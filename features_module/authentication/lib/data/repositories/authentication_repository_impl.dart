import 'package:authentication/data/datasource/authentication_datasource.dart';
import 'package:authentication/data/request/request.dart';
import 'package:authentication/data/response/response.dart';
import 'package:authentication/data/response/response_forgot_password.dart';
import 'package:core/network/network.dart';
import 'package:flutter/material.dart';
import '../../domain/repositories/authentication_repository.dart';

class AuthenticationRepositoryImpl extends AuthenticationRepository {
  final AuthenticationDataSource dataSource;

  AuthenticationRepositoryImpl({@required this.dataSource});

  @override
  Future<ApiResult<ResponseSignin>> doLogin({@required SigninRequest request}) async {
    return await dataSource.doLogin(request: request);
  }

  @override
  Future<String> doLoginByApple() async {
    var appleSource = await dataSource.doLoginByApple();
  }

  @override
  Future<String> doLoginByFacebook() async {
    var facebookSource = await dataSource.doLoginByFacebook();
  }

  @override
  Future<String> doLoginByGoogle() async {
    var googleSource = await dataSource.doLoginByGoogle();
  }

  @override
  Future<ApiResult<ResponseSignup>> doRegister({RegisterRequest request}) async{
    return await dataSource.doRegister(request: request);
  }

  @override
  Future<ApiResult<ResponseForgotPassword>> doForgotPassword({String email}) async{
    return await dataSource.doForgotPassword(email: email);
  }

}
