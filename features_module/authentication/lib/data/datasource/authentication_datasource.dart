import 'package:authentication/data/request/request.dart';
import 'package:authentication/data/response/response.dart';
import 'package:authentication/data/response/response_forgot_password.dart';
import 'package:core/network/api_result.dart';
import 'package:flutter/foundation.dart';

abstract class AuthenticationDataSource {
  Future<ApiResult<ResponseSignup>> doRegister({@required RegisterRequest request});
  Future<ApiResult<ResponseSignin>> doLogin({@required SigninRequest request});
  Future<String> doLoginByGoogle();
  Future<String> doLoginByFacebook();
  Future<String> doLoginByApple();
  Future<String> doRefreshToken();
  Future<String> doLogout();
  Future<ApiResult<ResponseForgotPassword>> doForgotPassword({@required String email});
}