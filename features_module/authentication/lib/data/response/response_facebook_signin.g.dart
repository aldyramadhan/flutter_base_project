// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'response_facebook_signin.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ResponseFacebookSignin _$ResponseFacebookSigninFromJson(
    Map<String, dynamic> json) {
  return ResponseFacebookSignin(
    json['token'] as String,
    json['userId'] as String,
    json['name'] as String,
    json['email'] as String,
    json['first_name'] as String,
    json['last_name'] as String,
    json['picture'] == null
        ? null
        : Picture.fromJson(json['picture'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ResponseFacebookSigninToJson(
        ResponseFacebookSignin instance) =>
    <String, dynamic>{
      'token': instance.token,
      'userId': instance.userId,
      'name': instance.name,
      'email': instance.email,
      'first_name': instance.firstName,
      'last_name': instance.lastName,
      'picture': instance.picture,
    };
