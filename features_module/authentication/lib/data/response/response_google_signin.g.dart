// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'response_google_signin.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ResponseGoogleSignin _$ResponseGoogleSigninFromJson(Map<String, dynamic> json) {
  return ResponseGoogleSignin(
    json['displayName'] as String,
    json['email'] as String,
    json['id'] as String,
    json['photoUrl'] as String,
    json['token'] as String,
  );
}

Map<String, dynamic> _$ResponseGoogleSigninToJson(
        ResponseGoogleSignin instance) =>
    <String, dynamic>{
      'displayName': instance.displayName,
      'email': instance.email,
      'id': instance.id,
      'photoUrl': instance.photoUrl,
      'token': instance.token,
    };
