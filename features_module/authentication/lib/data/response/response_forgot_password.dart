import 'package:json_annotation/json_annotation.dart';

part 'response_forgot_password.g.dart';

@JsonSerializable()
class ResponseForgotPassword {
  @JsonKey(name: 'success')
  bool success;

  @JsonKey(name: 'message')
  String message;

  ResponseForgotPassword([this.success, this.message]);

  factory ResponseForgotPassword.fromJson(Map<String, dynamic> json) =>
      _$ResponseForgotPasswordFromJson(json);

  Map<String, dynamic> toJson() => _$ResponseForgotPasswordToJson(this);
}
