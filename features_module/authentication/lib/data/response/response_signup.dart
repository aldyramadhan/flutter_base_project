import 'package:authentication/data/entity/entity.dart';
import 'package:json_annotation/json_annotation.dart';

part 'response_signup.g.dart';

@JsonSerializable()
class ResponseSignup {
  @JsonKey(name: 'success')
  bool success;

  @JsonKey(name: 'message')
  String message;

  @JsonKey(name: 'data')
  final SignupData data;

  ResponseSignup([this.success, this.message, this.data]);

  factory ResponseSignup.fromJson(Map<String, dynamic> json) =>
      _$ResponseSignupFromJson(json);

  Map<String, dynamic> toJson() => _$ResponseSignupToJson(this);
}
