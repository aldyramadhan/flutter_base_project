// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'response_getstarted.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ResponseGetstarted _$ResponseGetstartedFromJson(Map<String, dynamic> json) {
  return ResponseGetstarted(
    json['success'] as bool,
    json['message'] as String,
    (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : GetstartedData.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$ResponseGetstartedToJson(ResponseGetstarted instance) =>
    <String, dynamic>{
      'success': instance.success,
      'message': instance.message,
      'data': instance.data,
    };
