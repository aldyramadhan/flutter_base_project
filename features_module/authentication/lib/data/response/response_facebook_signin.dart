import 'package:authentication/data/entity/picture.dart';
import 'package:json_annotation/json_annotation.dart';

part 'response_facebook_signin.g.dart';

@JsonSerializable()
class ResponseFacebookSignin {
  @JsonKey(name: 'token')
  String token;

  @JsonKey(name: 'userId')
  String userId;

  @JsonKey(name: 'name')
  final String name;

  @JsonKey(name: 'email')
  final String email;

  @JsonKey(name: 'first_name')
  final String firstName;

  @JsonKey(name: 'last_name')
  final String lastName;

  @JsonKey(name: 'picture')
  final Picture picture;

  ResponseFacebookSignin(
      [this.token,
      this.userId,
      this.name,
      this.email,
      this.firstName,
      this.lastName,
      this.picture]);

  factory ResponseFacebookSignin.fromJson(Map<String, dynamic> json) =>
      _$ResponseFacebookSigninFromJson(json);

  Map<String, dynamic> toJson() => _$ResponseFacebookSigninToJson(this);
}
