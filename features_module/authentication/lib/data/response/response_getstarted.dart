import 'package:authentication/data/entity/entity.dart';
import 'package:json_annotation/json_annotation.dart';

part 'response_getstarted.g.dart';

@JsonSerializable()
class ResponseGetstarted {
  @JsonKey(name: 'success')
  bool success;

  @JsonKey(name: 'message')
  String message;

  @JsonKey(name: 'data')
  List<GetstartedData> data;

  ResponseGetstarted([this.success, this.message, this.data]);

  factory ResponseGetstarted.fromJson(Map<String, dynamic> json) =>
      _$ResponseGetstartedFromJson(json);

  Map<String, dynamic> toJson() => _$ResponseGetstartedToJson(this);
}
