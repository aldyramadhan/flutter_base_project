// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'response_signup.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ResponseSignup _$ResponseSignupFromJson(Map<String, dynamic> json) {
  return ResponseSignup(
    json['success'] as bool,
    json['message'] as String,
    json['data'] == null
        ? null
        : SignupData.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ResponseSignupToJson(ResponseSignup instance) =>
    <String, dynamic>{
      'success': instance.success,
      'message': instance.message,
      'data': instance.data,
    };
