// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'response_forgot_password.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ResponseForgotPassword _$ResponseForgotPasswordFromJson(
    Map<String, dynamic> json) {
  return ResponseForgotPassword(
    json['success'] as bool,
    json['message'] as String,
  );
}

Map<String, dynamic> _$ResponseForgotPasswordToJson(
        ResponseForgotPassword instance) =>
    <String, dynamic>{
      'success': instance.success,
      'message': instance.message,
    };
