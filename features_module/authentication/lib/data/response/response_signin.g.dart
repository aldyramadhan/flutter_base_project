// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'response_signin.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ResponseSignin _$ResponseSigninFromJson(Map<String, dynamic> json) {
  return ResponseSignin(
    json['success'] as bool,
    json['message'] as String,
    json['data'] == null
        ? null
        : SigninData.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ResponseSigninToJson(ResponseSignin instance) =>
    <String, dynamic>{
      'success': instance.success,
      'message': instance.message,
      'data': instance.data,
    };
