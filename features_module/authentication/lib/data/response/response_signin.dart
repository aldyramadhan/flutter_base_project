import 'package:authentication/data/entity/entity.dart';
import 'package:json_annotation/json_annotation.dart';

part 'response_signin.g.dart';

@JsonSerializable()
class ResponseSignin {
  @JsonKey(name: 'success')
  bool success;

  @JsonKey(name: 'message')
  String message;

  @JsonKey(name: 'data')
  final SigninData data;

  ResponseSignin([this.success, this.message, this.data]);

  factory ResponseSignin.fromJson(Map<String, dynamic> json) =>
      _$ResponseSigninFromJson(json);

  Map<String, dynamic> toJson() => _$ResponseSigninToJson(this);
}
