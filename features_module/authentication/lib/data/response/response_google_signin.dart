import 'package:json_annotation/json_annotation.dart';

part 'response_google_signin.g.dart';

@JsonSerializable()
class ResponseGoogleSignin {
  @JsonKey(name: 'displayName')
  final String displayName;

  @JsonKey(name: 'email')
  final String email;

  @JsonKey(name: 'id')
  final String id;

  @JsonKey(name: 'photoUrl')
  final String photoUrl;

  @JsonKey(name: 'token')
  final String token;

  const ResponseGoogleSignin(
      [this.displayName, this.email, this.id, this.photoUrl, this.token]);

  factory ResponseGoogleSignin.fromJson(Map<String, dynamic> json) =>
      _$ResponseGoogleSigninFromJson(json);

  Map<String, dynamic> toJson() => _$ResponseGoogleSigninToJson(this);
}
