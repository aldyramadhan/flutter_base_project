import 'package:authentication/data/datasource/authentication_datasource.dart';
import 'package:authentication/data/request/request.dart';
import 'package:authentication/data/response/response.dart';
import 'package:authentication/data/response/response_forgot_password.dart';
import 'package:dependencies/dependencies.dart';
import 'package:flutter/foundation.dart';
import 'package:core/network/network.dart';

class AuthenticationApi extends AuthenticationDataSource {
  final Dio dio;

  AuthenticationApi({@required this.dio});

  @override
  Future<ApiResult<ResponseSignin>> doLogin({@required SigninRequest request}) async{
    try {
      var formData = FormData.fromMap({
        "email": request.email,
        "password": request.email
      });
      Response response = await dio.post(ApiConstant.loginApi, data: formData);
      return ApiResult.success(data: ResponseSignin.fromJson(response.data));
    } on DioError catch (e) {
      return ApiResult.failure(error: NetworkExceptions.getDioException(e));
    }
  }

  @override
  Future<String> doLoginByApple() {}

  @override
  Future<String> doLoginByFacebook() {}

  @override
  Future<String> doLoginByGoogle() {}

  @override
  Future<ApiResult<ResponseSignup>> doRegister({@required RegisterRequest request}) async {
    try {
      var formData = FormData.fromMap({
        "fullName": request.fullName,
        "email": request.email,
        "password": request.email
      });
      Response response = await dio.post(ApiConstant.registerApi, data: formData);
      return ApiResult.success(data: ResponseSignup.fromJson(response.data));
    } on DioError catch (e) {
      return ApiResult.failure(error: NetworkExceptions.getDioException(e));
    }
  }

  @override
  Future<String> doRefreshToken() {
  }

  @override
  Future<String> doLogout() {
  }

  @override
  Future<ApiResult<ResponseForgotPassword>> doForgotPassword({@required String email}) async{
    try {
      var formData = FormData.fromMap({
        "email": email,
      });
      Response response = await dio.post(ApiConstant.forgotPasswoApi, data: formData);
      return ApiResult.success(data: ResponseForgotPassword.fromJson(response.data));
    } on DioError catch (e) {
      return ApiResult.failure(error: NetworkExceptions.getDioException(e));
    }
  }
}
