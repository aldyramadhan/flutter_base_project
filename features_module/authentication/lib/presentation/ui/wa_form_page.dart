import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared/common/base/base.dart';
import 'package:shared/common/common.dart';
import 'package:shared/widget/widget.dart';
import 'package:dependencies/dependencies.dart';

class WaFormPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => WaFormPageState();
}

class WaFormPageState extends State<WaFormPage> with BaseView {
  final formKey = GlobalKey<FormState>();
  final _phoneController = TextEditingController();
  final _phoneFocusNode = FocusNode();
  bool isValid = false;

  @override
  void initState() {
    super.initState();
    _phoneListener;
  }

  @override
  void dispose() {
    super.dispose();
    _phoneController.dispose();
  }

  void get _phoneListener => _phoneController.addListener(() {
    setState(() {
      if (_phoneController.text.isNotEmpty && formKey.currentState.validate())
        isValid = true;
      else
        isValid = false;
    });
  });

  void get _clearText => _phoneController.clear();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          backgroundColor: colorPalettes.white,
          appBar: _appBar,
          body: _content
        )
    );
  }

  Widget get _appBar => PreferredSize(
      preferredSize: Size.fromHeight(56),
      child: SimpleAppBar(
          color: colorPalettes.white,
          leading: InkWell(
            onTap: () => navigation.toPop(),
            child: Icon(
                Platform.isIOS ? CupertinoIcons.back : Icons.arrow_back,
                color: colorPalettes.black),
          )
      )
  );

  Widget get _content => SingleChildScrollView(
    padding: EdgeInsets.only(left: 24, right: 24, top: 16, bottom: 16),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Txt(appConstant.enterWaNumber,
          style: styles.textHugeBoldStyle(colorPalettes.primaryText)),
        SizedBox(height: 16.h),
        Txt(appConstant.enterWaDescription,
          style: styles.textNormalStyle(colorPalettes.textGray)),
        SizedBox(height: 54.h),
        _form(context),
        SizedBox(height: 54.h),
        Txt(appConstant.send,
            gesture: buttonTap..onTap(() => !isValid ? null :
            navigation.linkTo(Modular.get<NamedRoutes>().otpVerificationPage)),
            style: styles.mediumPrimaryButtonStyle(isValid)),
      ],
    ),
  );

  Widget _form(BuildContext context) => Form(
    key: formKey,
    child: Row(
      children: [
        Container(height: 45, width: 54,
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color: colorPalettes.grey7,
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.all(Radius.circular(5))),
          child: Txt(
            appConstant.countryCode,
            style: styles.textNormalStyle(colorPalettes.primaryText),
          )),
        SizedBox(width: 10.w),
        Expanded(child: Container(
          padding: EdgeInsets.symmetric(horizontal: 8, vertical: 3),
          alignment: Alignment.center,
          constraints: BoxConstraints(minHeight: 45),
          decoration: BoxDecoration(
              color: colorPalettes.grey7,
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.all(Radius.circular(5))),
          child: MyTextField(appConstant.enterWaNumber, _phoneController,
              nodes: _phoneFocusNode,
              fontSize: 16,
              required: false,
              maxLines: 1,
              maxLength: 13,
              suffixIcon: imageAsset.wrongCircleOutline,
              keyboard: TextInputType.phone,
              validator: Validator.numberPhoneValidator(minLength: 10, maxLength: 13),
              focusBorderColor: colorPalettes.lightPrimary,
              type: FieldType.WITHOUTBORDER,
              suffixTap: () => _clearText,
              onValueChanged: (value) => FocusScope.of(context).unfocus()),
        ))
      ],
    ),
  );
}

