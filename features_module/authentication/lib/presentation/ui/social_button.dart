import 'package:dependencies/dependencies.dart';
import 'package:flutter/material.dart';
import 'package:shared/common/base/base.dart';
import 'package:shared/common/utils/json_assets.dart';
import 'package:shared/widget/widget.dart';

import '../../im_authentication.dart';

class SocialButton extends StatelessWidget with BaseView {
  final Function onPressGoogle;
  final Function onPressFacebook;

  SocialButton(
      {Key key, @required this.onPressGoogle, @required this.onPressFacebook})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthenticationCubit, AuthenticationState>(builder: (context, state) {
      return Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: SizedBox(
                height: 48,
                child: SimpleButton(
                  onPress: onPressGoogle,
                  buttonColor: colorPalettes.lightPrimary,
                  buttontype: 1,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      state is GoogleLoading
                          ? lottie(Modular.get<JsonAssets>().googleLoading)
                          : SvgPicture.asset(imageAsset.google,
                              height: 22,
                              width: 22,
                            ),
                      SizedBox(width: 6),
                      Txt(appConstant.google,
                        style: styles.textMediumBoldStyle(colorPalettes.primaryText),
                      ),
                    ],
                  ),
                  borderRadius: 100,
                )),
          ),
          SizedBox(width: 10),
          Expanded(
            child: SizedBox(
                height: 48,
                child: SimpleButton(
                  onPress: onPressFacebook,
                  buttonColor: colorPalettes.lightPrimary,
                  buttontype: 1,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset(imageAsset.facebook,
                        height: 22,
                        width: 22,
                      ),
                      SizedBox(width: 6),
                      Txt(appConstant.facebook,
                        style: styles.textMediumBoldStyle(colorPalettes.primaryText),
                      ),
                    ],
                  ),
                  borderRadius: 100,
                )),
          ),
        ],
      );
    });
  }

  Widget lottie(String lottieAssets) => Container(
    height: 40,
    width: 40,
    child: Lottie.asset(lottieAssets, height: 200),
  );

}
