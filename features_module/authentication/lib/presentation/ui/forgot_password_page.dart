import 'package:authentication/data/response/response_forgot_password.dart';
import 'package:authentication/im_authentication.dart';
import 'package:authentication/presentation/bloc/authentication/authentication_state.dart';
import 'package:core/network/network.dart';
import 'package:dependencies/dependencies.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:shared/common/base/base.dart';
import 'package:shared/common/common.dart';
import 'package:shared/widget/widget.dart';

class ForgotPasswordPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => ForgotPasswordPageState();
}

class ForgotPasswordPageState extends State<ForgotPasswordPage> with BaseView {
  final formKey = GlobalKey<FormState>();
  final passCubit = Modular.get<AuthenticationCubit>();
  final _emailController = TextEditingController();
  final _emailFocusNode = FocusNode();
  bool _disable = false;

  @override
  void initState() {
    super.initState();
    _emailListener();
  }

  @override
  void dispose() {
    super.dispose();
    _emailController.dispose();
  }

  void get _doForgotPassword {
    passCubit.doForgotPassword(_emailController.text);
  }

  void _emailListener() {
    _emailController.addListener(() {
      setState(() {
        if (_emailController.text.isNotEmpty &&
            formKey.currentState.validate())
          _disable = true;
        else
          _disable = false;
      });
    });
  }

  void _hasData() {
    showToast(appConstant.emailSend, context);
    navigation.linkTo(Modular.get<NamedRoutes>().changePasswordSuccessPage);
  }

  void _forgotPassSuccess(ResponseForgotPassword response) {

  }

  @override
  Widget build(BuildContext context)  {
    return SafeArea(
        child: Scaffold(
            backgroundColor: colorPalettes.white,
            appBar: _appBar,
            body: BlocListener<AuthenticationCubit, AuthenticationState>(
              listener: (context, AuthenticationState state){
                if (state is ForgotPassHashData) {
                  return _hasData();
                } else if (state is ErrorForgotPass) {
                  return showMessage(context,
                      message: NetworkExceptions.getErrorMessage(state.error));
                }
              },
              child: BlocBuilder<AuthenticationCubit, AuthenticationState>(
                builder: (context, AuthenticationState state) {
                  if (state is ForgotPassLoading) {
                    context.showLoaderOverlay();
                  } else if (state is ForgotPassHashData) {
                    context.hideLoaderOverlay();
                  } else if (state is ErrorForgotPass) {
                    context.hideLoaderOverlay();
                  }
                  return Parent(
                    style: styles.hugeParent(),
                    child: _content,
                  );
                },
              )
            ),
            bottomNavigationBar: _submitButton()
        )
    );
  }

  Widget get _appBar => PreferredSize(
      preferredSize: Size.fromHeight(56),
      child: SimpleAppBar(
        color: colorPalettes.white,
        leading: InkWell(
            onTap: () => navigation.toPop(),
            child: Icon(imageAsset.leadingIcon,
              color: colorPalettes.black,
            )
        ),
      )
  );

  Widget get _content => Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Txt(appConstant.forgotPassword,
              style: styles.textHugeBoldStyle(colorPalettes.primaryText),
            ),
            SizedBox(height: 14),
            Txt(appConstant.forgotPasswordDesc,
              style: styles.textNormalStyle(colorPalettes.textGray),
            ),
            SizedBox(height: 20),
            Container(
                alignment: Alignment.center,
                child: Image.asset(imageAsset.passIllustration,
                height: 250.h,
              )),
            _form,
            SizedBox(height: 12),
            _rememberWordingRememberSandi
          ],
        ),
      )
  );

  Widget get _rememberWordingRememberSandi => Row(
      children: [
        Txt(appConstant.rememberSandi,
          style: styles.textNormalStyle(colorPalettes.textGray)
        ),
        Padding(padding: EdgeInsets.symmetric(horizontal: 8, vertical: 3),
            child: Txt(appConstant.masuk,
              gesture: Gestures()..onTap(() => navigation.toPop()),
              style: styles.textNormalBoldStyle(colorPalettes.lightPrimary, pH: 6, pV: 3),
        ))
      ]
  );

  Widget get _form => Form(
      key: formKey,
      child: Container(
        margin: EdgeInsets.only(top: 40),
        padding: EdgeInsets.symmetric(horizontal: 14, vertical: 3),
        alignment: Alignment.center,
        constraints: BoxConstraints(minHeight: 48),
        decoration: BoxDecoration(
            color: colorPalettes.grey7,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.all(Radius.circular(5))),
        child: MyTextField(appConstant.email, _emailController,
            nodes: _emailFocusNode,
            fontSize: 16,
            required: false,
            maxLines: 1,
            validator: Validator.emailValidator(),
            focusBorderColor: colorPalettes.lightPrimary,
            type: FieldType.WITHOUTBORDER,
            onValueChanged: (value) => FocusScope.of(context).unfocus()),
      )
  );

  Widget _submitButton() => Container(
    margin: EdgeInsets.only(left: 16, right: 16, bottom: 50),
    child: Txt(appConstant.send,
        gesture: buttonTap..onTap(() => !_disable ? null : _doForgotPassword),
        style: styles.mediumPrimaryButtonStyle(_disable)),
  );
}
