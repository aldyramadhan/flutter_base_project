import 'package:dependencies/dependencies.dart';
import 'package:flutter/material.dart';
import 'package:shared/common/base/base.dart';
import 'package:shared/widget/widget.dart';

class EmailSentPage extends StatelessWidget with BaseView{

  void get _doOpenEmail => navigation.launchURL(appConstant.launchEmail);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            backgroundColor: colorPalettes.primaryBackground,
            appBar: _appBar,
            body: _content,
            bottomNavigationBar: _openEmailButton
        )
    );
  }

  Widget get _appBar => PreferredSize(
      preferredSize: Size.fromHeight(56),
      child: SimpleAppBar(
          color: colorPalettes.white,
          leading: InkWell(
              onTap: () => navigation.toPop(),
              child: Icon(
                imageAsset.leadingIcon,
                color: colorPalettes.black,
              )
          )
      )
  );

  Widget get _content => Parent(
    style: styles.hugeParent(
        background: colorPalettes.primaryBackground
    ),
    child: SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Txt(appConstant.success,
              style: styles.textHugeBoldStyle(colorPalettes.primaryText)),
          SizedBox(height: 14),
          Txt(appConstant.successForgotPass,
              style: styles.textNormalStyle(colorPalettes.textGray)),
          SizedBox(height: 20),
          Container(
              alignment: Alignment.center,
              child: Image.asset(imageAsset.successIllustration,
                height: 250.h,
              )),
          SizedBox(height: 36),
          _resendTitle
        ],
      ),
    )
  );

  Widget get _resendTitle => Row(
      children: [
        Txt(appConstant.notGeetingEmail,
            style: styles.textNormalStyle(colorPalettes.textGray)),
        Padding(
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 3),
            child: Txt(appConstant.resend,
                gesture: Gestures()..onTap(() => navigation.toPop()),
                style: styles.textNormalBoldStyle(colorPalettes.lightPrimary, pH: 6, pV: 3)
            ))
      ]
  );

  Widget get _openEmailButton => Container(
    margin: EdgeInsets.only(left: 16, right: 16, bottom: 50),
    child: Txt(appConstant.openEmail,
        gesture: buttonTap..onTap(() => _doOpenEmail),
        style: styles.mediumPrimaryButtonStyle(true))
  );

}