import 'dart:async';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared/common/base/base.dart';
import 'package:shared/widget/widget.dart';
import 'package:dependencies/dependencies.dart';

class OtpVerificationPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => OtpVerificationPageState();
}

class OtpVerificationPageState extends State<OtpVerificationPage> with BaseView {
  TextEditingController pinController = TextEditingController();
  StreamController<ErrorAnimationType> errorController;
  String currentText = "";
  bool hasError = false;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            backgroundColor: colorPalettes.white,
            appBar: _appBar,
            body: _content
        )
    );
  }

  Widget get _content => SingleChildScrollView(
      child: Parent(
          style: styles.hugeParent(),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Txt(appConstant.verificationOtp,
                  style: styles.textHugeBoldStyle(colorPalettes.primaryText),
                ),
                SizedBox(height: 16.h),
                Txt(appConstant.otpDescription,
                  style: styles.textNormalStyle(colorPalettes.textGray),
                ),
                SizedBox(height: 46.h),
                _form(context),
                SizedBox(height: 68.h),
                Txt(appConstant.verificationOtp,
                    gesture: buttonTap..onTap((){}),
                    style: styles.mediumPrimaryButtonStyle(true)),
                SizedBox(height: 22.h),
                _countdownWording,
                SizedBox(height: 20.h),
                Center(
                    child: Txt(appConstant.changePhone,
                        style: styles.textSmallBoldStyle(colorPalettes.lightPrimary))
                )
              ])
      )
  );

  Widget get _appBar => PreferredSize(
      preferredSize: Size.fromHeight(56),
      child: SimpleAppBar(
          color: colorPalettes.white,
          leading: InkWell(
              onTap: () => navigation.toPop(),
              child: Icon(
                Platform.isIOS ? CupertinoIcons.back : Icons.arrow_back,
                color: colorPalettes.black,
              )
          )
      )
  );

  Widget _form(BuildContext context) => Container(
      height: 55,
      margin: EdgeInsets.only(left: 24, right: 24),
      child: Form(child: _pinCode)
  );

  Widget get _pinCode => PinCodeTextField(
    appContext: context,
    pastedTextStyle: TextStyle(
      fontSize: 18,
      color: colorPalettes.white,
      fontWeight: FontWeight.bold,
    ),
    length: 4,
    obscureText: false,
    obscuringCharacter: '*',
    animationType: AnimationType.fade,
    pinTheme: PinTheme(
      shape: PinCodeFieldShape.box,
      borderRadius: BorderRadius.circular(50),
      fieldHeight: 50,
      fieldWidth: 50,
      activeFillColor: colorPalettes.lightPrimary,
      inactiveFillColor: colorPalettes.grey7,
      activeColor: colorPalettes.lightPrimary,
      inactiveColor: colorPalettes.grey7,
    ),
    cursorColor: Colors.white,
    animationDuration: Duration(milliseconds: 300),
    textStyle: TextStyle(fontSize: 18, height: 1.6, color: colorPalettes.white),
    backgroundColor: colorPalettes.white,
    enableActiveFill: true,
    errorAnimationController: errorController,
    controller: pinController,
    keyboardType: TextInputType.number,
    onCompleted: (v) {
      print("Completed");
      },
    onChanged: (value) {
      setState(() => currentText = value);
      },
    beforeTextPaste: (text) {
      print("Allowing to paste $text");
      return true;
      },
  );


  Widget get _countdownWording => Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Txt("Tunggu ",
          style: styles.textSmallStyle(colorPalettes.textlightGray),
        ),
        _countdown,
        Txt(" detik untuk kode dikirim ulang",
          style: styles.textSmallStyle(colorPalettes.textlightGray),
        )
      ]
  );

  Widget get _countdown => Container(
      child: CountdownFormatted(
          duration: Duration(minutes: 1),
          builder: (BuildContext ctx, String remaining) {
            return Text(remaining,
              style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                  color: colorPalettes.lightPrimary),
              textAlign: TextAlign.center,
            );
          })
  );

}

