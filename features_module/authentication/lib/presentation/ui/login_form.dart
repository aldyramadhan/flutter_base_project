import 'package:authentication/data/request/request.dart';
import 'package:authentication/data/response/response.dart';
import 'package:authentication/presentation/bloc/authentication/authentication_state.dart';
import 'package:authentication/presentation/ui/social_button.dart';
import 'package:core/network/network.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared/common/base/base.dart';
import 'package:shared/common/common.dart';
import 'package:shared/widget/widget.dart';
import 'package:dependencies/dependencies.dart';

import '../../im_authentication.dart';

class LoginForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => LoginFormState();
}

class LoginFormState extends State<LoginForm> with BaseView {
  final formKey = GlobalKey<FormState>();
  final _emailController = TextEditingController();
  final _emailFocusNode = FocusNode();
  final _passController = TextEditingController();
  final _passFocusNode = FocusNode();
  bool _disable = false;

  @override
  void initState() {
    super.initState();
    _emailListener;
    _passListener;
  }

  @override
  void dispose() {
    super.dispose();
    _emailController.dispose();
    _passController.dispose();
  }

  void get _emailListener {
    _emailController.addListener(() {
      setState(() {
        if (_emailController.text.isNotEmpty &&
            _passController.text.isNotEmpty &&
            formKey.currentState.validate())
          _disable = true;
        else
          _disable = false;
      });
    });
  }

  void get _passListener {
    _passController.addListener(() {
      setState(() {
        if (_passController.text.isNotEmpty &&
            _emailController.text.isNotEmpty &&
            formKey.currentState.validate())
          _disable = true;
        else
          _disable = false;
      });
    });
  }

  void get doGoogleSignin {
    // Modular.get<LoginBloc>()
    //   ..add(DoGoogleSignin(context, "deviceInfo.model", "deviceInfo.type",
    //       "deviceInfo.id", "deviceInfo.imei"));
  }

  void get _doFacebookSignin {}

  void get _doSignin {
    context.showLoaderOverlay();
    var request = SigninRequest(
        email: _emailController.text.trim(),
        password: _passController.text.trim()
    );

    if (!formKey.currentState.validate()) return;
    Modular.get<AuthenticationCubit>().doLogin(request);
  }

  void _hasData() {
    context.hideLoaderOverlay();
    showToast(appConstant.signinSuccess, context, duration: 4);
    navigation.toOffAll(Modular.get<NamedRoutes>().dashboardPage);
  }

  void _loginSuccess(ResponseSignin response) {
    context.hideLoaderOverlay();
    saveLogin(response.data.accessToken);
    showToast(appConstant.signinSuccess, context, duration: 4);
    navigation.toOffAll(Modular.get<NamedRoutes>().dashboardPage);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthenticationCubit, AuthenticationState>(
        listener: (context, state) {
          if (state is LoginHashData) {
            return _loginSuccess(state.data);
          } else if (state is ErrorLogin) {
            return showMessage(context,
                message: NetworkExceptions.getErrorMessage(state.error));
          }        },
        child: BlocBuilder<AuthenticationCubit, AuthenticationState>(
          builder: (context, state) {
            if (state is LoginLoading) {
              context.showLoaderOverlay();
            } else if (state is LoginHashData) {
              context.hideLoaderOverlay();
            } else if (state is ErrorLogin) {
              context.hideLoaderOverlay();
            }
            return _content;
          },
        )
    );
  }


  Widget get _content => Container(
    margin: EdgeInsets.only(top: 10.h),
    child: Column(children: [
      _form,
      Txt(appConstant.masuk,
          gesture: buttonTap..onTap(() => !_disable ? null : _doSignin),
          style: styles.mediumPrimaryButtonStyle(_disable)),
      SizedBox(height: 20.h),
      _loginWithWording,
      SizedBox(height: 24.h),
      SocialButton(
        onPressGoogle: () => doGoogleSignin,
        onPressFacebook: () {},
      )
    ]),
  );

  Widget get _loginWithWording => Row(
    mainAxisAlignment: MainAxisAlignment.center,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Expanded(child: Container(
        height: 1,
        margin: EdgeInsets.only(right: 6),
        color: colorPalettes.grey4,
        )),
      Txt(appConstant.loginWith,
        style: styles.textTinyStyle(colorPalettes.primaryText),
      ),
      Expanded(child: Container(
        height: 1,
        margin: EdgeInsets.only(left: 6),
        color: colorPalettes.grey4,
      ))
    ],
  );


  Widget get _form => Container(
    child: Form(
      key: formKey,
      child: Column(children: [
        MyTextField(appConstant.email, _emailController,
            nodes: _emailFocusNode,
            fontSize: 16,
            required: true,
            focusBorderColor: colorPalettes.lightPrimary,
            validator: Validator.emailValidator(),
            onValueChanged: (value) =>
                FocusScope.of(context).requestFocus(_passFocusNode)),
        MyTextField(appConstant.password, _passController,
            nodes: _passFocusNode,
            isSecure: true,
            maxLines: 1,
            fontSize: 16,
            required: true,
            focusBorderColor: colorPalettes.lightPrimary,
            onValueChanged: (value) => FocusScope.of(context).unfocus()),
        Container(
            alignment: Alignment.centerRight,
            margin: EdgeInsets.only(top: 20.h),
            child: Txt(appConstant.forgotPassword,
              gesture: Gestures()..onTap(() =>
                  navigation.linkTo(Modular.get<NamedRoutes>().forgotPasswordPage)),
              style: styles.textTinyStyle(colorPalettes.primaryText, pH: 6, pV: 3),
            )
        ),
        SizedBox(height: 86.h)
      ])
    )
  );

}
