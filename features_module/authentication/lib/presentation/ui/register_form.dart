import 'package:authentication/data/request/register_request.dart';
import 'package:core/network/network.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared/common/base/base.dart';
import 'package:shared/common/common.dart';
import 'package:shared/widget/widget.dart';
import 'package:dependencies/dependencies.dart';

import '../../im_authentication.dart';
import 'social_button.dart';

class RegisterForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => RegisterFormState();
}

class RegisterFormState extends State<RegisterForm> with BaseView {
  final formKey = GlobalKey<FormState>();
  final _nameController = TextEditingController();
  final _nameFocusNode = FocusNode();
  final _emailController = TextEditingController();
  final _emailFocusNode = FocusNode();
  final _passController = TextEditingController();
  final _passFocusNode = FocusNode();
  CancelFunc cancelFunc;
  var capitalLetterRegex = new RegExp("^(?=.*?[A-Z])");
  var numberRegex = new RegExp("^(?=.*?[0-9])");
  bool _disable = false;
  bool _passValidate = false;

  @override
  void initState() {
    super.initState();
    _emailListener;
    _passTempListener;
    _passListener;
    _nameListener;
  }

  @override
  void dispose() {
    super.dispose();
    _emailController.dispose();
    _nameController.dispose();
    _passController.dispose();
    _showSnackValidation(dispose: true);
  }

  bool get _passValidate1 => capitalLetterRegex.hasMatch(_passController.value.text);

  bool get _passValidate2 => numberRegex.hasMatch(_passController.value.text);

  void get _emailListener => _emailController.addListener(() {
    setState(() {
      if (_passController.text.isNotEmpty &&
          _emailController.text.isNotEmpty &&
          _nameController.text.isNotEmpty &&
          formKey.currentState.validate())
        _disable = true;
      else
        _disable = false;
    });
  });

  void get _passListener => _passController.addListener(() {
    setState(() {
      if (_passController.text.isNotEmpty &&
          _emailController.text.isNotEmpty &&
          _nameController.text.isNotEmpty &&
          formKey.currentState.validate()) {
        _disable = true;
      } else {
        _disable = false;
      }
      if (_passValidate) return;
      if (!_passValidate1) {
        _showSnackValidation(validate1: _passValidate1, validate2: _passValidate2);
      }
      if (!_passValidate2) {
        _showSnackValidation(validate1: _passValidate1, validate2: _passValidate2);
      }
      if (_passValidate1 && _passValidate2) {
        _showSnackValidation(validate1: _passValidate1, validate2: _passValidate2);
        _passValidate = true;
      }
    });
  });

  void get _passTempListener => _passController.addListener(() {
    setState(() {
      if (!_passValidate) return;
      if (!_passValidate1) {
        _passValidate = false;
        _showSnackValidation(validate1: _passValidate1, validate2: _passValidate2);
      }
      if (!_passValidate2) {
        _passValidate = false;
        _showSnackValidation(validate1: _passValidate1, validate2: _passValidate2);
      }
    });
  });

  void get _nameListener => _nameController.addListener(() {
    setState(() {
      if (_passController.text.isNotEmpty &&
          _emailController.text.isNotEmpty &&
          _nameController.text.isNotEmpty &&
          formKey.currentState.validate())
        _disable = true;
      else
        _disable = false;
    });
  });

  void _showSnackValidation({bool validate1 = false, bool validate2 = false,
    bool dispose = false}) => PassValidateSnackBar(
      duration: null,
      isValidate1: validate1,
      isValidate2: validate2,
      dispose: dispose)
      .show();

  void _doSignup() {
    FocusScope.of(context).unfocus();
    var request = RegisterRequest(
        _nameController.text.trim(),
        _emailController.text.trim(),
        _passController.text.trim());

    if (!formKey.currentState.validate()) return;
    context.showLoaderOverlay();
    Modular.get<AuthenticationCubit>().doRegister(request);
  }

  void _hasData() {
    context.hideLoaderOverlay();
    navigation.linkTo(Modular.get<NamedRoutes>().waFormPage);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthenticationCubit, AuthenticationState>(
      listener: (context, state) {
        if (state is RegisterHashData) {
          return _hasData();
        } else if (state is ErrorRegist) {
          return showMessage(context,
              message: NetworkExceptions.getErrorMessage(state.error));
        }
      },
      child: BlocBuilder<AuthenticationCubit, AuthenticationState>(
        builder: (context, state) {
          if (state is RegisterLoading) {
            context.showLoaderOverlay();
          } else if (state is RegisterHashData) {
            context.hideLoaderOverlay();
          } else if (state is ErrorRegist) {
            context.hideLoaderOverlay();
          }
          return _content;
        },
      ),
    );
  }

  Widget get _content => Container(
    margin: EdgeInsets.only(top: 10.h),
    child: Column(
      children: [
        _form,
        Txt(appConstant.daftar,
            gesture: buttonTap..onTap(() => !_disable ? null : _doSignup()),
            style: styles.mediumPrimaryButtonStyle(_disable)),
        SizedBox(height: 20.h),
        _registerWithWording,
        SizedBox(height: 24.h),
        SocialButton(
          onPressGoogle: () {},
          onPressFacebook: () {},
        )
      ],
    ),
  );

  Widget get _form => Container(
    child: Form(
      key: formKey,
      child: Column(
        children: [
          MyTextField(appConstant.name, _nameController,
              nodes: _nameFocusNode,
              fontSize: 16,
              focusBorderColor: colorPalettes.lightPrimary,
              validator: Validator.textOnlyValidator(),
              onValueChanged: (value) =>
                  FocusScope.of(context).requestFocus(_emailFocusNode)),
          MyTextField(appConstant.email, _emailController,
              nodes: _emailFocusNode,
              fontSize: 16,
              focusBorderColor: colorPalettes.lightPrimary,
              validator: Validator.emailValidator(),
              onValueChanged: (value) =>
                  FocusScope.of(context).requestFocus(_passFocusNode)),
          MyTextField(appConstant.password, _passController,
              isSecure: true,
              maxLines: 1,
              nodes: _passFocusNode,
              fontSize: 16,
              focusBorderColor: colorPalettes.lightPrimary,
              validator: (String val) {
            return Validator.passwordValidator(minLength: 6, isUniques: true)(val);
            },
              onValueChanged: (value) => FocusScope.of(context).unfocus()),
          SizedBox(height: 86.h)
        ]
      )
    )
  );

  Widget get _registerWithWording => Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(child: Container(
          height: 1,
          margin: EdgeInsets.only(right: 6),
          color: colorPalettes.grey4,
          )),
        Txt(appConstant.registerWith,
          style: styles.textTinyStyle(colorPalettes.primaryText),
        ),
        Expanded(child: Container(
          height: 1,
          margin: EdgeInsets.only(left: 6),
          color: colorPalettes.grey4,
        ))
      ]
  );
}

