import 'package:authentication/data/response/response.dart';
import 'package:authentication/presentation/ui/login_form.dart';
import 'package:authentication/presentation/ui/register_form.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:dependencies/dependencies.dart';
import 'package:shared/common/base/base.dart';
import 'package:shared/common/common.dart';
import 'package:shared/widget/widget.dart';

class OnBoardingPage extends StatefulWidget {
  final ResponseGetstarted startedResult;

  const OnBoardingPage({Key key, this.startedResult}) : super(key: key);

  @override
  State<StatefulWidget> createState() => OnBoardingPageState();
}

class OnBoardingPageState extends State<OnBoardingPage> with
    BaseView, SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TabController controller;
  bool keyboardNotify = false;
  int page = 0;
  int current = 0;

  @override
  void initState() {
    super.initState();
    getKeyboardListener();
    controller = TabController(vsync: this, length: 2);
    page = controller.index;
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
    KeyboardVisibilityNotification().dispose();
  }

  void getKeyboardListener() {
    KeyboardVisibilityNotification().addNewListener(
      onChange: (bool visible) {
        setState(() {
          keyboardNotify = visible;
        });
      },
    );
  }


  void _showBottomShet(int form) {
    page = form;
    controller.index = page;
    return BaseBottomSheet(
            child: _bottomSheet(form),
            isScrollController: true,
            context: context,
            topLeft: 20,
            topRight: 20)
        .showBaseBottomSheet();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(top: true, bottom: false,
        child: Scaffold(
            key: _scaffoldKey,
            backgroundColor: colorPalettes.white,
            body: Container(
              child: Stack(
                children: [
                  _slider,
                  _skipText
                ],
              )
            ))
    );
  }

  Widget get _slider => Container(
      child: !keyboardNotify ? Column(
          children: [
            Expanded(child: Container(
                padding: EdgeInsets.only(top: 46),
                child: OnboardingSlider(
                    item: widget.startedResult.data,
                    ratio: 1/1,
                    height: MediaQuery.of(context).size.height.sh,
                    onPageChanged: (index, reason) => setState(() {
                      current = index;
                    })
                ))),
            DotsIndicator(
                dotsCount: widget.startedResult.data.length,
                position: current.toDouble(),
                decorator: DotsDecorator(
                  spacing: EdgeInsets.only(left: 6),
                  color: colorPalettes.grey4,
                  size: const Size.square(8.0),
                  activeSize: const Size(22.0, 8.0),
                  activeShape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0)),
                )
            ),
            _buttons()
          ]
      ) : null
  );

  Widget get _skipText => Positioned(
      top: 26,
      right: 10,
      child: Padding(
        padding: EdgeInsets.only(top: 8, bottom: 8, left: 14, right: 14),
        child: Txt(appConstant.skip,
          gesture: buttonTap..onTap(() =>
              navigation.toOffAll(Modular.get<NamedRoutes>().dashboardPage)),
          style: styles.textTinyStyle(colorPalettes.primaryText, pH: 6, pV: 3),
        )
      )
  );


  Widget _buttons() {
    return Padding(
      padding: EdgeInsets.only(left: 24, right: 24, bottom: 24.h, top: 54.h),
      child: Column(
        children: [
          Txt(appConstant.daftar,
              gesture: buttonTap..onTap(() => _showBottomShet(0)),
              style: styles.mediumPrimaryButtonStyle(true)
          ),
          SizedBox(height: 16.h),
          Txt(appConstant.masuk,
              gesture: buttonTap..onTap(() => _showBottomShet(1)),
              style: styles.mediumPrimaryButtonStyle(
                  false, color: colorPalettes.secondaryButton,
                  txtColor: colorPalettes.lightPrimary)
          ),
        ],
      ),
    );
  }

  Widget _bottomSheet(int form) {
    return StatefulBuilder(builder: (BuildContext ctxt, setState) {
      return GestureDetector(
          onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
          child: Container(
            alignment: Alignment.center,
            margin: EdgeInsets.only(top: 28),
            padding: EdgeInsets.only(top: 12, left: 24, bottom: 12, right: 24),
            decoration: BoxDecoration(
              color: colorPalettes.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20), topRight: Radius.circular(20)),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  height: 10.h,
                  width: 85.w,
                  margin: EdgeInsets.only(bottom: 30.h),
                  decoration: BoxDecoration(
                      color: colorPalettes.grey3,
                      borderRadius: BorderRadius.all(Radius.circular(50))),
                ),
                _tabbBar(setState),
                Container(child: _tabbarView)
              ],
            ),
          )
      );
    });
  }

  Widget _tabbBar(StateSetter setState) {
    return Container(
      alignment: Alignment.centerLeft,
      child: TabBar(
        controller: controller,
        indicatorWeight: 4.3,
        labelColor: colorPalettes.lightPrimary,
        unselectedLabelColor: colorPalettes.grey5,
        indicatorSize: TabBarIndicatorSize.label,
        onTap: (int index) {
          setState(() {
            page = index;
          });
        },
        tabs: [
          Container(
            margin: EdgeInsets.only(bottom: 4),
            child: Tab(
              child: Text(
                appConstant.daftar,
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 4),
            child: Tab(
              child: Text(
                appConstant.masuk,
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget get _tabbarView {
    if (page == 0) {
      return RegisterForm();
      // return MultiBlocProvider(providers: [
      //   BlocProvider.value(
      //     value: Modular.get<LoginBloc>(),
      //   ),
      //   BlocProvider.value(
      //     value: Modular.get<RegisterBloc>(),
      //   )
      // ], child: RegisterForm());
    } else if (page == 1) {
      LoginForm();
      // return BlocProvider.value(
      //   value: Modular.get<LoginBloc>(),
      //   child: LoginForm(),
      // );
    }
    return LoginForm();
    // return BlocProvider(
    //   create: (context) => Modular.get<LoginBloc>(),
    //   child: LoginForm(),
    // );
  }
}

