import 'package:authentication/data/request/request.dart';
import 'package:authentication/data/response/response.dart';
import 'package:authentication/data/response/response_forgot_password.dart';
import 'package:authentication/im_authentication.dart';
import 'package:core/network/network.dart';
import 'package:dependencies/dependencies.dart';

import 'authentication_state.dart';

class AuthenticationCubit extends Cubit<AuthenticationState> {
  final AuthenticationUseCase useCase;

  AuthenticationCubit({this.useCase}): assert(useCase != null), super(Idle());

  doLogin(SigninRequest request) async {
    emit(AuthenticationState.loginLoading());
    var signinService = await useCase.doLogin(request: request);
    signinService.when(success: (ResponseSignin data) {
      emit(AuthenticationState.loginHashData(data: data));
    }, failure: (NetworkExceptions error) {
      emit(AuthenticationState.errorLogin(error: error));
    });
  }

  doRegister(RegisterRequest request) async {
    emit(AuthenticationState.registerLoading());
    var registerService = await useCase.doRegister(request: request);
    registerService.when(success: (ResponseSignup data) {
      emit(AuthenticationState.registerHashData(data: data));
    }, failure: (NetworkExceptions error) {
      emit(AuthenticationState.errorRegist(error: error));
    });
  }

  doForgotPassword(String email) async {
    emit(AuthenticationState.forgotPassLoading());
    var forgotService = await useCase.doForgotPassword(email: email);
    forgotService.when(success: (ResponseForgotPassword data) {
      emit(AuthenticationState.forgotPassHashData(data: data));
    }, failure: (NetworkExceptions error) {
      emit(AuthenticationState.errorForgotPass(error: error));
    });
  }

}