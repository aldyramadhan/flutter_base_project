// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'authentication_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$AuthenticationStateTearOff {
  const _$AuthenticationStateTearOff();

  Idle<T> idle<T>() {
    return Idle<T>();
  }

  LoginLoading<T> loginLoading<T>() {
    return LoginLoading<T>();
  }

  LoginHashData<T> loginHashData<T>({@required T data}) {
    return LoginHashData<T>(
      data: data,
    );
  }

  ErrorLogin<T> errorLogin<T>({@required NetworkExceptions error}) {
    return ErrorLogin<T>(
      error: error,
    );
  }

  GoogleLoading<T> googleLoading<T>() {
    return GoogleLoading<T>();
  }

  GoogleHashData<T> googleHashData<T>({@required T data}) {
    return GoogleHashData<T>(
      data: data,
    );
  }

  ErrorGoogle<T> errorGoogle<T>({@required NetworkExceptions error}) {
    return ErrorGoogle<T>(
      error: error,
    );
  }

  FacebookLoading<T> facebookLoading<T>() {
    return FacebookLoading<T>();
  }

  FacebookHashData<T> facebookHashData<T>({@required T data}) {
    return FacebookHashData<T>(
      data: data,
    );
  }

  ErrorFacebook<T> errorFacebook<T>({@required NetworkExceptions error}) {
    return ErrorFacebook<T>(
      error: error,
    );
  }

  RegisterLoading<T> registerLoading<T>() {
    return RegisterLoading<T>();
  }

  RegisterHashData<T> registerHashData<T>({@required T data}) {
    return RegisterHashData<T>(
      data: data,
    );
  }

  ErrorRegist<T> errorRegist<T>({@required NetworkExceptions error}) {
    return ErrorRegist<T>(
      error: error,
    );
  }

  OtpLoading<T> otpLoading<T>() {
    return OtpLoading<T>();
  }

  OtpHashData<T> otpHashData<T>({@required T data}) {
    return OtpHashData<T>(
      data: data,
    );
  }

  ResendOtpHashData<T> resendOtpHashData<T>({@required T data}) {
    return ResendOtpHashData<T>(
      data: data,
    );
  }

  SendOtpHashData<T> sendOtpHashData<T>({@required T data}) {
    return SendOtpHashData<T>(
      data: data,
    );
  }

  ErrorOtp<T> errorOtp<T>({@required NetworkExceptions error}) {
    return ErrorOtp<T>(
      error: error,
    );
  }

  ErrorResendOtp<T> errorResendOtp<T>({@required NetworkExceptions error}) {
    return ErrorResendOtp<T>(
      error: error,
    );
  }

  ErrorSendOtp<T> errorSendOtp<T>({@required NetworkExceptions error}) {
    return ErrorSendOtp<T>(
      error: error,
    );
  }

  ForgotPassLoading<T> forgotPassLoading<T>() {
    return ForgotPassLoading<T>();
  }

  ForgotPassHashData<T> forgotPassHashData<T>({@required T data}) {
    return ForgotPassHashData<T>(
      data: data,
    );
  }

  ErrorForgotPass<T> errorForgotPass<T>({@required NetworkExceptions error}) {
    return ErrorForgotPass<T>(
      error: error,
    );
  }

  ChangePasswordHashData<T> changePasswordHashData<T>({@required T data}) {
    return ChangePasswordHashData<T>(
      data: data,
    );
  }

  ErrorChangePassword<T> errorChangePassword<T>(
      {@required NetworkExceptions error}) {
    return ErrorChangePassword<T>(
      error: error,
    );
  }
}

// ignore: unused_element
const $AuthenticationState = _$AuthenticationStateTearOff();

mixin _$AuthenticationState<T> {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result idle(),
    @required Result loginLoading(),
    @required Result loginHashData(T data),
    @required Result errorLogin(NetworkExceptions error),
    @required Result googleLoading(),
    @required Result googleHashData(T data),
    @required Result errorGoogle(NetworkExceptions error),
    @required Result facebookLoading(),
    @required Result facebookHashData(T data),
    @required Result errorFacebook(NetworkExceptions error),
    @required Result registerLoading(),
    @required Result registerHashData(T data),
    @required Result errorRegist(NetworkExceptions error),
    @required Result otpLoading(),
    @required Result otpHashData(T data),
    @required Result resendOtpHashData(T data),
    @required Result sendOtpHashData(T data),
    @required Result errorOtp(NetworkExceptions error),
    @required Result errorResendOtp(NetworkExceptions error),
    @required Result errorSendOtp(NetworkExceptions error),
    @required Result forgotPassLoading(),
    @required Result forgotPassHashData(T data),
    @required Result errorForgotPass(NetworkExceptions error),
    @required Result changePasswordHashData(T data),
    @required Result errorChangePassword(NetworkExceptions error),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result idle(),
    Result loginLoading(),
    Result loginHashData(T data),
    Result errorLogin(NetworkExceptions error),
    Result googleLoading(),
    Result googleHashData(T data),
    Result errorGoogle(NetworkExceptions error),
    Result facebookLoading(),
    Result facebookHashData(T data),
    Result errorFacebook(NetworkExceptions error),
    Result registerLoading(),
    Result registerHashData(T data),
    Result errorRegist(NetworkExceptions error),
    Result otpLoading(),
    Result otpHashData(T data),
    Result resendOtpHashData(T data),
    Result sendOtpHashData(T data),
    Result errorOtp(NetworkExceptions error),
    Result errorResendOtp(NetworkExceptions error),
    Result errorSendOtp(NetworkExceptions error),
    Result forgotPassLoading(),
    Result forgotPassHashData(T data),
    Result errorForgotPass(NetworkExceptions error),
    Result changePasswordHashData(T data),
    Result errorChangePassword(NetworkExceptions error),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result idle(Idle<T> value),
    @required Result loginLoading(LoginLoading<T> value),
    @required Result loginHashData(LoginHashData<T> value),
    @required Result errorLogin(ErrorLogin<T> value),
    @required Result googleLoading(GoogleLoading<T> value),
    @required Result googleHashData(GoogleHashData<T> value),
    @required Result errorGoogle(ErrorGoogle<T> value),
    @required Result facebookLoading(FacebookLoading<T> value),
    @required Result facebookHashData(FacebookHashData<T> value),
    @required Result errorFacebook(ErrorFacebook<T> value),
    @required Result registerLoading(RegisterLoading<T> value),
    @required Result registerHashData(RegisterHashData<T> value),
    @required Result errorRegist(ErrorRegist<T> value),
    @required Result otpLoading(OtpLoading<T> value),
    @required Result otpHashData(OtpHashData<T> value),
    @required Result resendOtpHashData(ResendOtpHashData<T> value),
    @required Result sendOtpHashData(SendOtpHashData<T> value),
    @required Result errorOtp(ErrorOtp<T> value),
    @required Result errorResendOtp(ErrorResendOtp<T> value),
    @required Result errorSendOtp(ErrorSendOtp<T> value),
    @required Result forgotPassLoading(ForgotPassLoading<T> value),
    @required Result forgotPassHashData(ForgotPassHashData<T> value),
    @required Result errorForgotPass(ErrorForgotPass<T> value),
    @required Result changePasswordHashData(ChangePasswordHashData<T> value),
    @required Result errorChangePassword(ErrorChangePassword<T> value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result idle(Idle<T> value),
    Result loginLoading(LoginLoading<T> value),
    Result loginHashData(LoginHashData<T> value),
    Result errorLogin(ErrorLogin<T> value),
    Result googleLoading(GoogleLoading<T> value),
    Result googleHashData(GoogleHashData<T> value),
    Result errorGoogle(ErrorGoogle<T> value),
    Result facebookLoading(FacebookLoading<T> value),
    Result facebookHashData(FacebookHashData<T> value),
    Result errorFacebook(ErrorFacebook<T> value),
    Result registerLoading(RegisterLoading<T> value),
    Result registerHashData(RegisterHashData<T> value),
    Result errorRegist(ErrorRegist<T> value),
    Result otpLoading(OtpLoading<T> value),
    Result otpHashData(OtpHashData<T> value),
    Result resendOtpHashData(ResendOtpHashData<T> value),
    Result sendOtpHashData(SendOtpHashData<T> value),
    Result errorOtp(ErrorOtp<T> value),
    Result errorResendOtp(ErrorResendOtp<T> value),
    Result errorSendOtp(ErrorSendOtp<T> value),
    Result forgotPassLoading(ForgotPassLoading<T> value),
    Result forgotPassHashData(ForgotPassHashData<T> value),
    Result errorForgotPass(ErrorForgotPass<T> value),
    Result changePasswordHashData(ChangePasswordHashData<T> value),
    Result errorChangePassword(ErrorChangePassword<T> value),
    @required Result orElse(),
  });
}

abstract class $AuthenticationStateCopyWith<T, $Res> {
  factory $AuthenticationStateCopyWith(AuthenticationState<T> value,
          $Res Function(AuthenticationState<T>) then) =
      _$AuthenticationStateCopyWithImpl<T, $Res>;
}

class _$AuthenticationStateCopyWithImpl<T, $Res>
    implements $AuthenticationStateCopyWith<T, $Res> {
  _$AuthenticationStateCopyWithImpl(this._value, this._then);

  final AuthenticationState<T> _value;
  // ignore: unused_field
  final $Res Function(AuthenticationState<T>) _then;
}

abstract class $IdleCopyWith<T, $Res> {
  factory $IdleCopyWith(Idle<T> value, $Res Function(Idle<T>) then) =
      _$IdleCopyWithImpl<T, $Res>;
}

class _$IdleCopyWithImpl<T, $Res>
    extends _$AuthenticationStateCopyWithImpl<T, $Res>
    implements $IdleCopyWith<T, $Res> {
  _$IdleCopyWithImpl(Idle<T> _value, $Res Function(Idle<T>) _then)
      : super(_value, (v) => _then(v as Idle<T>));

  @override
  Idle<T> get _value => super._value as Idle<T>;
}

class _$Idle<T> implements Idle<T> {
  const _$Idle();

  @override
  String toString() {
    return 'AuthenticationState<$T>.idle()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is Idle<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result idle(),
    @required Result loginLoading(),
    @required Result loginHashData(T data),
    @required Result errorLogin(NetworkExceptions error),
    @required Result googleLoading(),
    @required Result googleHashData(T data),
    @required Result errorGoogle(NetworkExceptions error),
    @required Result facebookLoading(),
    @required Result facebookHashData(T data),
    @required Result errorFacebook(NetworkExceptions error),
    @required Result registerLoading(),
    @required Result registerHashData(T data),
    @required Result errorRegist(NetworkExceptions error),
    @required Result otpLoading(),
    @required Result otpHashData(T data),
    @required Result resendOtpHashData(T data),
    @required Result sendOtpHashData(T data),
    @required Result errorOtp(NetworkExceptions error),
    @required Result errorResendOtp(NetworkExceptions error),
    @required Result errorSendOtp(NetworkExceptions error),
    @required Result forgotPassLoading(),
    @required Result forgotPassHashData(T data),
    @required Result errorForgotPass(NetworkExceptions error),
    @required Result changePasswordHashData(T data),
    @required Result errorChangePassword(NetworkExceptions error),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return idle();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result idle(),
    Result loginLoading(),
    Result loginHashData(T data),
    Result errorLogin(NetworkExceptions error),
    Result googleLoading(),
    Result googleHashData(T data),
    Result errorGoogle(NetworkExceptions error),
    Result facebookLoading(),
    Result facebookHashData(T data),
    Result errorFacebook(NetworkExceptions error),
    Result registerLoading(),
    Result registerHashData(T data),
    Result errorRegist(NetworkExceptions error),
    Result otpLoading(),
    Result otpHashData(T data),
    Result resendOtpHashData(T data),
    Result sendOtpHashData(T data),
    Result errorOtp(NetworkExceptions error),
    Result errorResendOtp(NetworkExceptions error),
    Result errorSendOtp(NetworkExceptions error),
    Result forgotPassLoading(),
    Result forgotPassHashData(T data),
    Result errorForgotPass(NetworkExceptions error),
    Result changePasswordHashData(T data),
    Result errorChangePassword(NetworkExceptions error),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (idle != null) {
      return idle();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result idle(Idle<T> value),
    @required Result loginLoading(LoginLoading<T> value),
    @required Result loginHashData(LoginHashData<T> value),
    @required Result errorLogin(ErrorLogin<T> value),
    @required Result googleLoading(GoogleLoading<T> value),
    @required Result googleHashData(GoogleHashData<T> value),
    @required Result errorGoogle(ErrorGoogle<T> value),
    @required Result facebookLoading(FacebookLoading<T> value),
    @required Result facebookHashData(FacebookHashData<T> value),
    @required Result errorFacebook(ErrorFacebook<T> value),
    @required Result registerLoading(RegisterLoading<T> value),
    @required Result registerHashData(RegisterHashData<T> value),
    @required Result errorRegist(ErrorRegist<T> value),
    @required Result otpLoading(OtpLoading<T> value),
    @required Result otpHashData(OtpHashData<T> value),
    @required Result resendOtpHashData(ResendOtpHashData<T> value),
    @required Result sendOtpHashData(SendOtpHashData<T> value),
    @required Result errorOtp(ErrorOtp<T> value),
    @required Result errorResendOtp(ErrorResendOtp<T> value),
    @required Result errorSendOtp(ErrorSendOtp<T> value),
    @required Result forgotPassLoading(ForgotPassLoading<T> value),
    @required Result forgotPassHashData(ForgotPassHashData<T> value),
    @required Result errorForgotPass(ErrorForgotPass<T> value),
    @required Result changePasswordHashData(ChangePasswordHashData<T> value),
    @required Result errorChangePassword(ErrorChangePassword<T> value),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return idle(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result idle(Idle<T> value),
    Result loginLoading(LoginLoading<T> value),
    Result loginHashData(LoginHashData<T> value),
    Result errorLogin(ErrorLogin<T> value),
    Result googleLoading(GoogleLoading<T> value),
    Result googleHashData(GoogleHashData<T> value),
    Result errorGoogle(ErrorGoogle<T> value),
    Result facebookLoading(FacebookLoading<T> value),
    Result facebookHashData(FacebookHashData<T> value),
    Result errorFacebook(ErrorFacebook<T> value),
    Result registerLoading(RegisterLoading<T> value),
    Result registerHashData(RegisterHashData<T> value),
    Result errorRegist(ErrorRegist<T> value),
    Result otpLoading(OtpLoading<T> value),
    Result otpHashData(OtpHashData<T> value),
    Result resendOtpHashData(ResendOtpHashData<T> value),
    Result sendOtpHashData(SendOtpHashData<T> value),
    Result errorOtp(ErrorOtp<T> value),
    Result errorResendOtp(ErrorResendOtp<T> value),
    Result errorSendOtp(ErrorSendOtp<T> value),
    Result forgotPassLoading(ForgotPassLoading<T> value),
    Result forgotPassHashData(ForgotPassHashData<T> value),
    Result errorForgotPass(ErrorForgotPass<T> value),
    Result changePasswordHashData(ChangePasswordHashData<T> value),
    Result errorChangePassword(ErrorChangePassword<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (idle != null) {
      return idle(this);
    }
    return orElse();
  }
}

abstract class Idle<T> implements AuthenticationState<T> {
  const factory Idle() = _$Idle<T>;
}

abstract class $LoginLoadingCopyWith<T, $Res> {
  factory $LoginLoadingCopyWith(
          LoginLoading<T> value, $Res Function(LoginLoading<T>) then) =
      _$LoginLoadingCopyWithImpl<T, $Res>;
}

class _$LoginLoadingCopyWithImpl<T, $Res>
    extends _$AuthenticationStateCopyWithImpl<T, $Res>
    implements $LoginLoadingCopyWith<T, $Res> {
  _$LoginLoadingCopyWithImpl(
      LoginLoading<T> _value, $Res Function(LoginLoading<T>) _then)
      : super(_value, (v) => _then(v as LoginLoading<T>));

  @override
  LoginLoading<T> get _value => super._value as LoginLoading<T>;
}

class _$LoginLoading<T> implements LoginLoading<T> {
  const _$LoginLoading();

  @override
  String toString() {
    return 'AuthenticationState<$T>.loginLoading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is LoginLoading<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result idle(),
    @required Result loginLoading(),
    @required Result loginHashData(T data),
    @required Result errorLogin(NetworkExceptions error),
    @required Result googleLoading(),
    @required Result googleHashData(T data),
    @required Result errorGoogle(NetworkExceptions error),
    @required Result facebookLoading(),
    @required Result facebookHashData(T data),
    @required Result errorFacebook(NetworkExceptions error),
    @required Result registerLoading(),
    @required Result registerHashData(T data),
    @required Result errorRegist(NetworkExceptions error),
    @required Result otpLoading(),
    @required Result otpHashData(T data),
    @required Result resendOtpHashData(T data),
    @required Result sendOtpHashData(T data),
    @required Result errorOtp(NetworkExceptions error),
    @required Result errorResendOtp(NetworkExceptions error),
    @required Result errorSendOtp(NetworkExceptions error),
    @required Result forgotPassLoading(),
    @required Result forgotPassHashData(T data),
    @required Result errorForgotPass(NetworkExceptions error),
    @required Result changePasswordHashData(T data),
    @required Result errorChangePassword(NetworkExceptions error),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return loginLoading();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result idle(),
    Result loginLoading(),
    Result loginHashData(T data),
    Result errorLogin(NetworkExceptions error),
    Result googleLoading(),
    Result googleHashData(T data),
    Result errorGoogle(NetworkExceptions error),
    Result facebookLoading(),
    Result facebookHashData(T data),
    Result errorFacebook(NetworkExceptions error),
    Result registerLoading(),
    Result registerHashData(T data),
    Result errorRegist(NetworkExceptions error),
    Result otpLoading(),
    Result otpHashData(T data),
    Result resendOtpHashData(T data),
    Result sendOtpHashData(T data),
    Result errorOtp(NetworkExceptions error),
    Result errorResendOtp(NetworkExceptions error),
    Result errorSendOtp(NetworkExceptions error),
    Result forgotPassLoading(),
    Result forgotPassHashData(T data),
    Result errorForgotPass(NetworkExceptions error),
    Result changePasswordHashData(T data),
    Result errorChangePassword(NetworkExceptions error),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loginLoading != null) {
      return loginLoading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result idle(Idle<T> value),
    @required Result loginLoading(LoginLoading<T> value),
    @required Result loginHashData(LoginHashData<T> value),
    @required Result errorLogin(ErrorLogin<T> value),
    @required Result googleLoading(GoogleLoading<T> value),
    @required Result googleHashData(GoogleHashData<T> value),
    @required Result errorGoogle(ErrorGoogle<T> value),
    @required Result facebookLoading(FacebookLoading<T> value),
    @required Result facebookHashData(FacebookHashData<T> value),
    @required Result errorFacebook(ErrorFacebook<T> value),
    @required Result registerLoading(RegisterLoading<T> value),
    @required Result registerHashData(RegisterHashData<T> value),
    @required Result errorRegist(ErrorRegist<T> value),
    @required Result otpLoading(OtpLoading<T> value),
    @required Result otpHashData(OtpHashData<T> value),
    @required Result resendOtpHashData(ResendOtpHashData<T> value),
    @required Result sendOtpHashData(SendOtpHashData<T> value),
    @required Result errorOtp(ErrorOtp<T> value),
    @required Result errorResendOtp(ErrorResendOtp<T> value),
    @required Result errorSendOtp(ErrorSendOtp<T> value),
    @required Result forgotPassLoading(ForgotPassLoading<T> value),
    @required Result forgotPassHashData(ForgotPassHashData<T> value),
    @required Result errorForgotPass(ErrorForgotPass<T> value),
    @required Result changePasswordHashData(ChangePasswordHashData<T> value),
    @required Result errorChangePassword(ErrorChangePassword<T> value),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return loginLoading(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result idle(Idle<T> value),
    Result loginLoading(LoginLoading<T> value),
    Result loginHashData(LoginHashData<T> value),
    Result errorLogin(ErrorLogin<T> value),
    Result googleLoading(GoogleLoading<T> value),
    Result googleHashData(GoogleHashData<T> value),
    Result errorGoogle(ErrorGoogle<T> value),
    Result facebookLoading(FacebookLoading<T> value),
    Result facebookHashData(FacebookHashData<T> value),
    Result errorFacebook(ErrorFacebook<T> value),
    Result registerLoading(RegisterLoading<T> value),
    Result registerHashData(RegisterHashData<T> value),
    Result errorRegist(ErrorRegist<T> value),
    Result otpLoading(OtpLoading<T> value),
    Result otpHashData(OtpHashData<T> value),
    Result resendOtpHashData(ResendOtpHashData<T> value),
    Result sendOtpHashData(SendOtpHashData<T> value),
    Result errorOtp(ErrorOtp<T> value),
    Result errorResendOtp(ErrorResendOtp<T> value),
    Result errorSendOtp(ErrorSendOtp<T> value),
    Result forgotPassLoading(ForgotPassLoading<T> value),
    Result forgotPassHashData(ForgotPassHashData<T> value),
    Result errorForgotPass(ErrorForgotPass<T> value),
    Result changePasswordHashData(ChangePasswordHashData<T> value),
    Result errorChangePassword(ErrorChangePassword<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loginLoading != null) {
      return loginLoading(this);
    }
    return orElse();
  }
}

abstract class LoginLoading<T> implements AuthenticationState<T> {
  const factory LoginLoading() = _$LoginLoading<T>;
}

abstract class $LoginHashDataCopyWith<T, $Res> {
  factory $LoginHashDataCopyWith(
          LoginHashData<T> value, $Res Function(LoginHashData<T>) then) =
      _$LoginHashDataCopyWithImpl<T, $Res>;
  $Res call({T data});
}

class _$LoginHashDataCopyWithImpl<T, $Res>
    extends _$AuthenticationStateCopyWithImpl<T, $Res>
    implements $LoginHashDataCopyWith<T, $Res> {
  _$LoginHashDataCopyWithImpl(
      LoginHashData<T> _value, $Res Function(LoginHashData<T>) _then)
      : super(_value, (v) => _then(v as LoginHashData<T>));

  @override
  LoginHashData<T> get _value => super._value as LoginHashData<T>;

  @override
  $Res call({
    Object data = freezed,
  }) {
    return _then(LoginHashData<T>(
      data: data == freezed ? _value.data : data as T,
    ));
  }
}

class _$LoginHashData<T> implements LoginHashData<T> {
  const _$LoginHashData({@required this.data}) : assert(data != null);

  @override
  final T data;

  @override
  String toString() {
    return 'AuthenticationState<$T>.loginHashData(data: $data)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is LoginHashData<T> &&
            (identical(other.data, data) ||
                const DeepCollectionEquality().equals(other.data, data)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(data);

  @override
  $LoginHashDataCopyWith<T, LoginHashData<T>> get copyWith =>
      _$LoginHashDataCopyWithImpl<T, LoginHashData<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result idle(),
    @required Result loginLoading(),
    @required Result loginHashData(T data),
    @required Result errorLogin(NetworkExceptions error),
    @required Result googleLoading(),
    @required Result googleHashData(T data),
    @required Result errorGoogle(NetworkExceptions error),
    @required Result facebookLoading(),
    @required Result facebookHashData(T data),
    @required Result errorFacebook(NetworkExceptions error),
    @required Result registerLoading(),
    @required Result registerHashData(T data),
    @required Result errorRegist(NetworkExceptions error),
    @required Result otpLoading(),
    @required Result otpHashData(T data),
    @required Result resendOtpHashData(T data),
    @required Result sendOtpHashData(T data),
    @required Result errorOtp(NetworkExceptions error),
    @required Result errorResendOtp(NetworkExceptions error),
    @required Result errorSendOtp(NetworkExceptions error),
    @required Result forgotPassLoading(),
    @required Result forgotPassHashData(T data),
    @required Result errorForgotPass(NetworkExceptions error),
    @required Result changePasswordHashData(T data),
    @required Result errorChangePassword(NetworkExceptions error),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return loginHashData(data);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result idle(),
    Result loginLoading(),
    Result loginHashData(T data),
    Result errorLogin(NetworkExceptions error),
    Result googleLoading(),
    Result googleHashData(T data),
    Result errorGoogle(NetworkExceptions error),
    Result facebookLoading(),
    Result facebookHashData(T data),
    Result errorFacebook(NetworkExceptions error),
    Result registerLoading(),
    Result registerHashData(T data),
    Result errorRegist(NetworkExceptions error),
    Result otpLoading(),
    Result otpHashData(T data),
    Result resendOtpHashData(T data),
    Result sendOtpHashData(T data),
    Result errorOtp(NetworkExceptions error),
    Result errorResendOtp(NetworkExceptions error),
    Result errorSendOtp(NetworkExceptions error),
    Result forgotPassLoading(),
    Result forgotPassHashData(T data),
    Result errorForgotPass(NetworkExceptions error),
    Result changePasswordHashData(T data),
    Result errorChangePassword(NetworkExceptions error),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loginHashData != null) {
      return loginHashData(data);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result idle(Idle<T> value),
    @required Result loginLoading(LoginLoading<T> value),
    @required Result loginHashData(LoginHashData<T> value),
    @required Result errorLogin(ErrorLogin<T> value),
    @required Result googleLoading(GoogleLoading<T> value),
    @required Result googleHashData(GoogleHashData<T> value),
    @required Result errorGoogle(ErrorGoogle<T> value),
    @required Result facebookLoading(FacebookLoading<T> value),
    @required Result facebookHashData(FacebookHashData<T> value),
    @required Result errorFacebook(ErrorFacebook<T> value),
    @required Result registerLoading(RegisterLoading<T> value),
    @required Result registerHashData(RegisterHashData<T> value),
    @required Result errorRegist(ErrorRegist<T> value),
    @required Result otpLoading(OtpLoading<T> value),
    @required Result otpHashData(OtpHashData<T> value),
    @required Result resendOtpHashData(ResendOtpHashData<T> value),
    @required Result sendOtpHashData(SendOtpHashData<T> value),
    @required Result errorOtp(ErrorOtp<T> value),
    @required Result errorResendOtp(ErrorResendOtp<T> value),
    @required Result errorSendOtp(ErrorSendOtp<T> value),
    @required Result forgotPassLoading(ForgotPassLoading<T> value),
    @required Result forgotPassHashData(ForgotPassHashData<T> value),
    @required Result errorForgotPass(ErrorForgotPass<T> value),
    @required Result changePasswordHashData(ChangePasswordHashData<T> value),
    @required Result errorChangePassword(ErrorChangePassword<T> value),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return loginHashData(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result idle(Idle<T> value),
    Result loginLoading(LoginLoading<T> value),
    Result loginHashData(LoginHashData<T> value),
    Result errorLogin(ErrorLogin<T> value),
    Result googleLoading(GoogleLoading<T> value),
    Result googleHashData(GoogleHashData<T> value),
    Result errorGoogle(ErrorGoogle<T> value),
    Result facebookLoading(FacebookLoading<T> value),
    Result facebookHashData(FacebookHashData<T> value),
    Result errorFacebook(ErrorFacebook<T> value),
    Result registerLoading(RegisterLoading<T> value),
    Result registerHashData(RegisterHashData<T> value),
    Result errorRegist(ErrorRegist<T> value),
    Result otpLoading(OtpLoading<T> value),
    Result otpHashData(OtpHashData<T> value),
    Result resendOtpHashData(ResendOtpHashData<T> value),
    Result sendOtpHashData(SendOtpHashData<T> value),
    Result errorOtp(ErrorOtp<T> value),
    Result errorResendOtp(ErrorResendOtp<T> value),
    Result errorSendOtp(ErrorSendOtp<T> value),
    Result forgotPassLoading(ForgotPassLoading<T> value),
    Result forgotPassHashData(ForgotPassHashData<T> value),
    Result errorForgotPass(ErrorForgotPass<T> value),
    Result changePasswordHashData(ChangePasswordHashData<T> value),
    Result errorChangePassword(ErrorChangePassword<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loginHashData != null) {
      return loginHashData(this);
    }
    return orElse();
  }
}

abstract class LoginHashData<T> implements AuthenticationState<T> {
  const factory LoginHashData({@required T data}) = _$LoginHashData<T>;

  T get data;
  $LoginHashDataCopyWith<T, LoginHashData<T>> get copyWith;
}

abstract class $ErrorLoginCopyWith<T, $Res> {
  factory $ErrorLoginCopyWith(
          ErrorLogin<T> value, $Res Function(ErrorLogin<T>) then) =
      _$ErrorLoginCopyWithImpl<T, $Res>;
  $Res call({NetworkExceptions error});

  $NetworkExceptionsCopyWith<$Res> get error;
}

class _$ErrorLoginCopyWithImpl<T, $Res>
    extends _$AuthenticationStateCopyWithImpl<T, $Res>
    implements $ErrorLoginCopyWith<T, $Res> {
  _$ErrorLoginCopyWithImpl(
      ErrorLogin<T> _value, $Res Function(ErrorLogin<T>) _then)
      : super(_value, (v) => _then(v as ErrorLogin<T>));

  @override
  ErrorLogin<T> get _value => super._value as ErrorLogin<T>;

  @override
  $Res call({
    Object error = freezed,
  }) {
    return _then(ErrorLogin<T>(
      error: error == freezed ? _value.error : error as NetworkExceptions,
    ));
  }

  @override
  $NetworkExceptionsCopyWith<$Res> get error {
    if (_value.error == null) {
      return null;
    }
    return $NetworkExceptionsCopyWith<$Res>(_value.error, (value) {
      return _then(_value.copyWith(error: value));
    });
  }
}

class _$ErrorLogin<T> implements ErrorLogin<T> {
  const _$ErrorLogin({@required this.error}) : assert(error != null);

  @override
  final NetworkExceptions error;

  @override
  String toString() {
    return 'AuthenticationState<$T>.errorLogin(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ErrorLogin<T> &&
            (identical(other.error, error) ||
                const DeepCollectionEquality().equals(other.error, error)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(error);

  @override
  $ErrorLoginCopyWith<T, ErrorLogin<T>> get copyWith =>
      _$ErrorLoginCopyWithImpl<T, ErrorLogin<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result idle(),
    @required Result loginLoading(),
    @required Result loginHashData(T data),
    @required Result errorLogin(NetworkExceptions error),
    @required Result googleLoading(),
    @required Result googleHashData(T data),
    @required Result errorGoogle(NetworkExceptions error),
    @required Result facebookLoading(),
    @required Result facebookHashData(T data),
    @required Result errorFacebook(NetworkExceptions error),
    @required Result registerLoading(),
    @required Result registerHashData(T data),
    @required Result errorRegist(NetworkExceptions error),
    @required Result otpLoading(),
    @required Result otpHashData(T data),
    @required Result resendOtpHashData(T data),
    @required Result sendOtpHashData(T data),
    @required Result errorOtp(NetworkExceptions error),
    @required Result errorResendOtp(NetworkExceptions error),
    @required Result errorSendOtp(NetworkExceptions error),
    @required Result forgotPassLoading(),
    @required Result forgotPassHashData(T data),
    @required Result errorForgotPass(NetworkExceptions error),
    @required Result changePasswordHashData(T data),
    @required Result errorChangePassword(NetworkExceptions error),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return errorLogin(error);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result idle(),
    Result loginLoading(),
    Result loginHashData(T data),
    Result errorLogin(NetworkExceptions error),
    Result googleLoading(),
    Result googleHashData(T data),
    Result errorGoogle(NetworkExceptions error),
    Result facebookLoading(),
    Result facebookHashData(T data),
    Result errorFacebook(NetworkExceptions error),
    Result registerLoading(),
    Result registerHashData(T data),
    Result errorRegist(NetworkExceptions error),
    Result otpLoading(),
    Result otpHashData(T data),
    Result resendOtpHashData(T data),
    Result sendOtpHashData(T data),
    Result errorOtp(NetworkExceptions error),
    Result errorResendOtp(NetworkExceptions error),
    Result errorSendOtp(NetworkExceptions error),
    Result forgotPassLoading(),
    Result forgotPassHashData(T data),
    Result errorForgotPass(NetworkExceptions error),
    Result changePasswordHashData(T data),
    Result errorChangePassword(NetworkExceptions error),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (errorLogin != null) {
      return errorLogin(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result idle(Idle<T> value),
    @required Result loginLoading(LoginLoading<T> value),
    @required Result loginHashData(LoginHashData<T> value),
    @required Result errorLogin(ErrorLogin<T> value),
    @required Result googleLoading(GoogleLoading<T> value),
    @required Result googleHashData(GoogleHashData<T> value),
    @required Result errorGoogle(ErrorGoogle<T> value),
    @required Result facebookLoading(FacebookLoading<T> value),
    @required Result facebookHashData(FacebookHashData<T> value),
    @required Result errorFacebook(ErrorFacebook<T> value),
    @required Result registerLoading(RegisterLoading<T> value),
    @required Result registerHashData(RegisterHashData<T> value),
    @required Result errorRegist(ErrorRegist<T> value),
    @required Result otpLoading(OtpLoading<T> value),
    @required Result otpHashData(OtpHashData<T> value),
    @required Result resendOtpHashData(ResendOtpHashData<T> value),
    @required Result sendOtpHashData(SendOtpHashData<T> value),
    @required Result errorOtp(ErrorOtp<T> value),
    @required Result errorResendOtp(ErrorResendOtp<T> value),
    @required Result errorSendOtp(ErrorSendOtp<T> value),
    @required Result forgotPassLoading(ForgotPassLoading<T> value),
    @required Result forgotPassHashData(ForgotPassHashData<T> value),
    @required Result errorForgotPass(ErrorForgotPass<T> value),
    @required Result changePasswordHashData(ChangePasswordHashData<T> value),
    @required Result errorChangePassword(ErrorChangePassword<T> value),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return errorLogin(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result idle(Idle<T> value),
    Result loginLoading(LoginLoading<T> value),
    Result loginHashData(LoginHashData<T> value),
    Result errorLogin(ErrorLogin<T> value),
    Result googleLoading(GoogleLoading<T> value),
    Result googleHashData(GoogleHashData<T> value),
    Result errorGoogle(ErrorGoogle<T> value),
    Result facebookLoading(FacebookLoading<T> value),
    Result facebookHashData(FacebookHashData<T> value),
    Result errorFacebook(ErrorFacebook<T> value),
    Result registerLoading(RegisterLoading<T> value),
    Result registerHashData(RegisterHashData<T> value),
    Result errorRegist(ErrorRegist<T> value),
    Result otpLoading(OtpLoading<T> value),
    Result otpHashData(OtpHashData<T> value),
    Result resendOtpHashData(ResendOtpHashData<T> value),
    Result sendOtpHashData(SendOtpHashData<T> value),
    Result errorOtp(ErrorOtp<T> value),
    Result errorResendOtp(ErrorResendOtp<T> value),
    Result errorSendOtp(ErrorSendOtp<T> value),
    Result forgotPassLoading(ForgotPassLoading<T> value),
    Result forgotPassHashData(ForgotPassHashData<T> value),
    Result errorForgotPass(ErrorForgotPass<T> value),
    Result changePasswordHashData(ChangePasswordHashData<T> value),
    Result errorChangePassword(ErrorChangePassword<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (errorLogin != null) {
      return errorLogin(this);
    }
    return orElse();
  }
}

abstract class ErrorLogin<T> implements AuthenticationState<T> {
  const factory ErrorLogin({@required NetworkExceptions error}) =
      _$ErrorLogin<T>;

  NetworkExceptions get error;
  $ErrorLoginCopyWith<T, ErrorLogin<T>> get copyWith;
}

abstract class $GoogleLoadingCopyWith<T, $Res> {
  factory $GoogleLoadingCopyWith(
          GoogleLoading<T> value, $Res Function(GoogleLoading<T>) then) =
      _$GoogleLoadingCopyWithImpl<T, $Res>;
}

class _$GoogleLoadingCopyWithImpl<T, $Res>
    extends _$AuthenticationStateCopyWithImpl<T, $Res>
    implements $GoogleLoadingCopyWith<T, $Res> {
  _$GoogleLoadingCopyWithImpl(
      GoogleLoading<T> _value, $Res Function(GoogleLoading<T>) _then)
      : super(_value, (v) => _then(v as GoogleLoading<T>));

  @override
  GoogleLoading<T> get _value => super._value as GoogleLoading<T>;
}

class _$GoogleLoading<T> implements GoogleLoading<T> {
  const _$GoogleLoading();

  @override
  String toString() {
    return 'AuthenticationState<$T>.googleLoading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is GoogleLoading<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result idle(),
    @required Result loginLoading(),
    @required Result loginHashData(T data),
    @required Result errorLogin(NetworkExceptions error),
    @required Result googleLoading(),
    @required Result googleHashData(T data),
    @required Result errorGoogle(NetworkExceptions error),
    @required Result facebookLoading(),
    @required Result facebookHashData(T data),
    @required Result errorFacebook(NetworkExceptions error),
    @required Result registerLoading(),
    @required Result registerHashData(T data),
    @required Result errorRegist(NetworkExceptions error),
    @required Result otpLoading(),
    @required Result otpHashData(T data),
    @required Result resendOtpHashData(T data),
    @required Result sendOtpHashData(T data),
    @required Result errorOtp(NetworkExceptions error),
    @required Result errorResendOtp(NetworkExceptions error),
    @required Result errorSendOtp(NetworkExceptions error),
    @required Result forgotPassLoading(),
    @required Result forgotPassHashData(T data),
    @required Result errorForgotPass(NetworkExceptions error),
    @required Result changePasswordHashData(T data),
    @required Result errorChangePassword(NetworkExceptions error),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return googleLoading();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result idle(),
    Result loginLoading(),
    Result loginHashData(T data),
    Result errorLogin(NetworkExceptions error),
    Result googleLoading(),
    Result googleHashData(T data),
    Result errorGoogle(NetworkExceptions error),
    Result facebookLoading(),
    Result facebookHashData(T data),
    Result errorFacebook(NetworkExceptions error),
    Result registerLoading(),
    Result registerHashData(T data),
    Result errorRegist(NetworkExceptions error),
    Result otpLoading(),
    Result otpHashData(T data),
    Result resendOtpHashData(T data),
    Result sendOtpHashData(T data),
    Result errorOtp(NetworkExceptions error),
    Result errorResendOtp(NetworkExceptions error),
    Result errorSendOtp(NetworkExceptions error),
    Result forgotPassLoading(),
    Result forgotPassHashData(T data),
    Result errorForgotPass(NetworkExceptions error),
    Result changePasswordHashData(T data),
    Result errorChangePassword(NetworkExceptions error),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (googleLoading != null) {
      return googleLoading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result idle(Idle<T> value),
    @required Result loginLoading(LoginLoading<T> value),
    @required Result loginHashData(LoginHashData<T> value),
    @required Result errorLogin(ErrorLogin<T> value),
    @required Result googleLoading(GoogleLoading<T> value),
    @required Result googleHashData(GoogleHashData<T> value),
    @required Result errorGoogle(ErrorGoogle<T> value),
    @required Result facebookLoading(FacebookLoading<T> value),
    @required Result facebookHashData(FacebookHashData<T> value),
    @required Result errorFacebook(ErrorFacebook<T> value),
    @required Result registerLoading(RegisterLoading<T> value),
    @required Result registerHashData(RegisterHashData<T> value),
    @required Result errorRegist(ErrorRegist<T> value),
    @required Result otpLoading(OtpLoading<T> value),
    @required Result otpHashData(OtpHashData<T> value),
    @required Result resendOtpHashData(ResendOtpHashData<T> value),
    @required Result sendOtpHashData(SendOtpHashData<T> value),
    @required Result errorOtp(ErrorOtp<T> value),
    @required Result errorResendOtp(ErrorResendOtp<T> value),
    @required Result errorSendOtp(ErrorSendOtp<T> value),
    @required Result forgotPassLoading(ForgotPassLoading<T> value),
    @required Result forgotPassHashData(ForgotPassHashData<T> value),
    @required Result errorForgotPass(ErrorForgotPass<T> value),
    @required Result changePasswordHashData(ChangePasswordHashData<T> value),
    @required Result errorChangePassword(ErrorChangePassword<T> value),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return googleLoading(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result idle(Idle<T> value),
    Result loginLoading(LoginLoading<T> value),
    Result loginHashData(LoginHashData<T> value),
    Result errorLogin(ErrorLogin<T> value),
    Result googleLoading(GoogleLoading<T> value),
    Result googleHashData(GoogleHashData<T> value),
    Result errorGoogle(ErrorGoogle<T> value),
    Result facebookLoading(FacebookLoading<T> value),
    Result facebookHashData(FacebookHashData<T> value),
    Result errorFacebook(ErrorFacebook<T> value),
    Result registerLoading(RegisterLoading<T> value),
    Result registerHashData(RegisterHashData<T> value),
    Result errorRegist(ErrorRegist<T> value),
    Result otpLoading(OtpLoading<T> value),
    Result otpHashData(OtpHashData<T> value),
    Result resendOtpHashData(ResendOtpHashData<T> value),
    Result sendOtpHashData(SendOtpHashData<T> value),
    Result errorOtp(ErrorOtp<T> value),
    Result errorResendOtp(ErrorResendOtp<T> value),
    Result errorSendOtp(ErrorSendOtp<T> value),
    Result forgotPassLoading(ForgotPassLoading<T> value),
    Result forgotPassHashData(ForgotPassHashData<T> value),
    Result errorForgotPass(ErrorForgotPass<T> value),
    Result changePasswordHashData(ChangePasswordHashData<T> value),
    Result errorChangePassword(ErrorChangePassword<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (googleLoading != null) {
      return googleLoading(this);
    }
    return orElse();
  }
}

abstract class GoogleLoading<T> implements AuthenticationState<T> {
  const factory GoogleLoading() = _$GoogleLoading<T>;
}

abstract class $GoogleHashDataCopyWith<T, $Res> {
  factory $GoogleHashDataCopyWith(
          GoogleHashData<T> value, $Res Function(GoogleHashData<T>) then) =
      _$GoogleHashDataCopyWithImpl<T, $Res>;
  $Res call({T data});
}

class _$GoogleHashDataCopyWithImpl<T, $Res>
    extends _$AuthenticationStateCopyWithImpl<T, $Res>
    implements $GoogleHashDataCopyWith<T, $Res> {
  _$GoogleHashDataCopyWithImpl(
      GoogleHashData<T> _value, $Res Function(GoogleHashData<T>) _then)
      : super(_value, (v) => _then(v as GoogleHashData<T>));

  @override
  GoogleHashData<T> get _value => super._value as GoogleHashData<T>;

  @override
  $Res call({
    Object data = freezed,
  }) {
    return _then(GoogleHashData<T>(
      data: data == freezed ? _value.data : data as T,
    ));
  }
}

class _$GoogleHashData<T> implements GoogleHashData<T> {
  const _$GoogleHashData({@required this.data}) : assert(data != null);

  @override
  final T data;

  @override
  String toString() {
    return 'AuthenticationState<$T>.googleHashData(data: $data)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is GoogleHashData<T> &&
            (identical(other.data, data) ||
                const DeepCollectionEquality().equals(other.data, data)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(data);

  @override
  $GoogleHashDataCopyWith<T, GoogleHashData<T>> get copyWith =>
      _$GoogleHashDataCopyWithImpl<T, GoogleHashData<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result idle(),
    @required Result loginLoading(),
    @required Result loginHashData(T data),
    @required Result errorLogin(NetworkExceptions error),
    @required Result googleLoading(),
    @required Result googleHashData(T data),
    @required Result errorGoogle(NetworkExceptions error),
    @required Result facebookLoading(),
    @required Result facebookHashData(T data),
    @required Result errorFacebook(NetworkExceptions error),
    @required Result registerLoading(),
    @required Result registerHashData(T data),
    @required Result errorRegist(NetworkExceptions error),
    @required Result otpLoading(),
    @required Result otpHashData(T data),
    @required Result resendOtpHashData(T data),
    @required Result sendOtpHashData(T data),
    @required Result errorOtp(NetworkExceptions error),
    @required Result errorResendOtp(NetworkExceptions error),
    @required Result errorSendOtp(NetworkExceptions error),
    @required Result forgotPassLoading(),
    @required Result forgotPassHashData(T data),
    @required Result errorForgotPass(NetworkExceptions error),
    @required Result changePasswordHashData(T data),
    @required Result errorChangePassword(NetworkExceptions error),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return googleHashData(data);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result idle(),
    Result loginLoading(),
    Result loginHashData(T data),
    Result errorLogin(NetworkExceptions error),
    Result googleLoading(),
    Result googleHashData(T data),
    Result errorGoogle(NetworkExceptions error),
    Result facebookLoading(),
    Result facebookHashData(T data),
    Result errorFacebook(NetworkExceptions error),
    Result registerLoading(),
    Result registerHashData(T data),
    Result errorRegist(NetworkExceptions error),
    Result otpLoading(),
    Result otpHashData(T data),
    Result resendOtpHashData(T data),
    Result sendOtpHashData(T data),
    Result errorOtp(NetworkExceptions error),
    Result errorResendOtp(NetworkExceptions error),
    Result errorSendOtp(NetworkExceptions error),
    Result forgotPassLoading(),
    Result forgotPassHashData(T data),
    Result errorForgotPass(NetworkExceptions error),
    Result changePasswordHashData(T data),
    Result errorChangePassword(NetworkExceptions error),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (googleHashData != null) {
      return googleHashData(data);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result idle(Idle<T> value),
    @required Result loginLoading(LoginLoading<T> value),
    @required Result loginHashData(LoginHashData<T> value),
    @required Result errorLogin(ErrorLogin<T> value),
    @required Result googleLoading(GoogleLoading<T> value),
    @required Result googleHashData(GoogleHashData<T> value),
    @required Result errorGoogle(ErrorGoogle<T> value),
    @required Result facebookLoading(FacebookLoading<T> value),
    @required Result facebookHashData(FacebookHashData<T> value),
    @required Result errorFacebook(ErrorFacebook<T> value),
    @required Result registerLoading(RegisterLoading<T> value),
    @required Result registerHashData(RegisterHashData<T> value),
    @required Result errorRegist(ErrorRegist<T> value),
    @required Result otpLoading(OtpLoading<T> value),
    @required Result otpHashData(OtpHashData<T> value),
    @required Result resendOtpHashData(ResendOtpHashData<T> value),
    @required Result sendOtpHashData(SendOtpHashData<T> value),
    @required Result errorOtp(ErrorOtp<T> value),
    @required Result errorResendOtp(ErrorResendOtp<T> value),
    @required Result errorSendOtp(ErrorSendOtp<T> value),
    @required Result forgotPassLoading(ForgotPassLoading<T> value),
    @required Result forgotPassHashData(ForgotPassHashData<T> value),
    @required Result errorForgotPass(ErrorForgotPass<T> value),
    @required Result changePasswordHashData(ChangePasswordHashData<T> value),
    @required Result errorChangePassword(ErrorChangePassword<T> value),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return googleHashData(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result idle(Idle<T> value),
    Result loginLoading(LoginLoading<T> value),
    Result loginHashData(LoginHashData<T> value),
    Result errorLogin(ErrorLogin<T> value),
    Result googleLoading(GoogleLoading<T> value),
    Result googleHashData(GoogleHashData<T> value),
    Result errorGoogle(ErrorGoogle<T> value),
    Result facebookLoading(FacebookLoading<T> value),
    Result facebookHashData(FacebookHashData<T> value),
    Result errorFacebook(ErrorFacebook<T> value),
    Result registerLoading(RegisterLoading<T> value),
    Result registerHashData(RegisterHashData<T> value),
    Result errorRegist(ErrorRegist<T> value),
    Result otpLoading(OtpLoading<T> value),
    Result otpHashData(OtpHashData<T> value),
    Result resendOtpHashData(ResendOtpHashData<T> value),
    Result sendOtpHashData(SendOtpHashData<T> value),
    Result errorOtp(ErrorOtp<T> value),
    Result errorResendOtp(ErrorResendOtp<T> value),
    Result errorSendOtp(ErrorSendOtp<T> value),
    Result forgotPassLoading(ForgotPassLoading<T> value),
    Result forgotPassHashData(ForgotPassHashData<T> value),
    Result errorForgotPass(ErrorForgotPass<T> value),
    Result changePasswordHashData(ChangePasswordHashData<T> value),
    Result errorChangePassword(ErrorChangePassword<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (googleHashData != null) {
      return googleHashData(this);
    }
    return orElse();
  }
}

abstract class GoogleHashData<T> implements AuthenticationState<T> {
  const factory GoogleHashData({@required T data}) = _$GoogleHashData<T>;

  T get data;
  $GoogleHashDataCopyWith<T, GoogleHashData<T>> get copyWith;
}

abstract class $ErrorGoogleCopyWith<T, $Res> {
  factory $ErrorGoogleCopyWith(
          ErrorGoogle<T> value, $Res Function(ErrorGoogle<T>) then) =
      _$ErrorGoogleCopyWithImpl<T, $Res>;
  $Res call({NetworkExceptions error});

  $NetworkExceptionsCopyWith<$Res> get error;
}

class _$ErrorGoogleCopyWithImpl<T, $Res>
    extends _$AuthenticationStateCopyWithImpl<T, $Res>
    implements $ErrorGoogleCopyWith<T, $Res> {
  _$ErrorGoogleCopyWithImpl(
      ErrorGoogle<T> _value, $Res Function(ErrorGoogle<T>) _then)
      : super(_value, (v) => _then(v as ErrorGoogle<T>));

  @override
  ErrorGoogle<T> get _value => super._value as ErrorGoogle<T>;

  @override
  $Res call({
    Object error = freezed,
  }) {
    return _then(ErrorGoogle<T>(
      error: error == freezed ? _value.error : error as NetworkExceptions,
    ));
  }

  @override
  $NetworkExceptionsCopyWith<$Res> get error {
    if (_value.error == null) {
      return null;
    }
    return $NetworkExceptionsCopyWith<$Res>(_value.error, (value) {
      return _then(_value.copyWith(error: value));
    });
  }
}

class _$ErrorGoogle<T> implements ErrorGoogle<T> {
  const _$ErrorGoogle({@required this.error}) : assert(error != null);

  @override
  final NetworkExceptions error;

  @override
  String toString() {
    return 'AuthenticationState<$T>.errorGoogle(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ErrorGoogle<T> &&
            (identical(other.error, error) ||
                const DeepCollectionEquality().equals(other.error, error)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(error);

  @override
  $ErrorGoogleCopyWith<T, ErrorGoogle<T>> get copyWith =>
      _$ErrorGoogleCopyWithImpl<T, ErrorGoogle<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result idle(),
    @required Result loginLoading(),
    @required Result loginHashData(T data),
    @required Result errorLogin(NetworkExceptions error),
    @required Result googleLoading(),
    @required Result googleHashData(T data),
    @required Result errorGoogle(NetworkExceptions error),
    @required Result facebookLoading(),
    @required Result facebookHashData(T data),
    @required Result errorFacebook(NetworkExceptions error),
    @required Result registerLoading(),
    @required Result registerHashData(T data),
    @required Result errorRegist(NetworkExceptions error),
    @required Result otpLoading(),
    @required Result otpHashData(T data),
    @required Result resendOtpHashData(T data),
    @required Result sendOtpHashData(T data),
    @required Result errorOtp(NetworkExceptions error),
    @required Result errorResendOtp(NetworkExceptions error),
    @required Result errorSendOtp(NetworkExceptions error),
    @required Result forgotPassLoading(),
    @required Result forgotPassHashData(T data),
    @required Result errorForgotPass(NetworkExceptions error),
    @required Result changePasswordHashData(T data),
    @required Result errorChangePassword(NetworkExceptions error),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return errorGoogle(error);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result idle(),
    Result loginLoading(),
    Result loginHashData(T data),
    Result errorLogin(NetworkExceptions error),
    Result googleLoading(),
    Result googleHashData(T data),
    Result errorGoogle(NetworkExceptions error),
    Result facebookLoading(),
    Result facebookHashData(T data),
    Result errorFacebook(NetworkExceptions error),
    Result registerLoading(),
    Result registerHashData(T data),
    Result errorRegist(NetworkExceptions error),
    Result otpLoading(),
    Result otpHashData(T data),
    Result resendOtpHashData(T data),
    Result sendOtpHashData(T data),
    Result errorOtp(NetworkExceptions error),
    Result errorResendOtp(NetworkExceptions error),
    Result errorSendOtp(NetworkExceptions error),
    Result forgotPassLoading(),
    Result forgotPassHashData(T data),
    Result errorForgotPass(NetworkExceptions error),
    Result changePasswordHashData(T data),
    Result errorChangePassword(NetworkExceptions error),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (errorGoogle != null) {
      return errorGoogle(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result idle(Idle<T> value),
    @required Result loginLoading(LoginLoading<T> value),
    @required Result loginHashData(LoginHashData<T> value),
    @required Result errorLogin(ErrorLogin<T> value),
    @required Result googleLoading(GoogleLoading<T> value),
    @required Result googleHashData(GoogleHashData<T> value),
    @required Result errorGoogle(ErrorGoogle<T> value),
    @required Result facebookLoading(FacebookLoading<T> value),
    @required Result facebookHashData(FacebookHashData<T> value),
    @required Result errorFacebook(ErrorFacebook<T> value),
    @required Result registerLoading(RegisterLoading<T> value),
    @required Result registerHashData(RegisterHashData<T> value),
    @required Result errorRegist(ErrorRegist<T> value),
    @required Result otpLoading(OtpLoading<T> value),
    @required Result otpHashData(OtpHashData<T> value),
    @required Result resendOtpHashData(ResendOtpHashData<T> value),
    @required Result sendOtpHashData(SendOtpHashData<T> value),
    @required Result errorOtp(ErrorOtp<T> value),
    @required Result errorResendOtp(ErrorResendOtp<T> value),
    @required Result errorSendOtp(ErrorSendOtp<T> value),
    @required Result forgotPassLoading(ForgotPassLoading<T> value),
    @required Result forgotPassHashData(ForgotPassHashData<T> value),
    @required Result errorForgotPass(ErrorForgotPass<T> value),
    @required Result changePasswordHashData(ChangePasswordHashData<T> value),
    @required Result errorChangePassword(ErrorChangePassword<T> value),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return errorGoogle(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result idle(Idle<T> value),
    Result loginLoading(LoginLoading<T> value),
    Result loginHashData(LoginHashData<T> value),
    Result errorLogin(ErrorLogin<T> value),
    Result googleLoading(GoogleLoading<T> value),
    Result googleHashData(GoogleHashData<T> value),
    Result errorGoogle(ErrorGoogle<T> value),
    Result facebookLoading(FacebookLoading<T> value),
    Result facebookHashData(FacebookHashData<T> value),
    Result errorFacebook(ErrorFacebook<T> value),
    Result registerLoading(RegisterLoading<T> value),
    Result registerHashData(RegisterHashData<T> value),
    Result errorRegist(ErrorRegist<T> value),
    Result otpLoading(OtpLoading<T> value),
    Result otpHashData(OtpHashData<T> value),
    Result resendOtpHashData(ResendOtpHashData<T> value),
    Result sendOtpHashData(SendOtpHashData<T> value),
    Result errorOtp(ErrorOtp<T> value),
    Result errorResendOtp(ErrorResendOtp<T> value),
    Result errorSendOtp(ErrorSendOtp<T> value),
    Result forgotPassLoading(ForgotPassLoading<T> value),
    Result forgotPassHashData(ForgotPassHashData<T> value),
    Result errorForgotPass(ErrorForgotPass<T> value),
    Result changePasswordHashData(ChangePasswordHashData<T> value),
    Result errorChangePassword(ErrorChangePassword<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (errorGoogle != null) {
      return errorGoogle(this);
    }
    return orElse();
  }
}

abstract class ErrorGoogle<T> implements AuthenticationState<T> {
  const factory ErrorGoogle({@required NetworkExceptions error}) =
      _$ErrorGoogle<T>;

  NetworkExceptions get error;
  $ErrorGoogleCopyWith<T, ErrorGoogle<T>> get copyWith;
}

abstract class $FacebookLoadingCopyWith<T, $Res> {
  factory $FacebookLoadingCopyWith(
          FacebookLoading<T> value, $Res Function(FacebookLoading<T>) then) =
      _$FacebookLoadingCopyWithImpl<T, $Res>;
}

class _$FacebookLoadingCopyWithImpl<T, $Res>
    extends _$AuthenticationStateCopyWithImpl<T, $Res>
    implements $FacebookLoadingCopyWith<T, $Res> {
  _$FacebookLoadingCopyWithImpl(
      FacebookLoading<T> _value, $Res Function(FacebookLoading<T>) _then)
      : super(_value, (v) => _then(v as FacebookLoading<T>));

  @override
  FacebookLoading<T> get _value => super._value as FacebookLoading<T>;
}

class _$FacebookLoading<T> implements FacebookLoading<T> {
  const _$FacebookLoading();

  @override
  String toString() {
    return 'AuthenticationState<$T>.facebookLoading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is FacebookLoading<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result idle(),
    @required Result loginLoading(),
    @required Result loginHashData(T data),
    @required Result errorLogin(NetworkExceptions error),
    @required Result googleLoading(),
    @required Result googleHashData(T data),
    @required Result errorGoogle(NetworkExceptions error),
    @required Result facebookLoading(),
    @required Result facebookHashData(T data),
    @required Result errorFacebook(NetworkExceptions error),
    @required Result registerLoading(),
    @required Result registerHashData(T data),
    @required Result errorRegist(NetworkExceptions error),
    @required Result otpLoading(),
    @required Result otpHashData(T data),
    @required Result resendOtpHashData(T data),
    @required Result sendOtpHashData(T data),
    @required Result errorOtp(NetworkExceptions error),
    @required Result errorResendOtp(NetworkExceptions error),
    @required Result errorSendOtp(NetworkExceptions error),
    @required Result forgotPassLoading(),
    @required Result forgotPassHashData(T data),
    @required Result errorForgotPass(NetworkExceptions error),
    @required Result changePasswordHashData(T data),
    @required Result errorChangePassword(NetworkExceptions error),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return facebookLoading();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result idle(),
    Result loginLoading(),
    Result loginHashData(T data),
    Result errorLogin(NetworkExceptions error),
    Result googleLoading(),
    Result googleHashData(T data),
    Result errorGoogle(NetworkExceptions error),
    Result facebookLoading(),
    Result facebookHashData(T data),
    Result errorFacebook(NetworkExceptions error),
    Result registerLoading(),
    Result registerHashData(T data),
    Result errorRegist(NetworkExceptions error),
    Result otpLoading(),
    Result otpHashData(T data),
    Result resendOtpHashData(T data),
    Result sendOtpHashData(T data),
    Result errorOtp(NetworkExceptions error),
    Result errorResendOtp(NetworkExceptions error),
    Result errorSendOtp(NetworkExceptions error),
    Result forgotPassLoading(),
    Result forgotPassHashData(T data),
    Result errorForgotPass(NetworkExceptions error),
    Result changePasswordHashData(T data),
    Result errorChangePassword(NetworkExceptions error),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (facebookLoading != null) {
      return facebookLoading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result idle(Idle<T> value),
    @required Result loginLoading(LoginLoading<T> value),
    @required Result loginHashData(LoginHashData<T> value),
    @required Result errorLogin(ErrorLogin<T> value),
    @required Result googleLoading(GoogleLoading<T> value),
    @required Result googleHashData(GoogleHashData<T> value),
    @required Result errorGoogle(ErrorGoogle<T> value),
    @required Result facebookLoading(FacebookLoading<T> value),
    @required Result facebookHashData(FacebookHashData<T> value),
    @required Result errorFacebook(ErrorFacebook<T> value),
    @required Result registerLoading(RegisterLoading<T> value),
    @required Result registerHashData(RegisterHashData<T> value),
    @required Result errorRegist(ErrorRegist<T> value),
    @required Result otpLoading(OtpLoading<T> value),
    @required Result otpHashData(OtpHashData<T> value),
    @required Result resendOtpHashData(ResendOtpHashData<T> value),
    @required Result sendOtpHashData(SendOtpHashData<T> value),
    @required Result errorOtp(ErrorOtp<T> value),
    @required Result errorResendOtp(ErrorResendOtp<T> value),
    @required Result errorSendOtp(ErrorSendOtp<T> value),
    @required Result forgotPassLoading(ForgotPassLoading<T> value),
    @required Result forgotPassHashData(ForgotPassHashData<T> value),
    @required Result errorForgotPass(ErrorForgotPass<T> value),
    @required Result changePasswordHashData(ChangePasswordHashData<T> value),
    @required Result errorChangePassword(ErrorChangePassword<T> value),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return facebookLoading(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result idle(Idle<T> value),
    Result loginLoading(LoginLoading<T> value),
    Result loginHashData(LoginHashData<T> value),
    Result errorLogin(ErrorLogin<T> value),
    Result googleLoading(GoogleLoading<T> value),
    Result googleHashData(GoogleHashData<T> value),
    Result errorGoogle(ErrorGoogle<T> value),
    Result facebookLoading(FacebookLoading<T> value),
    Result facebookHashData(FacebookHashData<T> value),
    Result errorFacebook(ErrorFacebook<T> value),
    Result registerLoading(RegisterLoading<T> value),
    Result registerHashData(RegisterHashData<T> value),
    Result errorRegist(ErrorRegist<T> value),
    Result otpLoading(OtpLoading<T> value),
    Result otpHashData(OtpHashData<T> value),
    Result resendOtpHashData(ResendOtpHashData<T> value),
    Result sendOtpHashData(SendOtpHashData<T> value),
    Result errorOtp(ErrorOtp<T> value),
    Result errorResendOtp(ErrorResendOtp<T> value),
    Result errorSendOtp(ErrorSendOtp<T> value),
    Result forgotPassLoading(ForgotPassLoading<T> value),
    Result forgotPassHashData(ForgotPassHashData<T> value),
    Result errorForgotPass(ErrorForgotPass<T> value),
    Result changePasswordHashData(ChangePasswordHashData<T> value),
    Result errorChangePassword(ErrorChangePassword<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (facebookLoading != null) {
      return facebookLoading(this);
    }
    return orElse();
  }
}

abstract class FacebookLoading<T> implements AuthenticationState<T> {
  const factory FacebookLoading() = _$FacebookLoading<T>;
}

abstract class $FacebookHashDataCopyWith<T, $Res> {
  factory $FacebookHashDataCopyWith(
          FacebookHashData<T> value, $Res Function(FacebookHashData<T>) then) =
      _$FacebookHashDataCopyWithImpl<T, $Res>;
  $Res call({T data});
}

class _$FacebookHashDataCopyWithImpl<T, $Res>
    extends _$AuthenticationStateCopyWithImpl<T, $Res>
    implements $FacebookHashDataCopyWith<T, $Res> {
  _$FacebookHashDataCopyWithImpl(
      FacebookHashData<T> _value, $Res Function(FacebookHashData<T>) _then)
      : super(_value, (v) => _then(v as FacebookHashData<T>));

  @override
  FacebookHashData<T> get _value => super._value as FacebookHashData<T>;

  @override
  $Res call({
    Object data = freezed,
  }) {
    return _then(FacebookHashData<T>(
      data: data == freezed ? _value.data : data as T,
    ));
  }
}

class _$FacebookHashData<T> implements FacebookHashData<T> {
  const _$FacebookHashData({@required this.data}) : assert(data != null);

  @override
  final T data;

  @override
  String toString() {
    return 'AuthenticationState<$T>.facebookHashData(data: $data)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is FacebookHashData<T> &&
            (identical(other.data, data) ||
                const DeepCollectionEquality().equals(other.data, data)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(data);

  @override
  $FacebookHashDataCopyWith<T, FacebookHashData<T>> get copyWith =>
      _$FacebookHashDataCopyWithImpl<T, FacebookHashData<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result idle(),
    @required Result loginLoading(),
    @required Result loginHashData(T data),
    @required Result errorLogin(NetworkExceptions error),
    @required Result googleLoading(),
    @required Result googleHashData(T data),
    @required Result errorGoogle(NetworkExceptions error),
    @required Result facebookLoading(),
    @required Result facebookHashData(T data),
    @required Result errorFacebook(NetworkExceptions error),
    @required Result registerLoading(),
    @required Result registerHashData(T data),
    @required Result errorRegist(NetworkExceptions error),
    @required Result otpLoading(),
    @required Result otpHashData(T data),
    @required Result resendOtpHashData(T data),
    @required Result sendOtpHashData(T data),
    @required Result errorOtp(NetworkExceptions error),
    @required Result errorResendOtp(NetworkExceptions error),
    @required Result errorSendOtp(NetworkExceptions error),
    @required Result forgotPassLoading(),
    @required Result forgotPassHashData(T data),
    @required Result errorForgotPass(NetworkExceptions error),
    @required Result changePasswordHashData(T data),
    @required Result errorChangePassword(NetworkExceptions error),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return facebookHashData(data);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result idle(),
    Result loginLoading(),
    Result loginHashData(T data),
    Result errorLogin(NetworkExceptions error),
    Result googleLoading(),
    Result googleHashData(T data),
    Result errorGoogle(NetworkExceptions error),
    Result facebookLoading(),
    Result facebookHashData(T data),
    Result errorFacebook(NetworkExceptions error),
    Result registerLoading(),
    Result registerHashData(T data),
    Result errorRegist(NetworkExceptions error),
    Result otpLoading(),
    Result otpHashData(T data),
    Result resendOtpHashData(T data),
    Result sendOtpHashData(T data),
    Result errorOtp(NetworkExceptions error),
    Result errorResendOtp(NetworkExceptions error),
    Result errorSendOtp(NetworkExceptions error),
    Result forgotPassLoading(),
    Result forgotPassHashData(T data),
    Result errorForgotPass(NetworkExceptions error),
    Result changePasswordHashData(T data),
    Result errorChangePassword(NetworkExceptions error),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (facebookHashData != null) {
      return facebookHashData(data);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result idle(Idle<T> value),
    @required Result loginLoading(LoginLoading<T> value),
    @required Result loginHashData(LoginHashData<T> value),
    @required Result errorLogin(ErrorLogin<T> value),
    @required Result googleLoading(GoogleLoading<T> value),
    @required Result googleHashData(GoogleHashData<T> value),
    @required Result errorGoogle(ErrorGoogle<T> value),
    @required Result facebookLoading(FacebookLoading<T> value),
    @required Result facebookHashData(FacebookHashData<T> value),
    @required Result errorFacebook(ErrorFacebook<T> value),
    @required Result registerLoading(RegisterLoading<T> value),
    @required Result registerHashData(RegisterHashData<T> value),
    @required Result errorRegist(ErrorRegist<T> value),
    @required Result otpLoading(OtpLoading<T> value),
    @required Result otpHashData(OtpHashData<T> value),
    @required Result resendOtpHashData(ResendOtpHashData<T> value),
    @required Result sendOtpHashData(SendOtpHashData<T> value),
    @required Result errorOtp(ErrorOtp<T> value),
    @required Result errorResendOtp(ErrorResendOtp<T> value),
    @required Result errorSendOtp(ErrorSendOtp<T> value),
    @required Result forgotPassLoading(ForgotPassLoading<T> value),
    @required Result forgotPassHashData(ForgotPassHashData<T> value),
    @required Result errorForgotPass(ErrorForgotPass<T> value),
    @required Result changePasswordHashData(ChangePasswordHashData<T> value),
    @required Result errorChangePassword(ErrorChangePassword<T> value),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return facebookHashData(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result idle(Idle<T> value),
    Result loginLoading(LoginLoading<T> value),
    Result loginHashData(LoginHashData<T> value),
    Result errorLogin(ErrorLogin<T> value),
    Result googleLoading(GoogleLoading<T> value),
    Result googleHashData(GoogleHashData<T> value),
    Result errorGoogle(ErrorGoogle<T> value),
    Result facebookLoading(FacebookLoading<T> value),
    Result facebookHashData(FacebookHashData<T> value),
    Result errorFacebook(ErrorFacebook<T> value),
    Result registerLoading(RegisterLoading<T> value),
    Result registerHashData(RegisterHashData<T> value),
    Result errorRegist(ErrorRegist<T> value),
    Result otpLoading(OtpLoading<T> value),
    Result otpHashData(OtpHashData<T> value),
    Result resendOtpHashData(ResendOtpHashData<T> value),
    Result sendOtpHashData(SendOtpHashData<T> value),
    Result errorOtp(ErrorOtp<T> value),
    Result errorResendOtp(ErrorResendOtp<T> value),
    Result errorSendOtp(ErrorSendOtp<T> value),
    Result forgotPassLoading(ForgotPassLoading<T> value),
    Result forgotPassHashData(ForgotPassHashData<T> value),
    Result errorForgotPass(ErrorForgotPass<T> value),
    Result changePasswordHashData(ChangePasswordHashData<T> value),
    Result errorChangePassword(ErrorChangePassword<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (facebookHashData != null) {
      return facebookHashData(this);
    }
    return orElse();
  }
}

abstract class FacebookHashData<T> implements AuthenticationState<T> {
  const factory FacebookHashData({@required T data}) = _$FacebookHashData<T>;

  T get data;
  $FacebookHashDataCopyWith<T, FacebookHashData<T>> get copyWith;
}

abstract class $ErrorFacebookCopyWith<T, $Res> {
  factory $ErrorFacebookCopyWith(
          ErrorFacebook<T> value, $Res Function(ErrorFacebook<T>) then) =
      _$ErrorFacebookCopyWithImpl<T, $Res>;
  $Res call({NetworkExceptions error});

  $NetworkExceptionsCopyWith<$Res> get error;
}

class _$ErrorFacebookCopyWithImpl<T, $Res>
    extends _$AuthenticationStateCopyWithImpl<T, $Res>
    implements $ErrorFacebookCopyWith<T, $Res> {
  _$ErrorFacebookCopyWithImpl(
      ErrorFacebook<T> _value, $Res Function(ErrorFacebook<T>) _then)
      : super(_value, (v) => _then(v as ErrorFacebook<T>));

  @override
  ErrorFacebook<T> get _value => super._value as ErrorFacebook<T>;

  @override
  $Res call({
    Object error = freezed,
  }) {
    return _then(ErrorFacebook<T>(
      error: error == freezed ? _value.error : error as NetworkExceptions,
    ));
  }

  @override
  $NetworkExceptionsCopyWith<$Res> get error {
    if (_value.error == null) {
      return null;
    }
    return $NetworkExceptionsCopyWith<$Res>(_value.error, (value) {
      return _then(_value.copyWith(error: value));
    });
  }
}

class _$ErrorFacebook<T> implements ErrorFacebook<T> {
  const _$ErrorFacebook({@required this.error}) : assert(error != null);

  @override
  final NetworkExceptions error;

  @override
  String toString() {
    return 'AuthenticationState<$T>.errorFacebook(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ErrorFacebook<T> &&
            (identical(other.error, error) ||
                const DeepCollectionEquality().equals(other.error, error)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(error);

  @override
  $ErrorFacebookCopyWith<T, ErrorFacebook<T>> get copyWith =>
      _$ErrorFacebookCopyWithImpl<T, ErrorFacebook<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result idle(),
    @required Result loginLoading(),
    @required Result loginHashData(T data),
    @required Result errorLogin(NetworkExceptions error),
    @required Result googleLoading(),
    @required Result googleHashData(T data),
    @required Result errorGoogle(NetworkExceptions error),
    @required Result facebookLoading(),
    @required Result facebookHashData(T data),
    @required Result errorFacebook(NetworkExceptions error),
    @required Result registerLoading(),
    @required Result registerHashData(T data),
    @required Result errorRegist(NetworkExceptions error),
    @required Result otpLoading(),
    @required Result otpHashData(T data),
    @required Result resendOtpHashData(T data),
    @required Result sendOtpHashData(T data),
    @required Result errorOtp(NetworkExceptions error),
    @required Result errorResendOtp(NetworkExceptions error),
    @required Result errorSendOtp(NetworkExceptions error),
    @required Result forgotPassLoading(),
    @required Result forgotPassHashData(T data),
    @required Result errorForgotPass(NetworkExceptions error),
    @required Result changePasswordHashData(T data),
    @required Result errorChangePassword(NetworkExceptions error),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return errorFacebook(error);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result idle(),
    Result loginLoading(),
    Result loginHashData(T data),
    Result errorLogin(NetworkExceptions error),
    Result googleLoading(),
    Result googleHashData(T data),
    Result errorGoogle(NetworkExceptions error),
    Result facebookLoading(),
    Result facebookHashData(T data),
    Result errorFacebook(NetworkExceptions error),
    Result registerLoading(),
    Result registerHashData(T data),
    Result errorRegist(NetworkExceptions error),
    Result otpLoading(),
    Result otpHashData(T data),
    Result resendOtpHashData(T data),
    Result sendOtpHashData(T data),
    Result errorOtp(NetworkExceptions error),
    Result errorResendOtp(NetworkExceptions error),
    Result errorSendOtp(NetworkExceptions error),
    Result forgotPassLoading(),
    Result forgotPassHashData(T data),
    Result errorForgotPass(NetworkExceptions error),
    Result changePasswordHashData(T data),
    Result errorChangePassword(NetworkExceptions error),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (errorFacebook != null) {
      return errorFacebook(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result idle(Idle<T> value),
    @required Result loginLoading(LoginLoading<T> value),
    @required Result loginHashData(LoginHashData<T> value),
    @required Result errorLogin(ErrorLogin<T> value),
    @required Result googleLoading(GoogleLoading<T> value),
    @required Result googleHashData(GoogleHashData<T> value),
    @required Result errorGoogle(ErrorGoogle<T> value),
    @required Result facebookLoading(FacebookLoading<T> value),
    @required Result facebookHashData(FacebookHashData<T> value),
    @required Result errorFacebook(ErrorFacebook<T> value),
    @required Result registerLoading(RegisterLoading<T> value),
    @required Result registerHashData(RegisterHashData<T> value),
    @required Result errorRegist(ErrorRegist<T> value),
    @required Result otpLoading(OtpLoading<T> value),
    @required Result otpHashData(OtpHashData<T> value),
    @required Result resendOtpHashData(ResendOtpHashData<T> value),
    @required Result sendOtpHashData(SendOtpHashData<T> value),
    @required Result errorOtp(ErrorOtp<T> value),
    @required Result errorResendOtp(ErrorResendOtp<T> value),
    @required Result errorSendOtp(ErrorSendOtp<T> value),
    @required Result forgotPassLoading(ForgotPassLoading<T> value),
    @required Result forgotPassHashData(ForgotPassHashData<T> value),
    @required Result errorForgotPass(ErrorForgotPass<T> value),
    @required Result changePasswordHashData(ChangePasswordHashData<T> value),
    @required Result errorChangePassword(ErrorChangePassword<T> value),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return errorFacebook(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result idle(Idle<T> value),
    Result loginLoading(LoginLoading<T> value),
    Result loginHashData(LoginHashData<T> value),
    Result errorLogin(ErrorLogin<T> value),
    Result googleLoading(GoogleLoading<T> value),
    Result googleHashData(GoogleHashData<T> value),
    Result errorGoogle(ErrorGoogle<T> value),
    Result facebookLoading(FacebookLoading<T> value),
    Result facebookHashData(FacebookHashData<T> value),
    Result errorFacebook(ErrorFacebook<T> value),
    Result registerLoading(RegisterLoading<T> value),
    Result registerHashData(RegisterHashData<T> value),
    Result errorRegist(ErrorRegist<T> value),
    Result otpLoading(OtpLoading<T> value),
    Result otpHashData(OtpHashData<T> value),
    Result resendOtpHashData(ResendOtpHashData<T> value),
    Result sendOtpHashData(SendOtpHashData<T> value),
    Result errorOtp(ErrorOtp<T> value),
    Result errorResendOtp(ErrorResendOtp<T> value),
    Result errorSendOtp(ErrorSendOtp<T> value),
    Result forgotPassLoading(ForgotPassLoading<T> value),
    Result forgotPassHashData(ForgotPassHashData<T> value),
    Result errorForgotPass(ErrorForgotPass<T> value),
    Result changePasswordHashData(ChangePasswordHashData<T> value),
    Result errorChangePassword(ErrorChangePassword<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (errorFacebook != null) {
      return errorFacebook(this);
    }
    return orElse();
  }
}

abstract class ErrorFacebook<T> implements AuthenticationState<T> {
  const factory ErrorFacebook({@required NetworkExceptions error}) =
      _$ErrorFacebook<T>;

  NetworkExceptions get error;
  $ErrorFacebookCopyWith<T, ErrorFacebook<T>> get copyWith;
}

abstract class $RegisterLoadingCopyWith<T, $Res> {
  factory $RegisterLoadingCopyWith(
          RegisterLoading<T> value, $Res Function(RegisterLoading<T>) then) =
      _$RegisterLoadingCopyWithImpl<T, $Res>;
}

class _$RegisterLoadingCopyWithImpl<T, $Res>
    extends _$AuthenticationStateCopyWithImpl<T, $Res>
    implements $RegisterLoadingCopyWith<T, $Res> {
  _$RegisterLoadingCopyWithImpl(
      RegisterLoading<T> _value, $Res Function(RegisterLoading<T>) _then)
      : super(_value, (v) => _then(v as RegisterLoading<T>));

  @override
  RegisterLoading<T> get _value => super._value as RegisterLoading<T>;
}

class _$RegisterLoading<T> implements RegisterLoading<T> {
  const _$RegisterLoading();

  @override
  String toString() {
    return 'AuthenticationState<$T>.registerLoading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is RegisterLoading<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result idle(),
    @required Result loginLoading(),
    @required Result loginHashData(T data),
    @required Result errorLogin(NetworkExceptions error),
    @required Result googleLoading(),
    @required Result googleHashData(T data),
    @required Result errorGoogle(NetworkExceptions error),
    @required Result facebookLoading(),
    @required Result facebookHashData(T data),
    @required Result errorFacebook(NetworkExceptions error),
    @required Result registerLoading(),
    @required Result registerHashData(T data),
    @required Result errorRegist(NetworkExceptions error),
    @required Result otpLoading(),
    @required Result otpHashData(T data),
    @required Result resendOtpHashData(T data),
    @required Result sendOtpHashData(T data),
    @required Result errorOtp(NetworkExceptions error),
    @required Result errorResendOtp(NetworkExceptions error),
    @required Result errorSendOtp(NetworkExceptions error),
    @required Result forgotPassLoading(),
    @required Result forgotPassHashData(T data),
    @required Result errorForgotPass(NetworkExceptions error),
    @required Result changePasswordHashData(T data),
    @required Result errorChangePassword(NetworkExceptions error),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return registerLoading();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result idle(),
    Result loginLoading(),
    Result loginHashData(T data),
    Result errorLogin(NetworkExceptions error),
    Result googleLoading(),
    Result googleHashData(T data),
    Result errorGoogle(NetworkExceptions error),
    Result facebookLoading(),
    Result facebookHashData(T data),
    Result errorFacebook(NetworkExceptions error),
    Result registerLoading(),
    Result registerHashData(T data),
    Result errorRegist(NetworkExceptions error),
    Result otpLoading(),
    Result otpHashData(T data),
    Result resendOtpHashData(T data),
    Result sendOtpHashData(T data),
    Result errorOtp(NetworkExceptions error),
    Result errorResendOtp(NetworkExceptions error),
    Result errorSendOtp(NetworkExceptions error),
    Result forgotPassLoading(),
    Result forgotPassHashData(T data),
    Result errorForgotPass(NetworkExceptions error),
    Result changePasswordHashData(T data),
    Result errorChangePassword(NetworkExceptions error),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (registerLoading != null) {
      return registerLoading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result idle(Idle<T> value),
    @required Result loginLoading(LoginLoading<T> value),
    @required Result loginHashData(LoginHashData<T> value),
    @required Result errorLogin(ErrorLogin<T> value),
    @required Result googleLoading(GoogleLoading<T> value),
    @required Result googleHashData(GoogleHashData<T> value),
    @required Result errorGoogle(ErrorGoogle<T> value),
    @required Result facebookLoading(FacebookLoading<T> value),
    @required Result facebookHashData(FacebookHashData<T> value),
    @required Result errorFacebook(ErrorFacebook<T> value),
    @required Result registerLoading(RegisterLoading<T> value),
    @required Result registerHashData(RegisterHashData<T> value),
    @required Result errorRegist(ErrorRegist<T> value),
    @required Result otpLoading(OtpLoading<T> value),
    @required Result otpHashData(OtpHashData<T> value),
    @required Result resendOtpHashData(ResendOtpHashData<T> value),
    @required Result sendOtpHashData(SendOtpHashData<T> value),
    @required Result errorOtp(ErrorOtp<T> value),
    @required Result errorResendOtp(ErrorResendOtp<T> value),
    @required Result errorSendOtp(ErrorSendOtp<T> value),
    @required Result forgotPassLoading(ForgotPassLoading<T> value),
    @required Result forgotPassHashData(ForgotPassHashData<T> value),
    @required Result errorForgotPass(ErrorForgotPass<T> value),
    @required Result changePasswordHashData(ChangePasswordHashData<T> value),
    @required Result errorChangePassword(ErrorChangePassword<T> value),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return registerLoading(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result idle(Idle<T> value),
    Result loginLoading(LoginLoading<T> value),
    Result loginHashData(LoginHashData<T> value),
    Result errorLogin(ErrorLogin<T> value),
    Result googleLoading(GoogleLoading<T> value),
    Result googleHashData(GoogleHashData<T> value),
    Result errorGoogle(ErrorGoogle<T> value),
    Result facebookLoading(FacebookLoading<T> value),
    Result facebookHashData(FacebookHashData<T> value),
    Result errorFacebook(ErrorFacebook<T> value),
    Result registerLoading(RegisterLoading<T> value),
    Result registerHashData(RegisterHashData<T> value),
    Result errorRegist(ErrorRegist<T> value),
    Result otpLoading(OtpLoading<T> value),
    Result otpHashData(OtpHashData<T> value),
    Result resendOtpHashData(ResendOtpHashData<T> value),
    Result sendOtpHashData(SendOtpHashData<T> value),
    Result errorOtp(ErrorOtp<T> value),
    Result errorResendOtp(ErrorResendOtp<T> value),
    Result errorSendOtp(ErrorSendOtp<T> value),
    Result forgotPassLoading(ForgotPassLoading<T> value),
    Result forgotPassHashData(ForgotPassHashData<T> value),
    Result errorForgotPass(ErrorForgotPass<T> value),
    Result changePasswordHashData(ChangePasswordHashData<T> value),
    Result errorChangePassword(ErrorChangePassword<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (registerLoading != null) {
      return registerLoading(this);
    }
    return orElse();
  }
}

abstract class RegisterLoading<T> implements AuthenticationState<T> {
  const factory RegisterLoading() = _$RegisterLoading<T>;
}

abstract class $RegisterHashDataCopyWith<T, $Res> {
  factory $RegisterHashDataCopyWith(
          RegisterHashData<T> value, $Res Function(RegisterHashData<T>) then) =
      _$RegisterHashDataCopyWithImpl<T, $Res>;
  $Res call({T data});
}

class _$RegisterHashDataCopyWithImpl<T, $Res>
    extends _$AuthenticationStateCopyWithImpl<T, $Res>
    implements $RegisterHashDataCopyWith<T, $Res> {
  _$RegisterHashDataCopyWithImpl(
      RegisterHashData<T> _value, $Res Function(RegisterHashData<T>) _then)
      : super(_value, (v) => _then(v as RegisterHashData<T>));

  @override
  RegisterHashData<T> get _value => super._value as RegisterHashData<T>;

  @override
  $Res call({
    Object data = freezed,
  }) {
    return _then(RegisterHashData<T>(
      data: data == freezed ? _value.data : data as T,
    ));
  }
}

class _$RegisterHashData<T> implements RegisterHashData<T> {
  const _$RegisterHashData({@required this.data}) : assert(data != null);

  @override
  final T data;

  @override
  String toString() {
    return 'AuthenticationState<$T>.registerHashData(data: $data)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is RegisterHashData<T> &&
            (identical(other.data, data) ||
                const DeepCollectionEquality().equals(other.data, data)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(data);

  @override
  $RegisterHashDataCopyWith<T, RegisterHashData<T>> get copyWith =>
      _$RegisterHashDataCopyWithImpl<T, RegisterHashData<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result idle(),
    @required Result loginLoading(),
    @required Result loginHashData(T data),
    @required Result errorLogin(NetworkExceptions error),
    @required Result googleLoading(),
    @required Result googleHashData(T data),
    @required Result errorGoogle(NetworkExceptions error),
    @required Result facebookLoading(),
    @required Result facebookHashData(T data),
    @required Result errorFacebook(NetworkExceptions error),
    @required Result registerLoading(),
    @required Result registerHashData(T data),
    @required Result errorRegist(NetworkExceptions error),
    @required Result otpLoading(),
    @required Result otpHashData(T data),
    @required Result resendOtpHashData(T data),
    @required Result sendOtpHashData(T data),
    @required Result errorOtp(NetworkExceptions error),
    @required Result errorResendOtp(NetworkExceptions error),
    @required Result errorSendOtp(NetworkExceptions error),
    @required Result forgotPassLoading(),
    @required Result forgotPassHashData(T data),
    @required Result errorForgotPass(NetworkExceptions error),
    @required Result changePasswordHashData(T data),
    @required Result errorChangePassword(NetworkExceptions error),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return registerHashData(data);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result idle(),
    Result loginLoading(),
    Result loginHashData(T data),
    Result errorLogin(NetworkExceptions error),
    Result googleLoading(),
    Result googleHashData(T data),
    Result errorGoogle(NetworkExceptions error),
    Result facebookLoading(),
    Result facebookHashData(T data),
    Result errorFacebook(NetworkExceptions error),
    Result registerLoading(),
    Result registerHashData(T data),
    Result errorRegist(NetworkExceptions error),
    Result otpLoading(),
    Result otpHashData(T data),
    Result resendOtpHashData(T data),
    Result sendOtpHashData(T data),
    Result errorOtp(NetworkExceptions error),
    Result errorResendOtp(NetworkExceptions error),
    Result errorSendOtp(NetworkExceptions error),
    Result forgotPassLoading(),
    Result forgotPassHashData(T data),
    Result errorForgotPass(NetworkExceptions error),
    Result changePasswordHashData(T data),
    Result errorChangePassword(NetworkExceptions error),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (registerHashData != null) {
      return registerHashData(data);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result idle(Idle<T> value),
    @required Result loginLoading(LoginLoading<T> value),
    @required Result loginHashData(LoginHashData<T> value),
    @required Result errorLogin(ErrorLogin<T> value),
    @required Result googleLoading(GoogleLoading<T> value),
    @required Result googleHashData(GoogleHashData<T> value),
    @required Result errorGoogle(ErrorGoogle<T> value),
    @required Result facebookLoading(FacebookLoading<T> value),
    @required Result facebookHashData(FacebookHashData<T> value),
    @required Result errorFacebook(ErrorFacebook<T> value),
    @required Result registerLoading(RegisterLoading<T> value),
    @required Result registerHashData(RegisterHashData<T> value),
    @required Result errorRegist(ErrorRegist<T> value),
    @required Result otpLoading(OtpLoading<T> value),
    @required Result otpHashData(OtpHashData<T> value),
    @required Result resendOtpHashData(ResendOtpHashData<T> value),
    @required Result sendOtpHashData(SendOtpHashData<T> value),
    @required Result errorOtp(ErrorOtp<T> value),
    @required Result errorResendOtp(ErrorResendOtp<T> value),
    @required Result errorSendOtp(ErrorSendOtp<T> value),
    @required Result forgotPassLoading(ForgotPassLoading<T> value),
    @required Result forgotPassHashData(ForgotPassHashData<T> value),
    @required Result errorForgotPass(ErrorForgotPass<T> value),
    @required Result changePasswordHashData(ChangePasswordHashData<T> value),
    @required Result errorChangePassword(ErrorChangePassword<T> value),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return registerHashData(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result idle(Idle<T> value),
    Result loginLoading(LoginLoading<T> value),
    Result loginHashData(LoginHashData<T> value),
    Result errorLogin(ErrorLogin<T> value),
    Result googleLoading(GoogleLoading<T> value),
    Result googleHashData(GoogleHashData<T> value),
    Result errorGoogle(ErrorGoogle<T> value),
    Result facebookLoading(FacebookLoading<T> value),
    Result facebookHashData(FacebookHashData<T> value),
    Result errorFacebook(ErrorFacebook<T> value),
    Result registerLoading(RegisterLoading<T> value),
    Result registerHashData(RegisterHashData<T> value),
    Result errorRegist(ErrorRegist<T> value),
    Result otpLoading(OtpLoading<T> value),
    Result otpHashData(OtpHashData<T> value),
    Result resendOtpHashData(ResendOtpHashData<T> value),
    Result sendOtpHashData(SendOtpHashData<T> value),
    Result errorOtp(ErrorOtp<T> value),
    Result errorResendOtp(ErrorResendOtp<T> value),
    Result errorSendOtp(ErrorSendOtp<T> value),
    Result forgotPassLoading(ForgotPassLoading<T> value),
    Result forgotPassHashData(ForgotPassHashData<T> value),
    Result errorForgotPass(ErrorForgotPass<T> value),
    Result changePasswordHashData(ChangePasswordHashData<T> value),
    Result errorChangePassword(ErrorChangePassword<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (registerHashData != null) {
      return registerHashData(this);
    }
    return orElse();
  }
}

abstract class RegisterHashData<T> implements AuthenticationState<T> {
  const factory RegisterHashData({@required T data}) = _$RegisterHashData<T>;

  T get data;
  $RegisterHashDataCopyWith<T, RegisterHashData<T>> get copyWith;
}

abstract class $ErrorRegistCopyWith<T, $Res> {
  factory $ErrorRegistCopyWith(
          ErrorRegist<T> value, $Res Function(ErrorRegist<T>) then) =
      _$ErrorRegistCopyWithImpl<T, $Res>;
  $Res call({NetworkExceptions error});

  $NetworkExceptionsCopyWith<$Res> get error;
}

class _$ErrorRegistCopyWithImpl<T, $Res>
    extends _$AuthenticationStateCopyWithImpl<T, $Res>
    implements $ErrorRegistCopyWith<T, $Res> {
  _$ErrorRegistCopyWithImpl(
      ErrorRegist<T> _value, $Res Function(ErrorRegist<T>) _then)
      : super(_value, (v) => _then(v as ErrorRegist<T>));

  @override
  ErrorRegist<T> get _value => super._value as ErrorRegist<T>;

  @override
  $Res call({
    Object error = freezed,
  }) {
    return _then(ErrorRegist<T>(
      error: error == freezed ? _value.error : error as NetworkExceptions,
    ));
  }

  @override
  $NetworkExceptionsCopyWith<$Res> get error {
    if (_value.error == null) {
      return null;
    }
    return $NetworkExceptionsCopyWith<$Res>(_value.error, (value) {
      return _then(_value.copyWith(error: value));
    });
  }
}

class _$ErrorRegist<T> implements ErrorRegist<T> {
  const _$ErrorRegist({@required this.error}) : assert(error != null);

  @override
  final NetworkExceptions error;

  @override
  String toString() {
    return 'AuthenticationState<$T>.errorRegist(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ErrorRegist<T> &&
            (identical(other.error, error) ||
                const DeepCollectionEquality().equals(other.error, error)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(error);

  @override
  $ErrorRegistCopyWith<T, ErrorRegist<T>> get copyWith =>
      _$ErrorRegistCopyWithImpl<T, ErrorRegist<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result idle(),
    @required Result loginLoading(),
    @required Result loginHashData(T data),
    @required Result errorLogin(NetworkExceptions error),
    @required Result googleLoading(),
    @required Result googleHashData(T data),
    @required Result errorGoogle(NetworkExceptions error),
    @required Result facebookLoading(),
    @required Result facebookHashData(T data),
    @required Result errorFacebook(NetworkExceptions error),
    @required Result registerLoading(),
    @required Result registerHashData(T data),
    @required Result errorRegist(NetworkExceptions error),
    @required Result otpLoading(),
    @required Result otpHashData(T data),
    @required Result resendOtpHashData(T data),
    @required Result sendOtpHashData(T data),
    @required Result errorOtp(NetworkExceptions error),
    @required Result errorResendOtp(NetworkExceptions error),
    @required Result errorSendOtp(NetworkExceptions error),
    @required Result forgotPassLoading(),
    @required Result forgotPassHashData(T data),
    @required Result errorForgotPass(NetworkExceptions error),
    @required Result changePasswordHashData(T data),
    @required Result errorChangePassword(NetworkExceptions error),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return errorRegist(error);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result idle(),
    Result loginLoading(),
    Result loginHashData(T data),
    Result errorLogin(NetworkExceptions error),
    Result googleLoading(),
    Result googleHashData(T data),
    Result errorGoogle(NetworkExceptions error),
    Result facebookLoading(),
    Result facebookHashData(T data),
    Result errorFacebook(NetworkExceptions error),
    Result registerLoading(),
    Result registerHashData(T data),
    Result errorRegist(NetworkExceptions error),
    Result otpLoading(),
    Result otpHashData(T data),
    Result resendOtpHashData(T data),
    Result sendOtpHashData(T data),
    Result errorOtp(NetworkExceptions error),
    Result errorResendOtp(NetworkExceptions error),
    Result errorSendOtp(NetworkExceptions error),
    Result forgotPassLoading(),
    Result forgotPassHashData(T data),
    Result errorForgotPass(NetworkExceptions error),
    Result changePasswordHashData(T data),
    Result errorChangePassword(NetworkExceptions error),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (errorRegist != null) {
      return errorRegist(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result idle(Idle<T> value),
    @required Result loginLoading(LoginLoading<T> value),
    @required Result loginHashData(LoginHashData<T> value),
    @required Result errorLogin(ErrorLogin<T> value),
    @required Result googleLoading(GoogleLoading<T> value),
    @required Result googleHashData(GoogleHashData<T> value),
    @required Result errorGoogle(ErrorGoogle<T> value),
    @required Result facebookLoading(FacebookLoading<T> value),
    @required Result facebookHashData(FacebookHashData<T> value),
    @required Result errorFacebook(ErrorFacebook<T> value),
    @required Result registerLoading(RegisterLoading<T> value),
    @required Result registerHashData(RegisterHashData<T> value),
    @required Result errorRegist(ErrorRegist<T> value),
    @required Result otpLoading(OtpLoading<T> value),
    @required Result otpHashData(OtpHashData<T> value),
    @required Result resendOtpHashData(ResendOtpHashData<T> value),
    @required Result sendOtpHashData(SendOtpHashData<T> value),
    @required Result errorOtp(ErrorOtp<T> value),
    @required Result errorResendOtp(ErrorResendOtp<T> value),
    @required Result errorSendOtp(ErrorSendOtp<T> value),
    @required Result forgotPassLoading(ForgotPassLoading<T> value),
    @required Result forgotPassHashData(ForgotPassHashData<T> value),
    @required Result errorForgotPass(ErrorForgotPass<T> value),
    @required Result changePasswordHashData(ChangePasswordHashData<T> value),
    @required Result errorChangePassword(ErrorChangePassword<T> value),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return errorRegist(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result idle(Idle<T> value),
    Result loginLoading(LoginLoading<T> value),
    Result loginHashData(LoginHashData<T> value),
    Result errorLogin(ErrorLogin<T> value),
    Result googleLoading(GoogleLoading<T> value),
    Result googleHashData(GoogleHashData<T> value),
    Result errorGoogle(ErrorGoogle<T> value),
    Result facebookLoading(FacebookLoading<T> value),
    Result facebookHashData(FacebookHashData<T> value),
    Result errorFacebook(ErrorFacebook<T> value),
    Result registerLoading(RegisterLoading<T> value),
    Result registerHashData(RegisterHashData<T> value),
    Result errorRegist(ErrorRegist<T> value),
    Result otpLoading(OtpLoading<T> value),
    Result otpHashData(OtpHashData<T> value),
    Result resendOtpHashData(ResendOtpHashData<T> value),
    Result sendOtpHashData(SendOtpHashData<T> value),
    Result errorOtp(ErrorOtp<T> value),
    Result errorResendOtp(ErrorResendOtp<T> value),
    Result errorSendOtp(ErrorSendOtp<T> value),
    Result forgotPassLoading(ForgotPassLoading<T> value),
    Result forgotPassHashData(ForgotPassHashData<T> value),
    Result errorForgotPass(ErrorForgotPass<T> value),
    Result changePasswordHashData(ChangePasswordHashData<T> value),
    Result errorChangePassword(ErrorChangePassword<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (errorRegist != null) {
      return errorRegist(this);
    }
    return orElse();
  }
}

abstract class ErrorRegist<T> implements AuthenticationState<T> {
  const factory ErrorRegist({@required NetworkExceptions error}) =
      _$ErrorRegist<T>;

  NetworkExceptions get error;
  $ErrorRegistCopyWith<T, ErrorRegist<T>> get copyWith;
}

abstract class $OtpLoadingCopyWith<T, $Res> {
  factory $OtpLoadingCopyWith(
          OtpLoading<T> value, $Res Function(OtpLoading<T>) then) =
      _$OtpLoadingCopyWithImpl<T, $Res>;
}

class _$OtpLoadingCopyWithImpl<T, $Res>
    extends _$AuthenticationStateCopyWithImpl<T, $Res>
    implements $OtpLoadingCopyWith<T, $Res> {
  _$OtpLoadingCopyWithImpl(
      OtpLoading<T> _value, $Res Function(OtpLoading<T>) _then)
      : super(_value, (v) => _then(v as OtpLoading<T>));

  @override
  OtpLoading<T> get _value => super._value as OtpLoading<T>;
}

class _$OtpLoading<T> implements OtpLoading<T> {
  const _$OtpLoading();

  @override
  String toString() {
    return 'AuthenticationState<$T>.otpLoading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is OtpLoading<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result idle(),
    @required Result loginLoading(),
    @required Result loginHashData(T data),
    @required Result errorLogin(NetworkExceptions error),
    @required Result googleLoading(),
    @required Result googleHashData(T data),
    @required Result errorGoogle(NetworkExceptions error),
    @required Result facebookLoading(),
    @required Result facebookHashData(T data),
    @required Result errorFacebook(NetworkExceptions error),
    @required Result registerLoading(),
    @required Result registerHashData(T data),
    @required Result errorRegist(NetworkExceptions error),
    @required Result otpLoading(),
    @required Result otpHashData(T data),
    @required Result resendOtpHashData(T data),
    @required Result sendOtpHashData(T data),
    @required Result errorOtp(NetworkExceptions error),
    @required Result errorResendOtp(NetworkExceptions error),
    @required Result errorSendOtp(NetworkExceptions error),
    @required Result forgotPassLoading(),
    @required Result forgotPassHashData(T data),
    @required Result errorForgotPass(NetworkExceptions error),
    @required Result changePasswordHashData(T data),
    @required Result errorChangePassword(NetworkExceptions error),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return otpLoading();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result idle(),
    Result loginLoading(),
    Result loginHashData(T data),
    Result errorLogin(NetworkExceptions error),
    Result googleLoading(),
    Result googleHashData(T data),
    Result errorGoogle(NetworkExceptions error),
    Result facebookLoading(),
    Result facebookHashData(T data),
    Result errorFacebook(NetworkExceptions error),
    Result registerLoading(),
    Result registerHashData(T data),
    Result errorRegist(NetworkExceptions error),
    Result otpLoading(),
    Result otpHashData(T data),
    Result resendOtpHashData(T data),
    Result sendOtpHashData(T data),
    Result errorOtp(NetworkExceptions error),
    Result errorResendOtp(NetworkExceptions error),
    Result errorSendOtp(NetworkExceptions error),
    Result forgotPassLoading(),
    Result forgotPassHashData(T data),
    Result errorForgotPass(NetworkExceptions error),
    Result changePasswordHashData(T data),
    Result errorChangePassword(NetworkExceptions error),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (otpLoading != null) {
      return otpLoading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result idle(Idle<T> value),
    @required Result loginLoading(LoginLoading<T> value),
    @required Result loginHashData(LoginHashData<T> value),
    @required Result errorLogin(ErrorLogin<T> value),
    @required Result googleLoading(GoogleLoading<T> value),
    @required Result googleHashData(GoogleHashData<T> value),
    @required Result errorGoogle(ErrorGoogle<T> value),
    @required Result facebookLoading(FacebookLoading<T> value),
    @required Result facebookHashData(FacebookHashData<T> value),
    @required Result errorFacebook(ErrorFacebook<T> value),
    @required Result registerLoading(RegisterLoading<T> value),
    @required Result registerHashData(RegisterHashData<T> value),
    @required Result errorRegist(ErrorRegist<T> value),
    @required Result otpLoading(OtpLoading<T> value),
    @required Result otpHashData(OtpHashData<T> value),
    @required Result resendOtpHashData(ResendOtpHashData<T> value),
    @required Result sendOtpHashData(SendOtpHashData<T> value),
    @required Result errorOtp(ErrorOtp<T> value),
    @required Result errorResendOtp(ErrorResendOtp<T> value),
    @required Result errorSendOtp(ErrorSendOtp<T> value),
    @required Result forgotPassLoading(ForgotPassLoading<T> value),
    @required Result forgotPassHashData(ForgotPassHashData<T> value),
    @required Result errorForgotPass(ErrorForgotPass<T> value),
    @required Result changePasswordHashData(ChangePasswordHashData<T> value),
    @required Result errorChangePassword(ErrorChangePassword<T> value),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return otpLoading(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result idle(Idle<T> value),
    Result loginLoading(LoginLoading<T> value),
    Result loginHashData(LoginHashData<T> value),
    Result errorLogin(ErrorLogin<T> value),
    Result googleLoading(GoogleLoading<T> value),
    Result googleHashData(GoogleHashData<T> value),
    Result errorGoogle(ErrorGoogle<T> value),
    Result facebookLoading(FacebookLoading<T> value),
    Result facebookHashData(FacebookHashData<T> value),
    Result errorFacebook(ErrorFacebook<T> value),
    Result registerLoading(RegisterLoading<T> value),
    Result registerHashData(RegisterHashData<T> value),
    Result errorRegist(ErrorRegist<T> value),
    Result otpLoading(OtpLoading<T> value),
    Result otpHashData(OtpHashData<T> value),
    Result resendOtpHashData(ResendOtpHashData<T> value),
    Result sendOtpHashData(SendOtpHashData<T> value),
    Result errorOtp(ErrorOtp<T> value),
    Result errorResendOtp(ErrorResendOtp<T> value),
    Result errorSendOtp(ErrorSendOtp<T> value),
    Result forgotPassLoading(ForgotPassLoading<T> value),
    Result forgotPassHashData(ForgotPassHashData<T> value),
    Result errorForgotPass(ErrorForgotPass<T> value),
    Result changePasswordHashData(ChangePasswordHashData<T> value),
    Result errorChangePassword(ErrorChangePassword<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (otpLoading != null) {
      return otpLoading(this);
    }
    return orElse();
  }
}

abstract class OtpLoading<T> implements AuthenticationState<T> {
  const factory OtpLoading() = _$OtpLoading<T>;
}

abstract class $OtpHashDataCopyWith<T, $Res> {
  factory $OtpHashDataCopyWith(
          OtpHashData<T> value, $Res Function(OtpHashData<T>) then) =
      _$OtpHashDataCopyWithImpl<T, $Res>;
  $Res call({T data});
}

class _$OtpHashDataCopyWithImpl<T, $Res>
    extends _$AuthenticationStateCopyWithImpl<T, $Res>
    implements $OtpHashDataCopyWith<T, $Res> {
  _$OtpHashDataCopyWithImpl(
      OtpHashData<T> _value, $Res Function(OtpHashData<T>) _then)
      : super(_value, (v) => _then(v as OtpHashData<T>));

  @override
  OtpHashData<T> get _value => super._value as OtpHashData<T>;

  @override
  $Res call({
    Object data = freezed,
  }) {
    return _then(OtpHashData<T>(
      data: data == freezed ? _value.data : data as T,
    ));
  }
}

class _$OtpHashData<T> implements OtpHashData<T> {
  const _$OtpHashData({@required this.data}) : assert(data != null);

  @override
  final T data;

  @override
  String toString() {
    return 'AuthenticationState<$T>.otpHashData(data: $data)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is OtpHashData<T> &&
            (identical(other.data, data) ||
                const DeepCollectionEquality().equals(other.data, data)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(data);

  @override
  $OtpHashDataCopyWith<T, OtpHashData<T>> get copyWith =>
      _$OtpHashDataCopyWithImpl<T, OtpHashData<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result idle(),
    @required Result loginLoading(),
    @required Result loginHashData(T data),
    @required Result errorLogin(NetworkExceptions error),
    @required Result googleLoading(),
    @required Result googleHashData(T data),
    @required Result errorGoogle(NetworkExceptions error),
    @required Result facebookLoading(),
    @required Result facebookHashData(T data),
    @required Result errorFacebook(NetworkExceptions error),
    @required Result registerLoading(),
    @required Result registerHashData(T data),
    @required Result errorRegist(NetworkExceptions error),
    @required Result otpLoading(),
    @required Result otpHashData(T data),
    @required Result resendOtpHashData(T data),
    @required Result sendOtpHashData(T data),
    @required Result errorOtp(NetworkExceptions error),
    @required Result errorResendOtp(NetworkExceptions error),
    @required Result errorSendOtp(NetworkExceptions error),
    @required Result forgotPassLoading(),
    @required Result forgotPassHashData(T data),
    @required Result errorForgotPass(NetworkExceptions error),
    @required Result changePasswordHashData(T data),
    @required Result errorChangePassword(NetworkExceptions error),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return otpHashData(data);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result idle(),
    Result loginLoading(),
    Result loginHashData(T data),
    Result errorLogin(NetworkExceptions error),
    Result googleLoading(),
    Result googleHashData(T data),
    Result errorGoogle(NetworkExceptions error),
    Result facebookLoading(),
    Result facebookHashData(T data),
    Result errorFacebook(NetworkExceptions error),
    Result registerLoading(),
    Result registerHashData(T data),
    Result errorRegist(NetworkExceptions error),
    Result otpLoading(),
    Result otpHashData(T data),
    Result resendOtpHashData(T data),
    Result sendOtpHashData(T data),
    Result errorOtp(NetworkExceptions error),
    Result errorResendOtp(NetworkExceptions error),
    Result errorSendOtp(NetworkExceptions error),
    Result forgotPassLoading(),
    Result forgotPassHashData(T data),
    Result errorForgotPass(NetworkExceptions error),
    Result changePasswordHashData(T data),
    Result errorChangePassword(NetworkExceptions error),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (otpHashData != null) {
      return otpHashData(data);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result idle(Idle<T> value),
    @required Result loginLoading(LoginLoading<T> value),
    @required Result loginHashData(LoginHashData<T> value),
    @required Result errorLogin(ErrorLogin<T> value),
    @required Result googleLoading(GoogleLoading<T> value),
    @required Result googleHashData(GoogleHashData<T> value),
    @required Result errorGoogle(ErrorGoogle<T> value),
    @required Result facebookLoading(FacebookLoading<T> value),
    @required Result facebookHashData(FacebookHashData<T> value),
    @required Result errorFacebook(ErrorFacebook<T> value),
    @required Result registerLoading(RegisterLoading<T> value),
    @required Result registerHashData(RegisterHashData<T> value),
    @required Result errorRegist(ErrorRegist<T> value),
    @required Result otpLoading(OtpLoading<T> value),
    @required Result otpHashData(OtpHashData<T> value),
    @required Result resendOtpHashData(ResendOtpHashData<T> value),
    @required Result sendOtpHashData(SendOtpHashData<T> value),
    @required Result errorOtp(ErrorOtp<T> value),
    @required Result errorResendOtp(ErrorResendOtp<T> value),
    @required Result errorSendOtp(ErrorSendOtp<T> value),
    @required Result forgotPassLoading(ForgotPassLoading<T> value),
    @required Result forgotPassHashData(ForgotPassHashData<T> value),
    @required Result errorForgotPass(ErrorForgotPass<T> value),
    @required Result changePasswordHashData(ChangePasswordHashData<T> value),
    @required Result errorChangePassword(ErrorChangePassword<T> value),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return otpHashData(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result idle(Idle<T> value),
    Result loginLoading(LoginLoading<T> value),
    Result loginHashData(LoginHashData<T> value),
    Result errorLogin(ErrorLogin<T> value),
    Result googleLoading(GoogleLoading<T> value),
    Result googleHashData(GoogleHashData<T> value),
    Result errorGoogle(ErrorGoogle<T> value),
    Result facebookLoading(FacebookLoading<T> value),
    Result facebookHashData(FacebookHashData<T> value),
    Result errorFacebook(ErrorFacebook<T> value),
    Result registerLoading(RegisterLoading<T> value),
    Result registerHashData(RegisterHashData<T> value),
    Result errorRegist(ErrorRegist<T> value),
    Result otpLoading(OtpLoading<T> value),
    Result otpHashData(OtpHashData<T> value),
    Result resendOtpHashData(ResendOtpHashData<T> value),
    Result sendOtpHashData(SendOtpHashData<T> value),
    Result errorOtp(ErrorOtp<T> value),
    Result errorResendOtp(ErrorResendOtp<T> value),
    Result errorSendOtp(ErrorSendOtp<T> value),
    Result forgotPassLoading(ForgotPassLoading<T> value),
    Result forgotPassHashData(ForgotPassHashData<T> value),
    Result errorForgotPass(ErrorForgotPass<T> value),
    Result changePasswordHashData(ChangePasswordHashData<T> value),
    Result errorChangePassword(ErrorChangePassword<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (otpHashData != null) {
      return otpHashData(this);
    }
    return orElse();
  }
}

abstract class OtpHashData<T> implements AuthenticationState<T> {
  const factory OtpHashData({@required T data}) = _$OtpHashData<T>;

  T get data;
  $OtpHashDataCopyWith<T, OtpHashData<T>> get copyWith;
}

abstract class $ResendOtpHashDataCopyWith<T, $Res> {
  factory $ResendOtpHashDataCopyWith(ResendOtpHashData<T> value,
          $Res Function(ResendOtpHashData<T>) then) =
      _$ResendOtpHashDataCopyWithImpl<T, $Res>;
  $Res call({T data});
}

class _$ResendOtpHashDataCopyWithImpl<T, $Res>
    extends _$AuthenticationStateCopyWithImpl<T, $Res>
    implements $ResendOtpHashDataCopyWith<T, $Res> {
  _$ResendOtpHashDataCopyWithImpl(
      ResendOtpHashData<T> _value, $Res Function(ResendOtpHashData<T>) _then)
      : super(_value, (v) => _then(v as ResendOtpHashData<T>));

  @override
  ResendOtpHashData<T> get _value => super._value as ResendOtpHashData<T>;

  @override
  $Res call({
    Object data = freezed,
  }) {
    return _then(ResendOtpHashData<T>(
      data: data == freezed ? _value.data : data as T,
    ));
  }
}

class _$ResendOtpHashData<T> implements ResendOtpHashData<T> {
  const _$ResendOtpHashData({@required this.data}) : assert(data != null);

  @override
  final T data;

  @override
  String toString() {
    return 'AuthenticationState<$T>.resendOtpHashData(data: $data)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ResendOtpHashData<T> &&
            (identical(other.data, data) ||
                const DeepCollectionEquality().equals(other.data, data)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(data);

  @override
  $ResendOtpHashDataCopyWith<T, ResendOtpHashData<T>> get copyWith =>
      _$ResendOtpHashDataCopyWithImpl<T, ResendOtpHashData<T>>(
          this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result idle(),
    @required Result loginLoading(),
    @required Result loginHashData(T data),
    @required Result errorLogin(NetworkExceptions error),
    @required Result googleLoading(),
    @required Result googleHashData(T data),
    @required Result errorGoogle(NetworkExceptions error),
    @required Result facebookLoading(),
    @required Result facebookHashData(T data),
    @required Result errorFacebook(NetworkExceptions error),
    @required Result registerLoading(),
    @required Result registerHashData(T data),
    @required Result errorRegist(NetworkExceptions error),
    @required Result otpLoading(),
    @required Result otpHashData(T data),
    @required Result resendOtpHashData(T data),
    @required Result sendOtpHashData(T data),
    @required Result errorOtp(NetworkExceptions error),
    @required Result errorResendOtp(NetworkExceptions error),
    @required Result errorSendOtp(NetworkExceptions error),
    @required Result forgotPassLoading(),
    @required Result forgotPassHashData(T data),
    @required Result errorForgotPass(NetworkExceptions error),
    @required Result changePasswordHashData(T data),
    @required Result errorChangePassword(NetworkExceptions error),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return resendOtpHashData(data);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result idle(),
    Result loginLoading(),
    Result loginHashData(T data),
    Result errorLogin(NetworkExceptions error),
    Result googleLoading(),
    Result googleHashData(T data),
    Result errorGoogle(NetworkExceptions error),
    Result facebookLoading(),
    Result facebookHashData(T data),
    Result errorFacebook(NetworkExceptions error),
    Result registerLoading(),
    Result registerHashData(T data),
    Result errorRegist(NetworkExceptions error),
    Result otpLoading(),
    Result otpHashData(T data),
    Result resendOtpHashData(T data),
    Result sendOtpHashData(T data),
    Result errorOtp(NetworkExceptions error),
    Result errorResendOtp(NetworkExceptions error),
    Result errorSendOtp(NetworkExceptions error),
    Result forgotPassLoading(),
    Result forgotPassHashData(T data),
    Result errorForgotPass(NetworkExceptions error),
    Result changePasswordHashData(T data),
    Result errorChangePassword(NetworkExceptions error),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (resendOtpHashData != null) {
      return resendOtpHashData(data);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result idle(Idle<T> value),
    @required Result loginLoading(LoginLoading<T> value),
    @required Result loginHashData(LoginHashData<T> value),
    @required Result errorLogin(ErrorLogin<T> value),
    @required Result googleLoading(GoogleLoading<T> value),
    @required Result googleHashData(GoogleHashData<T> value),
    @required Result errorGoogle(ErrorGoogle<T> value),
    @required Result facebookLoading(FacebookLoading<T> value),
    @required Result facebookHashData(FacebookHashData<T> value),
    @required Result errorFacebook(ErrorFacebook<T> value),
    @required Result registerLoading(RegisterLoading<T> value),
    @required Result registerHashData(RegisterHashData<T> value),
    @required Result errorRegist(ErrorRegist<T> value),
    @required Result otpLoading(OtpLoading<T> value),
    @required Result otpHashData(OtpHashData<T> value),
    @required Result resendOtpHashData(ResendOtpHashData<T> value),
    @required Result sendOtpHashData(SendOtpHashData<T> value),
    @required Result errorOtp(ErrorOtp<T> value),
    @required Result errorResendOtp(ErrorResendOtp<T> value),
    @required Result errorSendOtp(ErrorSendOtp<T> value),
    @required Result forgotPassLoading(ForgotPassLoading<T> value),
    @required Result forgotPassHashData(ForgotPassHashData<T> value),
    @required Result errorForgotPass(ErrorForgotPass<T> value),
    @required Result changePasswordHashData(ChangePasswordHashData<T> value),
    @required Result errorChangePassword(ErrorChangePassword<T> value),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return resendOtpHashData(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result idle(Idle<T> value),
    Result loginLoading(LoginLoading<T> value),
    Result loginHashData(LoginHashData<T> value),
    Result errorLogin(ErrorLogin<T> value),
    Result googleLoading(GoogleLoading<T> value),
    Result googleHashData(GoogleHashData<T> value),
    Result errorGoogle(ErrorGoogle<T> value),
    Result facebookLoading(FacebookLoading<T> value),
    Result facebookHashData(FacebookHashData<T> value),
    Result errorFacebook(ErrorFacebook<T> value),
    Result registerLoading(RegisterLoading<T> value),
    Result registerHashData(RegisterHashData<T> value),
    Result errorRegist(ErrorRegist<T> value),
    Result otpLoading(OtpLoading<T> value),
    Result otpHashData(OtpHashData<T> value),
    Result resendOtpHashData(ResendOtpHashData<T> value),
    Result sendOtpHashData(SendOtpHashData<T> value),
    Result errorOtp(ErrorOtp<T> value),
    Result errorResendOtp(ErrorResendOtp<T> value),
    Result errorSendOtp(ErrorSendOtp<T> value),
    Result forgotPassLoading(ForgotPassLoading<T> value),
    Result forgotPassHashData(ForgotPassHashData<T> value),
    Result errorForgotPass(ErrorForgotPass<T> value),
    Result changePasswordHashData(ChangePasswordHashData<T> value),
    Result errorChangePassword(ErrorChangePassword<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (resendOtpHashData != null) {
      return resendOtpHashData(this);
    }
    return orElse();
  }
}

abstract class ResendOtpHashData<T> implements AuthenticationState<T> {
  const factory ResendOtpHashData({@required T data}) = _$ResendOtpHashData<T>;

  T get data;
  $ResendOtpHashDataCopyWith<T, ResendOtpHashData<T>> get copyWith;
}

abstract class $SendOtpHashDataCopyWith<T, $Res> {
  factory $SendOtpHashDataCopyWith(
          SendOtpHashData<T> value, $Res Function(SendOtpHashData<T>) then) =
      _$SendOtpHashDataCopyWithImpl<T, $Res>;
  $Res call({T data});
}

class _$SendOtpHashDataCopyWithImpl<T, $Res>
    extends _$AuthenticationStateCopyWithImpl<T, $Res>
    implements $SendOtpHashDataCopyWith<T, $Res> {
  _$SendOtpHashDataCopyWithImpl(
      SendOtpHashData<T> _value, $Res Function(SendOtpHashData<T>) _then)
      : super(_value, (v) => _then(v as SendOtpHashData<T>));

  @override
  SendOtpHashData<T> get _value => super._value as SendOtpHashData<T>;

  @override
  $Res call({
    Object data = freezed,
  }) {
    return _then(SendOtpHashData<T>(
      data: data == freezed ? _value.data : data as T,
    ));
  }
}

class _$SendOtpHashData<T> implements SendOtpHashData<T> {
  const _$SendOtpHashData({@required this.data}) : assert(data != null);

  @override
  final T data;

  @override
  String toString() {
    return 'AuthenticationState<$T>.sendOtpHashData(data: $data)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is SendOtpHashData<T> &&
            (identical(other.data, data) ||
                const DeepCollectionEquality().equals(other.data, data)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(data);

  @override
  $SendOtpHashDataCopyWith<T, SendOtpHashData<T>> get copyWith =>
      _$SendOtpHashDataCopyWithImpl<T, SendOtpHashData<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result idle(),
    @required Result loginLoading(),
    @required Result loginHashData(T data),
    @required Result errorLogin(NetworkExceptions error),
    @required Result googleLoading(),
    @required Result googleHashData(T data),
    @required Result errorGoogle(NetworkExceptions error),
    @required Result facebookLoading(),
    @required Result facebookHashData(T data),
    @required Result errorFacebook(NetworkExceptions error),
    @required Result registerLoading(),
    @required Result registerHashData(T data),
    @required Result errorRegist(NetworkExceptions error),
    @required Result otpLoading(),
    @required Result otpHashData(T data),
    @required Result resendOtpHashData(T data),
    @required Result sendOtpHashData(T data),
    @required Result errorOtp(NetworkExceptions error),
    @required Result errorResendOtp(NetworkExceptions error),
    @required Result errorSendOtp(NetworkExceptions error),
    @required Result forgotPassLoading(),
    @required Result forgotPassHashData(T data),
    @required Result errorForgotPass(NetworkExceptions error),
    @required Result changePasswordHashData(T data),
    @required Result errorChangePassword(NetworkExceptions error),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return sendOtpHashData(data);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result idle(),
    Result loginLoading(),
    Result loginHashData(T data),
    Result errorLogin(NetworkExceptions error),
    Result googleLoading(),
    Result googleHashData(T data),
    Result errorGoogle(NetworkExceptions error),
    Result facebookLoading(),
    Result facebookHashData(T data),
    Result errorFacebook(NetworkExceptions error),
    Result registerLoading(),
    Result registerHashData(T data),
    Result errorRegist(NetworkExceptions error),
    Result otpLoading(),
    Result otpHashData(T data),
    Result resendOtpHashData(T data),
    Result sendOtpHashData(T data),
    Result errorOtp(NetworkExceptions error),
    Result errorResendOtp(NetworkExceptions error),
    Result errorSendOtp(NetworkExceptions error),
    Result forgotPassLoading(),
    Result forgotPassHashData(T data),
    Result errorForgotPass(NetworkExceptions error),
    Result changePasswordHashData(T data),
    Result errorChangePassword(NetworkExceptions error),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (sendOtpHashData != null) {
      return sendOtpHashData(data);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result idle(Idle<T> value),
    @required Result loginLoading(LoginLoading<T> value),
    @required Result loginHashData(LoginHashData<T> value),
    @required Result errorLogin(ErrorLogin<T> value),
    @required Result googleLoading(GoogleLoading<T> value),
    @required Result googleHashData(GoogleHashData<T> value),
    @required Result errorGoogle(ErrorGoogle<T> value),
    @required Result facebookLoading(FacebookLoading<T> value),
    @required Result facebookHashData(FacebookHashData<T> value),
    @required Result errorFacebook(ErrorFacebook<T> value),
    @required Result registerLoading(RegisterLoading<T> value),
    @required Result registerHashData(RegisterHashData<T> value),
    @required Result errorRegist(ErrorRegist<T> value),
    @required Result otpLoading(OtpLoading<T> value),
    @required Result otpHashData(OtpHashData<T> value),
    @required Result resendOtpHashData(ResendOtpHashData<T> value),
    @required Result sendOtpHashData(SendOtpHashData<T> value),
    @required Result errorOtp(ErrorOtp<T> value),
    @required Result errorResendOtp(ErrorResendOtp<T> value),
    @required Result errorSendOtp(ErrorSendOtp<T> value),
    @required Result forgotPassLoading(ForgotPassLoading<T> value),
    @required Result forgotPassHashData(ForgotPassHashData<T> value),
    @required Result errorForgotPass(ErrorForgotPass<T> value),
    @required Result changePasswordHashData(ChangePasswordHashData<T> value),
    @required Result errorChangePassword(ErrorChangePassword<T> value),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return sendOtpHashData(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result idle(Idle<T> value),
    Result loginLoading(LoginLoading<T> value),
    Result loginHashData(LoginHashData<T> value),
    Result errorLogin(ErrorLogin<T> value),
    Result googleLoading(GoogleLoading<T> value),
    Result googleHashData(GoogleHashData<T> value),
    Result errorGoogle(ErrorGoogle<T> value),
    Result facebookLoading(FacebookLoading<T> value),
    Result facebookHashData(FacebookHashData<T> value),
    Result errorFacebook(ErrorFacebook<T> value),
    Result registerLoading(RegisterLoading<T> value),
    Result registerHashData(RegisterHashData<T> value),
    Result errorRegist(ErrorRegist<T> value),
    Result otpLoading(OtpLoading<T> value),
    Result otpHashData(OtpHashData<T> value),
    Result resendOtpHashData(ResendOtpHashData<T> value),
    Result sendOtpHashData(SendOtpHashData<T> value),
    Result errorOtp(ErrorOtp<T> value),
    Result errorResendOtp(ErrorResendOtp<T> value),
    Result errorSendOtp(ErrorSendOtp<T> value),
    Result forgotPassLoading(ForgotPassLoading<T> value),
    Result forgotPassHashData(ForgotPassHashData<T> value),
    Result errorForgotPass(ErrorForgotPass<T> value),
    Result changePasswordHashData(ChangePasswordHashData<T> value),
    Result errorChangePassword(ErrorChangePassword<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (sendOtpHashData != null) {
      return sendOtpHashData(this);
    }
    return orElse();
  }
}

abstract class SendOtpHashData<T> implements AuthenticationState<T> {
  const factory SendOtpHashData({@required T data}) = _$SendOtpHashData<T>;

  T get data;
  $SendOtpHashDataCopyWith<T, SendOtpHashData<T>> get copyWith;
}

abstract class $ErrorOtpCopyWith<T, $Res> {
  factory $ErrorOtpCopyWith(
          ErrorOtp<T> value, $Res Function(ErrorOtp<T>) then) =
      _$ErrorOtpCopyWithImpl<T, $Res>;
  $Res call({NetworkExceptions error});

  $NetworkExceptionsCopyWith<$Res> get error;
}

class _$ErrorOtpCopyWithImpl<T, $Res>
    extends _$AuthenticationStateCopyWithImpl<T, $Res>
    implements $ErrorOtpCopyWith<T, $Res> {
  _$ErrorOtpCopyWithImpl(ErrorOtp<T> _value, $Res Function(ErrorOtp<T>) _then)
      : super(_value, (v) => _then(v as ErrorOtp<T>));

  @override
  ErrorOtp<T> get _value => super._value as ErrorOtp<T>;

  @override
  $Res call({
    Object error = freezed,
  }) {
    return _then(ErrorOtp<T>(
      error: error == freezed ? _value.error : error as NetworkExceptions,
    ));
  }

  @override
  $NetworkExceptionsCopyWith<$Res> get error {
    if (_value.error == null) {
      return null;
    }
    return $NetworkExceptionsCopyWith<$Res>(_value.error, (value) {
      return _then(_value.copyWith(error: value));
    });
  }
}

class _$ErrorOtp<T> implements ErrorOtp<T> {
  const _$ErrorOtp({@required this.error}) : assert(error != null);

  @override
  final NetworkExceptions error;

  @override
  String toString() {
    return 'AuthenticationState<$T>.errorOtp(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ErrorOtp<T> &&
            (identical(other.error, error) ||
                const DeepCollectionEquality().equals(other.error, error)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(error);

  @override
  $ErrorOtpCopyWith<T, ErrorOtp<T>> get copyWith =>
      _$ErrorOtpCopyWithImpl<T, ErrorOtp<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result idle(),
    @required Result loginLoading(),
    @required Result loginHashData(T data),
    @required Result errorLogin(NetworkExceptions error),
    @required Result googleLoading(),
    @required Result googleHashData(T data),
    @required Result errorGoogle(NetworkExceptions error),
    @required Result facebookLoading(),
    @required Result facebookHashData(T data),
    @required Result errorFacebook(NetworkExceptions error),
    @required Result registerLoading(),
    @required Result registerHashData(T data),
    @required Result errorRegist(NetworkExceptions error),
    @required Result otpLoading(),
    @required Result otpHashData(T data),
    @required Result resendOtpHashData(T data),
    @required Result sendOtpHashData(T data),
    @required Result errorOtp(NetworkExceptions error),
    @required Result errorResendOtp(NetworkExceptions error),
    @required Result errorSendOtp(NetworkExceptions error),
    @required Result forgotPassLoading(),
    @required Result forgotPassHashData(T data),
    @required Result errorForgotPass(NetworkExceptions error),
    @required Result changePasswordHashData(T data),
    @required Result errorChangePassword(NetworkExceptions error),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return errorOtp(error);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result idle(),
    Result loginLoading(),
    Result loginHashData(T data),
    Result errorLogin(NetworkExceptions error),
    Result googleLoading(),
    Result googleHashData(T data),
    Result errorGoogle(NetworkExceptions error),
    Result facebookLoading(),
    Result facebookHashData(T data),
    Result errorFacebook(NetworkExceptions error),
    Result registerLoading(),
    Result registerHashData(T data),
    Result errorRegist(NetworkExceptions error),
    Result otpLoading(),
    Result otpHashData(T data),
    Result resendOtpHashData(T data),
    Result sendOtpHashData(T data),
    Result errorOtp(NetworkExceptions error),
    Result errorResendOtp(NetworkExceptions error),
    Result errorSendOtp(NetworkExceptions error),
    Result forgotPassLoading(),
    Result forgotPassHashData(T data),
    Result errorForgotPass(NetworkExceptions error),
    Result changePasswordHashData(T data),
    Result errorChangePassword(NetworkExceptions error),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (errorOtp != null) {
      return errorOtp(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result idle(Idle<T> value),
    @required Result loginLoading(LoginLoading<T> value),
    @required Result loginHashData(LoginHashData<T> value),
    @required Result errorLogin(ErrorLogin<T> value),
    @required Result googleLoading(GoogleLoading<T> value),
    @required Result googleHashData(GoogleHashData<T> value),
    @required Result errorGoogle(ErrorGoogle<T> value),
    @required Result facebookLoading(FacebookLoading<T> value),
    @required Result facebookHashData(FacebookHashData<T> value),
    @required Result errorFacebook(ErrorFacebook<T> value),
    @required Result registerLoading(RegisterLoading<T> value),
    @required Result registerHashData(RegisterHashData<T> value),
    @required Result errorRegist(ErrorRegist<T> value),
    @required Result otpLoading(OtpLoading<T> value),
    @required Result otpHashData(OtpHashData<T> value),
    @required Result resendOtpHashData(ResendOtpHashData<T> value),
    @required Result sendOtpHashData(SendOtpHashData<T> value),
    @required Result errorOtp(ErrorOtp<T> value),
    @required Result errorResendOtp(ErrorResendOtp<T> value),
    @required Result errorSendOtp(ErrorSendOtp<T> value),
    @required Result forgotPassLoading(ForgotPassLoading<T> value),
    @required Result forgotPassHashData(ForgotPassHashData<T> value),
    @required Result errorForgotPass(ErrorForgotPass<T> value),
    @required Result changePasswordHashData(ChangePasswordHashData<T> value),
    @required Result errorChangePassword(ErrorChangePassword<T> value),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return errorOtp(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result idle(Idle<T> value),
    Result loginLoading(LoginLoading<T> value),
    Result loginHashData(LoginHashData<T> value),
    Result errorLogin(ErrorLogin<T> value),
    Result googleLoading(GoogleLoading<T> value),
    Result googleHashData(GoogleHashData<T> value),
    Result errorGoogle(ErrorGoogle<T> value),
    Result facebookLoading(FacebookLoading<T> value),
    Result facebookHashData(FacebookHashData<T> value),
    Result errorFacebook(ErrorFacebook<T> value),
    Result registerLoading(RegisterLoading<T> value),
    Result registerHashData(RegisterHashData<T> value),
    Result errorRegist(ErrorRegist<T> value),
    Result otpLoading(OtpLoading<T> value),
    Result otpHashData(OtpHashData<T> value),
    Result resendOtpHashData(ResendOtpHashData<T> value),
    Result sendOtpHashData(SendOtpHashData<T> value),
    Result errorOtp(ErrorOtp<T> value),
    Result errorResendOtp(ErrorResendOtp<T> value),
    Result errorSendOtp(ErrorSendOtp<T> value),
    Result forgotPassLoading(ForgotPassLoading<T> value),
    Result forgotPassHashData(ForgotPassHashData<T> value),
    Result errorForgotPass(ErrorForgotPass<T> value),
    Result changePasswordHashData(ChangePasswordHashData<T> value),
    Result errorChangePassword(ErrorChangePassword<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (errorOtp != null) {
      return errorOtp(this);
    }
    return orElse();
  }
}

abstract class ErrorOtp<T> implements AuthenticationState<T> {
  const factory ErrorOtp({@required NetworkExceptions error}) = _$ErrorOtp<T>;

  NetworkExceptions get error;
  $ErrorOtpCopyWith<T, ErrorOtp<T>> get copyWith;
}

abstract class $ErrorResendOtpCopyWith<T, $Res> {
  factory $ErrorResendOtpCopyWith(
          ErrorResendOtp<T> value, $Res Function(ErrorResendOtp<T>) then) =
      _$ErrorResendOtpCopyWithImpl<T, $Res>;
  $Res call({NetworkExceptions error});

  $NetworkExceptionsCopyWith<$Res> get error;
}

class _$ErrorResendOtpCopyWithImpl<T, $Res>
    extends _$AuthenticationStateCopyWithImpl<T, $Res>
    implements $ErrorResendOtpCopyWith<T, $Res> {
  _$ErrorResendOtpCopyWithImpl(
      ErrorResendOtp<T> _value, $Res Function(ErrorResendOtp<T>) _then)
      : super(_value, (v) => _then(v as ErrorResendOtp<T>));

  @override
  ErrorResendOtp<T> get _value => super._value as ErrorResendOtp<T>;

  @override
  $Res call({
    Object error = freezed,
  }) {
    return _then(ErrorResendOtp<T>(
      error: error == freezed ? _value.error : error as NetworkExceptions,
    ));
  }

  @override
  $NetworkExceptionsCopyWith<$Res> get error {
    if (_value.error == null) {
      return null;
    }
    return $NetworkExceptionsCopyWith<$Res>(_value.error, (value) {
      return _then(_value.copyWith(error: value));
    });
  }
}

class _$ErrorResendOtp<T> implements ErrorResendOtp<T> {
  const _$ErrorResendOtp({@required this.error}) : assert(error != null);

  @override
  final NetworkExceptions error;

  @override
  String toString() {
    return 'AuthenticationState<$T>.errorResendOtp(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ErrorResendOtp<T> &&
            (identical(other.error, error) ||
                const DeepCollectionEquality().equals(other.error, error)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(error);

  @override
  $ErrorResendOtpCopyWith<T, ErrorResendOtp<T>> get copyWith =>
      _$ErrorResendOtpCopyWithImpl<T, ErrorResendOtp<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result idle(),
    @required Result loginLoading(),
    @required Result loginHashData(T data),
    @required Result errorLogin(NetworkExceptions error),
    @required Result googleLoading(),
    @required Result googleHashData(T data),
    @required Result errorGoogle(NetworkExceptions error),
    @required Result facebookLoading(),
    @required Result facebookHashData(T data),
    @required Result errorFacebook(NetworkExceptions error),
    @required Result registerLoading(),
    @required Result registerHashData(T data),
    @required Result errorRegist(NetworkExceptions error),
    @required Result otpLoading(),
    @required Result otpHashData(T data),
    @required Result resendOtpHashData(T data),
    @required Result sendOtpHashData(T data),
    @required Result errorOtp(NetworkExceptions error),
    @required Result errorResendOtp(NetworkExceptions error),
    @required Result errorSendOtp(NetworkExceptions error),
    @required Result forgotPassLoading(),
    @required Result forgotPassHashData(T data),
    @required Result errorForgotPass(NetworkExceptions error),
    @required Result changePasswordHashData(T data),
    @required Result errorChangePassword(NetworkExceptions error),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return errorResendOtp(error);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result idle(),
    Result loginLoading(),
    Result loginHashData(T data),
    Result errorLogin(NetworkExceptions error),
    Result googleLoading(),
    Result googleHashData(T data),
    Result errorGoogle(NetworkExceptions error),
    Result facebookLoading(),
    Result facebookHashData(T data),
    Result errorFacebook(NetworkExceptions error),
    Result registerLoading(),
    Result registerHashData(T data),
    Result errorRegist(NetworkExceptions error),
    Result otpLoading(),
    Result otpHashData(T data),
    Result resendOtpHashData(T data),
    Result sendOtpHashData(T data),
    Result errorOtp(NetworkExceptions error),
    Result errorResendOtp(NetworkExceptions error),
    Result errorSendOtp(NetworkExceptions error),
    Result forgotPassLoading(),
    Result forgotPassHashData(T data),
    Result errorForgotPass(NetworkExceptions error),
    Result changePasswordHashData(T data),
    Result errorChangePassword(NetworkExceptions error),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (errorResendOtp != null) {
      return errorResendOtp(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result idle(Idle<T> value),
    @required Result loginLoading(LoginLoading<T> value),
    @required Result loginHashData(LoginHashData<T> value),
    @required Result errorLogin(ErrorLogin<T> value),
    @required Result googleLoading(GoogleLoading<T> value),
    @required Result googleHashData(GoogleHashData<T> value),
    @required Result errorGoogle(ErrorGoogle<T> value),
    @required Result facebookLoading(FacebookLoading<T> value),
    @required Result facebookHashData(FacebookHashData<T> value),
    @required Result errorFacebook(ErrorFacebook<T> value),
    @required Result registerLoading(RegisterLoading<T> value),
    @required Result registerHashData(RegisterHashData<T> value),
    @required Result errorRegist(ErrorRegist<T> value),
    @required Result otpLoading(OtpLoading<T> value),
    @required Result otpHashData(OtpHashData<T> value),
    @required Result resendOtpHashData(ResendOtpHashData<T> value),
    @required Result sendOtpHashData(SendOtpHashData<T> value),
    @required Result errorOtp(ErrorOtp<T> value),
    @required Result errorResendOtp(ErrorResendOtp<T> value),
    @required Result errorSendOtp(ErrorSendOtp<T> value),
    @required Result forgotPassLoading(ForgotPassLoading<T> value),
    @required Result forgotPassHashData(ForgotPassHashData<T> value),
    @required Result errorForgotPass(ErrorForgotPass<T> value),
    @required Result changePasswordHashData(ChangePasswordHashData<T> value),
    @required Result errorChangePassword(ErrorChangePassword<T> value),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return errorResendOtp(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result idle(Idle<T> value),
    Result loginLoading(LoginLoading<T> value),
    Result loginHashData(LoginHashData<T> value),
    Result errorLogin(ErrorLogin<T> value),
    Result googleLoading(GoogleLoading<T> value),
    Result googleHashData(GoogleHashData<T> value),
    Result errorGoogle(ErrorGoogle<T> value),
    Result facebookLoading(FacebookLoading<T> value),
    Result facebookHashData(FacebookHashData<T> value),
    Result errorFacebook(ErrorFacebook<T> value),
    Result registerLoading(RegisterLoading<T> value),
    Result registerHashData(RegisterHashData<T> value),
    Result errorRegist(ErrorRegist<T> value),
    Result otpLoading(OtpLoading<T> value),
    Result otpHashData(OtpHashData<T> value),
    Result resendOtpHashData(ResendOtpHashData<T> value),
    Result sendOtpHashData(SendOtpHashData<T> value),
    Result errorOtp(ErrorOtp<T> value),
    Result errorResendOtp(ErrorResendOtp<T> value),
    Result errorSendOtp(ErrorSendOtp<T> value),
    Result forgotPassLoading(ForgotPassLoading<T> value),
    Result forgotPassHashData(ForgotPassHashData<T> value),
    Result errorForgotPass(ErrorForgotPass<T> value),
    Result changePasswordHashData(ChangePasswordHashData<T> value),
    Result errorChangePassword(ErrorChangePassword<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (errorResendOtp != null) {
      return errorResendOtp(this);
    }
    return orElse();
  }
}

abstract class ErrorResendOtp<T> implements AuthenticationState<T> {
  const factory ErrorResendOtp({@required NetworkExceptions error}) =
      _$ErrorResendOtp<T>;

  NetworkExceptions get error;
  $ErrorResendOtpCopyWith<T, ErrorResendOtp<T>> get copyWith;
}

abstract class $ErrorSendOtpCopyWith<T, $Res> {
  factory $ErrorSendOtpCopyWith(
          ErrorSendOtp<T> value, $Res Function(ErrorSendOtp<T>) then) =
      _$ErrorSendOtpCopyWithImpl<T, $Res>;
  $Res call({NetworkExceptions error});

  $NetworkExceptionsCopyWith<$Res> get error;
}

class _$ErrorSendOtpCopyWithImpl<T, $Res>
    extends _$AuthenticationStateCopyWithImpl<T, $Res>
    implements $ErrorSendOtpCopyWith<T, $Res> {
  _$ErrorSendOtpCopyWithImpl(
      ErrorSendOtp<T> _value, $Res Function(ErrorSendOtp<T>) _then)
      : super(_value, (v) => _then(v as ErrorSendOtp<T>));

  @override
  ErrorSendOtp<T> get _value => super._value as ErrorSendOtp<T>;

  @override
  $Res call({
    Object error = freezed,
  }) {
    return _then(ErrorSendOtp<T>(
      error: error == freezed ? _value.error : error as NetworkExceptions,
    ));
  }

  @override
  $NetworkExceptionsCopyWith<$Res> get error {
    if (_value.error == null) {
      return null;
    }
    return $NetworkExceptionsCopyWith<$Res>(_value.error, (value) {
      return _then(_value.copyWith(error: value));
    });
  }
}

class _$ErrorSendOtp<T> implements ErrorSendOtp<T> {
  const _$ErrorSendOtp({@required this.error}) : assert(error != null);

  @override
  final NetworkExceptions error;

  @override
  String toString() {
    return 'AuthenticationState<$T>.errorSendOtp(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ErrorSendOtp<T> &&
            (identical(other.error, error) ||
                const DeepCollectionEquality().equals(other.error, error)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(error);

  @override
  $ErrorSendOtpCopyWith<T, ErrorSendOtp<T>> get copyWith =>
      _$ErrorSendOtpCopyWithImpl<T, ErrorSendOtp<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result idle(),
    @required Result loginLoading(),
    @required Result loginHashData(T data),
    @required Result errorLogin(NetworkExceptions error),
    @required Result googleLoading(),
    @required Result googleHashData(T data),
    @required Result errorGoogle(NetworkExceptions error),
    @required Result facebookLoading(),
    @required Result facebookHashData(T data),
    @required Result errorFacebook(NetworkExceptions error),
    @required Result registerLoading(),
    @required Result registerHashData(T data),
    @required Result errorRegist(NetworkExceptions error),
    @required Result otpLoading(),
    @required Result otpHashData(T data),
    @required Result resendOtpHashData(T data),
    @required Result sendOtpHashData(T data),
    @required Result errorOtp(NetworkExceptions error),
    @required Result errorResendOtp(NetworkExceptions error),
    @required Result errorSendOtp(NetworkExceptions error),
    @required Result forgotPassLoading(),
    @required Result forgotPassHashData(T data),
    @required Result errorForgotPass(NetworkExceptions error),
    @required Result changePasswordHashData(T data),
    @required Result errorChangePassword(NetworkExceptions error),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return errorSendOtp(error);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result idle(),
    Result loginLoading(),
    Result loginHashData(T data),
    Result errorLogin(NetworkExceptions error),
    Result googleLoading(),
    Result googleHashData(T data),
    Result errorGoogle(NetworkExceptions error),
    Result facebookLoading(),
    Result facebookHashData(T data),
    Result errorFacebook(NetworkExceptions error),
    Result registerLoading(),
    Result registerHashData(T data),
    Result errorRegist(NetworkExceptions error),
    Result otpLoading(),
    Result otpHashData(T data),
    Result resendOtpHashData(T data),
    Result sendOtpHashData(T data),
    Result errorOtp(NetworkExceptions error),
    Result errorResendOtp(NetworkExceptions error),
    Result errorSendOtp(NetworkExceptions error),
    Result forgotPassLoading(),
    Result forgotPassHashData(T data),
    Result errorForgotPass(NetworkExceptions error),
    Result changePasswordHashData(T data),
    Result errorChangePassword(NetworkExceptions error),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (errorSendOtp != null) {
      return errorSendOtp(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result idle(Idle<T> value),
    @required Result loginLoading(LoginLoading<T> value),
    @required Result loginHashData(LoginHashData<T> value),
    @required Result errorLogin(ErrorLogin<T> value),
    @required Result googleLoading(GoogleLoading<T> value),
    @required Result googleHashData(GoogleHashData<T> value),
    @required Result errorGoogle(ErrorGoogle<T> value),
    @required Result facebookLoading(FacebookLoading<T> value),
    @required Result facebookHashData(FacebookHashData<T> value),
    @required Result errorFacebook(ErrorFacebook<T> value),
    @required Result registerLoading(RegisterLoading<T> value),
    @required Result registerHashData(RegisterHashData<T> value),
    @required Result errorRegist(ErrorRegist<T> value),
    @required Result otpLoading(OtpLoading<T> value),
    @required Result otpHashData(OtpHashData<T> value),
    @required Result resendOtpHashData(ResendOtpHashData<T> value),
    @required Result sendOtpHashData(SendOtpHashData<T> value),
    @required Result errorOtp(ErrorOtp<T> value),
    @required Result errorResendOtp(ErrorResendOtp<T> value),
    @required Result errorSendOtp(ErrorSendOtp<T> value),
    @required Result forgotPassLoading(ForgotPassLoading<T> value),
    @required Result forgotPassHashData(ForgotPassHashData<T> value),
    @required Result errorForgotPass(ErrorForgotPass<T> value),
    @required Result changePasswordHashData(ChangePasswordHashData<T> value),
    @required Result errorChangePassword(ErrorChangePassword<T> value),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return errorSendOtp(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result idle(Idle<T> value),
    Result loginLoading(LoginLoading<T> value),
    Result loginHashData(LoginHashData<T> value),
    Result errorLogin(ErrorLogin<T> value),
    Result googleLoading(GoogleLoading<T> value),
    Result googleHashData(GoogleHashData<T> value),
    Result errorGoogle(ErrorGoogle<T> value),
    Result facebookLoading(FacebookLoading<T> value),
    Result facebookHashData(FacebookHashData<T> value),
    Result errorFacebook(ErrorFacebook<T> value),
    Result registerLoading(RegisterLoading<T> value),
    Result registerHashData(RegisterHashData<T> value),
    Result errorRegist(ErrorRegist<T> value),
    Result otpLoading(OtpLoading<T> value),
    Result otpHashData(OtpHashData<T> value),
    Result resendOtpHashData(ResendOtpHashData<T> value),
    Result sendOtpHashData(SendOtpHashData<T> value),
    Result errorOtp(ErrorOtp<T> value),
    Result errorResendOtp(ErrorResendOtp<T> value),
    Result errorSendOtp(ErrorSendOtp<T> value),
    Result forgotPassLoading(ForgotPassLoading<T> value),
    Result forgotPassHashData(ForgotPassHashData<T> value),
    Result errorForgotPass(ErrorForgotPass<T> value),
    Result changePasswordHashData(ChangePasswordHashData<T> value),
    Result errorChangePassword(ErrorChangePassword<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (errorSendOtp != null) {
      return errorSendOtp(this);
    }
    return orElse();
  }
}

abstract class ErrorSendOtp<T> implements AuthenticationState<T> {
  const factory ErrorSendOtp({@required NetworkExceptions error}) =
      _$ErrorSendOtp<T>;

  NetworkExceptions get error;
  $ErrorSendOtpCopyWith<T, ErrorSendOtp<T>> get copyWith;
}

abstract class $ForgotPassLoadingCopyWith<T, $Res> {
  factory $ForgotPassLoadingCopyWith(ForgotPassLoading<T> value,
          $Res Function(ForgotPassLoading<T>) then) =
      _$ForgotPassLoadingCopyWithImpl<T, $Res>;
}

class _$ForgotPassLoadingCopyWithImpl<T, $Res>
    extends _$AuthenticationStateCopyWithImpl<T, $Res>
    implements $ForgotPassLoadingCopyWith<T, $Res> {
  _$ForgotPassLoadingCopyWithImpl(
      ForgotPassLoading<T> _value, $Res Function(ForgotPassLoading<T>) _then)
      : super(_value, (v) => _then(v as ForgotPassLoading<T>));

  @override
  ForgotPassLoading<T> get _value => super._value as ForgotPassLoading<T>;
}

class _$ForgotPassLoading<T> implements ForgotPassLoading<T> {
  const _$ForgotPassLoading();

  @override
  String toString() {
    return 'AuthenticationState<$T>.forgotPassLoading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is ForgotPassLoading<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result idle(),
    @required Result loginLoading(),
    @required Result loginHashData(T data),
    @required Result errorLogin(NetworkExceptions error),
    @required Result googleLoading(),
    @required Result googleHashData(T data),
    @required Result errorGoogle(NetworkExceptions error),
    @required Result facebookLoading(),
    @required Result facebookHashData(T data),
    @required Result errorFacebook(NetworkExceptions error),
    @required Result registerLoading(),
    @required Result registerHashData(T data),
    @required Result errorRegist(NetworkExceptions error),
    @required Result otpLoading(),
    @required Result otpHashData(T data),
    @required Result resendOtpHashData(T data),
    @required Result sendOtpHashData(T data),
    @required Result errorOtp(NetworkExceptions error),
    @required Result errorResendOtp(NetworkExceptions error),
    @required Result errorSendOtp(NetworkExceptions error),
    @required Result forgotPassLoading(),
    @required Result forgotPassHashData(T data),
    @required Result errorForgotPass(NetworkExceptions error),
    @required Result changePasswordHashData(T data),
    @required Result errorChangePassword(NetworkExceptions error),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return forgotPassLoading();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result idle(),
    Result loginLoading(),
    Result loginHashData(T data),
    Result errorLogin(NetworkExceptions error),
    Result googleLoading(),
    Result googleHashData(T data),
    Result errorGoogle(NetworkExceptions error),
    Result facebookLoading(),
    Result facebookHashData(T data),
    Result errorFacebook(NetworkExceptions error),
    Result registerLoading(),
    Result registerHashData(T data),
    Result errorRegist(NetworkExceptions error),
    Result otpLoading(),
    Result otpHashData(T data),
    Result resendOtpHashData(T data),
    Result sendOtpHashData(T data),
    Result errorOtp(NetworkExceptions error),
    Result errorResendOtp(NetworkExceptions error),
    Result errorSendOtp(NetworkExceptions error),
    Result forgotPassLoading(),
    Result forgotPassHashData(T data),
    Result errorForgotPass(NetworkExceptions error),
    Result changePasswordHashData(T data),
    Result errorChangePassword(NetworkExceptions error),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (forgotPassLoading != null) {
      return forgotPassLoading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result idle(Idle<T> value),
    @required Result loginLoading(LoginLoading<T> value),
    @required Result loginHashData(LoginHashData<T> value),
    @required Result errorLogin(ErrorLogin<T> value),
    @required Result googleLoading(GoogleLoading<T> value),
    @required Result googleHashData(GoogleHashData<T> value),
    @required Result errorGoogle(ErrorGoogle<T> value),
    @required Result facebookLoading(FacebookLoading<T> value),
    @required Result facebookHashData(FacebookHashData<T> value),
    @required Result errorFacebook(ErrorFacebook<T> value),
    @required Result registerLoading(RegisterLoading<T> value),
    @required Result registerHashData(RegisterHashData<T> value),
    @required Result errorRegist(ErrorRegist<T> value),
    @required Result otpLoading(OtpLoading<T> value),
    @required Result otpHashData(OtpHashData<T> value),
    @required Result resendOtpHashData(ResendOtpHashData<T> value),
    @required Result sendOtpHashData(SendOtpHashData<T> value),
    @required Result errorOtp(ErrorOtp<T> value),
    @required Result errorResendOtp(ErrorResendOtp<T> value),
    @required Result errorSendOtp(ErrorSendOtp<T> value),
    @required Result forgotPassLoading(ForgotPassLoading<T> value),
    @required Result forgotPassHashData(ForgotPassHashData<T> value),
    @required Result errorForgotPass(ErrorForgotPass<T> value),
    @required Result changePasswordHashData(ChangePasswordHashData<T> value),
    @required Result errorChangePassword(ErrorChangePassword<T> value),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return forgotPassLoading(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result idle(Idle<T> value),
    Result loginLoading(LoginLoading<T> value),
    Result loginHashData(LoginHashData<T> value),
    Result errorLogin(ErrorLogin<T> value),
    Result googleLoading(GoogleLoading<T> value),
    Result googleHashData(GoogleHashData<T> value),
    Result errorGoogle(ErrorGoogle<T> value),
    Result facebookLoading(FacebookLoading<T> value),
    Result facebookHashData(FacebookHashData<T> value),
    Result errorFacebook(ErrorFacebook<T> value),
    Result registerLoading(RegisterLoading<T> value),
    Result registerHashData(RegisterHashData<T> value),
    Result errorRegist(ErrorRegist<T> value),
    Result otpLoading(OtpLoading<T> value),
    Result otpHashData(OtpHashData<T> value),
    Result resendOtpHashData(ResendOtpHashData<T> value),
    Result sendOtpHashData(SendOtpHashData<T> value),
    Result errorOtp(ErrorOtp<T> value),
    Result errorResendOtp(ErrorResendOtp<T> value),
    Result errorSendOtp(ErrorSendOtp<T> value),
    Result forgotPassLoading(ForgotPassLoading<T> value),
    Result forgotPassHashData(ForgotPassHashData<T> value),
    Result errorForgotPass(ErrorForgotPass<T> value),
    Result changePasswordHashData(ChangePasswordHashData<T> value),
    Result errorChangePassword(ErrorChangePassword<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (forgotPassLoading != null) {
      return forgotPassLoading(this);
    }
    return orElse();
  }
}

abstract class ForgotPassLoading<T> implements AuthenticationState<T> {
  const factory ForgotPassLoading() = _$ForgotPassLoading<T>;
}

abstract class $ForgotPassHashDataCopyWith<T, $Res> {
  factory $ForgotPassHashDataCopyWith(ForgotPassHashData<T> value,
          $Res Function(ForgotPassHashData<T>) then) =
      _$ForgotPassHashDataCopyWithImpl<T, $Res>;
  $Res call({T data});
}

class _$ForgotPassHashDataCopyWithImpl<T, $Res>
    extends _$AuthenticationStateCopyWithImpl<T, $Res>
    implements $ForgotPassHashDataCopyWith<T, $Res> {
  _$ForgotPassHashDataCopyWithImpl(
      ForgotPassHashData<T> _value, $Res Function(ForgotPassHashData<T>) _then)
      : super(_value, (v) => _then(v as ForgotPassHashData<T>));

  @override
  ForgotPassHashData<T> get _value => super._value as ForgotPassHashData<T>;

  @override
  $Res call({
    Object data = freezed,
  }) {
    return _then(ForgotPassHashData<T>(
      data: data == freezed ? _value.data : data as T,
    ));
  }
}

class _$ForgotPassHashData<T> implements ForgotPassHashData<T> {
  const _$ForgotPassHashData({@required this.data}) : assert(data != null);

  @override
  final T data;

  @override
  String toString() {
    return 'AuthenticationState<$T>.forgotPassHashData(data: $data)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ForgotPassHashData<T> &&
            (identical(other.data, data) ||
                const DeepCollectionEquality().equals(other.data, data)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(data);

  @override
  $ForgotPassHashDataCopyWith<T, ForgotPassHashData<T>> get copyWith =>
      _$ForgotPassHashDataCopyWithImpl<T, ForgotPassHashData<T>>(
          this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result idle(),
    @required Result loginLoading(),
    @required Result loginHashData(T data),
    @required Result errorLogin(NetworkExceptions error),
    @required Result googleLoading(),
    @required Result googleHashData(T data),
    @required Result errorGoogle(NetworkExceptions error),
    @required Result facebookLoading(),
    @required Result facebookHashData(T data),
    @required Result errorFacebook(NetworkExceptions error),
    @required Result registerLoading(),
    @required Result registerHashData(T data),
    @required Result errorRegist(NetworkExceptions error),
    @required Result otpLoading(),
    @required Result otpHashData(T data),
    @required Result resendOtpHashData(T data),
    @required Result sendOtpHashData(T data),
    @required Result errorOtp(NetworkExceptions error),
    @required Result errorResendOtp(NetworkExceptions error),
    @required Result errorSendOtp(NetworkExceptions error),
    @required Result forgotPassLoading(),
    @required Result forgotPassHashData(T data),
    @required Result errorForgotPass(NetworkExceptions error),
    @required Result changePasswordHashData(T data),
    @required Result errorChangePassword(NetworkExceptions error),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return forgotPassHashData(data);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result idle(),
    Result loginLoading(),
    Result loginHashData(T data),
    Result errorLogin(NetworkExceptions error),
    Result googleLoading(),
    Result googleHashData(T data),
    Result errorGoogle(NetworkExceptions error),
    Result facebookLoading(),
    Result facebookHashData(T data),
    Result errorFacebook(NetworkExceptions error),
    Result registerLoading(),
    Result registerHashData(T data),
    Result errorRegist(NetworkExceptions error),
    Result otpLoading(),
    Result otpHashData(T data),
    Result resendOtpHashData(T data),
    Result sendOtpHashData(T data),
    Result errorOtp(NetworkExceptions error),
    Result errorResendOtp(NetworkExceptions error),
    Result errorSendOtp(NetworkExceptions error),
    Result forgotPassLoading(),
    Result forgotPassHashData(T data),
    Result errorForgotPass(NetworkExceptions error),
    Result changePasswordHashData(T data),
    Result errorChangePassword(NetworkExceptions error),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (forgotPassHashData != null) {
      return forgotPassHashData(data);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result idle(Idle<T> value),
    @required Result loginLoading(LoginLoading<T> value),
    @required Result loginHashData(LoginHashData<T> value),
    @required Result errorLogin(ErrorLogin<T> value),
    @required Result googleLoading(GoogleLoading<T> value),
    @required Result googleHashData(GoogleHashData<T> value),
    @required Result errorGoogle(ErrorGoogle<T> value),
    @required Result facebookLoading(FacebookLoading<T> value),
    @required Result facebookHashData(FacebookHashData<T> value),
    @required Result errorFacebook(ErrorFacebook<T> value),
    @required Result registerLoading(RegisterLoading<T> value),
    @required Result registerHashData(RegisterHashData<T> value),
    @required Result errorRegist(ErrorRegist<T> value),
    @required Result otpLoading(OtpLoading<T> value),
    @required Result otpHashData(OtpHashData<T> value),
    @required Result resendOtpHashData(ResendOtpHashData<T> value),
    @required Result sendOtpHashData(SendOtpHashData<T> value),
    @required Result errorOtp(ErrorOtp<T> value),
    @required Result errorResendOtp(ErrorResendOtp<T> value),
    @required Result errorSendOtp(ErrorSendOtp<T> value),
    @required Result forgotPassLoading(ForgotPassLoading<T> value),
    @required Result forgotPassHashData(ForgotPassHashData<T> value),
    @required Result errorForgotPass(ErrorForgotPass<T> value),
    @required Result changePasswordHashData(ChangePasswordHashData<T> value),
    @required Result errorChangePassword(ErrorChangePassword<T> value),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return forgotPassHashData(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result idle(Idle<T> value),
    Result loginLoading(LoginLoading<T> value),
    Result loginHashData(LoginHashData<T> value),
    Result errorLogin(ErrorLogin<T> value),
    Result googleLoading(GoogleLoading<T> value),
    Result googleHashData(GoogleHashData<T> value),
    Result errorGoogle(ErrorGoogle<T> value),
    Result facebookLoading(FacebookLoading<T> value),
    Result facebookHashData(FacebookHashData<T> value),
    Result errorFacebook(ErrorFacebook<T> value),
    Result registerLoading(RegisterLoading<T> value),
    Result registerHashData(RegisterHashData<T> value),
    Result errorRegist(ErrorRegist<T> value),
    Result otpLoading(OtpLoading<T> value),
    Result otpHashData(OtpHashData<T> value),
    Result resendOtpHashData(ResendOtpHashData<T> value),
    Result sendOtpHashData(SendOtpHashData<T> value),
    Result errorOtp(ErrorOtp<T> value),
    Result errorResendOtp(ErrorResendOtp<T> value),
    Result errorSendOtp(ErrorSendOtp<T> value),
    Result forgotPassLoading(ForgotPassLoading<T> value),
    Result forgotPassHashData(ForgotPassHashData<T> value),
    Result errorForgotPass(ErrorForgotPass<T> value),
    Result changePasswordHashData(ChangePasswordHashData<T> value),
    Result errorChangePassword(ErrorChangePassword<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (forgotPassHashData != null) {
      return forgotPassHashData(this);
    }
    return orElse();
  }
}

abstract class ForgotPassHashData<T> implements AuthenticationState<T> {
  const factory ForgotPassHashData({@required T data}) =
      _$ForgotPassHashData<T>;

  T get data;
  $ForgotPassHashDataCopyWith<T, ForgotPassHashData<T>> get copyWith;
}

abstract class $ErrorForgotPassCopyWith<T, $Res> {
  factory $ErrorForgotPassCopyWith(
          ErrorForgotPass<T> value, $Res Function(ErrorForgotPass<T>) then) =
      _$ErrorForgotPassCopyWithImpl<T, $Res>;
  $Res call({NetworkExceptions error});

  $NetworkExceptionsCopyWith<$Res> get error;
}

class _$ErrorForgotPassCopyWithImpl<T, $Res>
    extends _$AuthenticationStateCopyWithImpl<T, $Res>
    implements $ErrorForgotPassCopyWith<T, $Res> {
  _$ErrorForgotPassCopyWithImpl(
      ErrorForgotPass<T> _value, $Res Function(ErrorForgotPass<T>) _then)
      : super(_value, (v) => _then(v as ErrorForgotPass<T>));

  @override
  ErrorForgotPass<T> get _value => super._value as ErrorForgotPass<T>;

  @override
  $Res call({
    Object error = freezed,
  }) {
    return _then(ErrorForgotPass<T>(
      error: error == freezed ? _value.error : error as NetworkExceptions,
    ));
  }

  @override
  $NetworkExceptionsCopyWith<$Res> get error {
    if (_value.error == null) {
      return null;
    }
    return $NetworkExceptionsCopyWith<$Res>(_value.error, (value) {
      return _then(_value.copyWith(error: value));
    });
  }
}

class _$ErrorForgotPass<T> implements ErrorForgotPass<T> {
  const _$ErrorForgotPass({@required this.error}) : assert(error != null);

  @override
  final NetworkExceptions error;

  @override
  String toString() {
    return 'AuthenticationState<$T>.errorForgotPass(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ErrorForgotPass<T> &&
            (identical(other.error, error) ||
                const DeepCollectionEquality().equals(other.error, error)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(error);

  @override
  $ErrorForgotPassCopyWith<T, ErrorForgotPass<T>> get copyWith =>
      _$ErrorForgotPassCopyWithImpl<T, ErrorForgotPass<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result idle(),
    @required Result loginLoading(),
    @required Result loginHashData(T data),
    @required Result errorLogin(NetworkExceptions error),
    @required Result googleLoading(),
    @required Result googleHashData(T data),
    @required Result errorGoogle(NetworkExceptions error),
    @required Result facebookLoading(),
    @required Result facebookHashData(T data),
    @required Result errorFacebook(NetworkExceptions error),
    @required Result registerLoading(),
    @required Result registerHashData(T data),
    @required Result errorRegist(NetworkExceptions error),
    @required Result otpLoading(),
    @required Result otpHashData(T data),
    @required Result resendOtpHashData(T data),
    @required Result sendOtpHashData(T data),
    @required Result errorOtp(NetworkExceptions error),
    @required Result errorResendOtp(NetworkExceptions error),
    @required Result errorSendOtp(NetworkExceptions error),
    @required Result forgotPassLoading(),
    @required Result forgotPassHashData(T data),
    @required Result errorForgotPass(NetworkExceptions error),
    @required Result changePasswordHashData(T data),
    @required Result errorChangePassword(NetworkExceptions error),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return errorForgotPass(error);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result idle(),
    Result loginLoading(),
    Result loginHashData(T data),
    Result errorLogin(NetworkExceptions error),
    Result googleLoading(),
    Result googleHashData(T data),
    Result errorGoogle(NetworkExceptions error),
    Result facebookLoading(),
    Result facebookHashData(T data),
    Result errorFacebook(NetworkExceptions error),
    Result registerLoading(),
    Result registerHashData(T data),
    Result errorRegist(NetworkExceptions error),
    Result otpLoading(),
    Result otpHashData(T data),
    Result resendOtpHashData(T data),
    Result sendOtpHashData(T data),
    Result errorOtp(NetworkExceptions error),
    Result errorResendOtp(NetworkExceptions error),
    Result errorSendOtp(NetworkExceptions error),
    Result forgotPassLoading(),
    Result forgotPassHashData(T data),
    Result errorForgotPass(NetworkExceptions error),
    Result changePasswordHashData(T data),
    Result errorChangePassword(NetworkExceptions error),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (errorForgotPass != null) {
      return errorForgotPass(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result idle(Idle<T> value),
    @required Result loginLoading(LoginLoading<T> value),
    @required Result loginHashData(LoginHashData<T> value),
    @required Result errorLogin(ErrorLogin<T> value),
    @required Result googleLoading(GoogleLoading<T> value),
    @required Result googleHashData(GoogleHashData<T> value),
    @required Result errorGoogle(ErrorGoogle<T> value),
    @required Result facebookLoading(FacebookLoading<T> value),
    @required Result facebookHashData(FacebookHashData<T> value),
    @required Result errorFacebook(ErrorFacebook<T> value),
    @required Result registerLoading(RegisterLoading<T> value),
    @required Result registerHashData(RegisterHashData<T> value),
    @required Result errorRegist(ErrorRegist<T> value),
    @required Result otpLoading(OtpLoading<T> value),
    @required Result otpHashData(OtpHashData<T> value),
    @required Result resendOtpHashData(ResendOtpHashData<T> value),
    @required Result sendOtpHashData(SendOtpHashData<T> value),
    @required Result errorOtp(ErrorOtp<T> value),
    @required Result errorResendOtp(ErrorResendOtp<T> value),
    @required Result errorSendOtp(ErrorSendOtp<T> value),
    @required Result forgotPassLoading(ForgotPassLoading<T> value),
    @required Result forgotPassHashData(ForgotPassHashData<T> value),
    @required Result errorForgotPass(ErrorForgotPass<T> value),
    @required Result changePasswordHashData(ChangePasswordHashData<T> value),
    @required Result errorChangePassword(ErrorChangePassword<T> value),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return errorForgotPass(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result idle(Idle<T> value),
    Result loginLoading(LoginLoading<T> value),
    Result loginHashData(LoginHashData<T> value),
    Result errorLogin(ErrorLogin<T> value),
    Result googleLoading(GoogleLoading<T> value),
    Result googleHashData(GoogleHashData<T> value),
    Result errorGoogle(ErrorGoogle<T> value),
    Result facebookLoading(FacebookLoading<T> value),
    Result facebookHashData(FacebookHashData<T> value),
    Result errorFacebook(ErrorFacebook<T> value),
    Result registerLoading(RegisterLoading<T> value),
    Result registerHashData(RegisterHashData<T> value),
    Result errorRegist(ErrorRegist<T> value),
    Result otpLoading(OtpLoading<T> value),
    Result otpHashData(OtpHashData<T> value),
    Result resendOtpHashData(ResendOtpHashData<T> value),
    Result sendOtpHashData(SendOtpHashData<T> value),
    Result errorOtp(ErrorOtp<T> value),
    Result errorResendOtp(ErrorResendOtp<T> value),
    Result errorSendOtp(ErrorSendOtp<T> value),
    Result forgotPassLoading(ForgotPassLoading<T> value),
    Result forgotPassHashData(ForgotPassHashData<T> value),
    Result errorForgotPass(ErrorForgotPass<T> value),
    Result changePasswordHashData(ChangePasswordHashData<T> value),
    Result errorChangePassword(ErrorChangePassword<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (errorForgotPass != null) {
      return errorForgotPass(this);
    }
    return orElse();
  }
}

abstract class ErrorForgotPass<T> implements AuthenticationState<T> {
  const factory ErrorForgotPass({@required NetworkExceptions error}) =
      _$ErrorForgotPass<T>;

  NetworkExceptions get error;
  $ErrorForgotPassCopyWith<T, ErrorForgotPass<T>> get copyWith;
}

abstract class $ChangePasswordHashDataCopyWith<T, $Res> {
  factory $ChangePasswordHashDataCopyWith(ChangePasswordHashData<T> value,
          $Res Function(ChangePasswordHashData<T>) then) =
      _$ChangePasswordHashDataCopyWithImpl<T, $Res>;
  $Res call({T data});
}

class _$ChangePasswordHashDataCopyWithImpl<T, $Res>
    extends _$AuthenticationStateCopyWithImpl<T, $Res>
    implements $ChangePasswordHashDataCopyWith<T, $Res> {
  _$ChangePasswordHashDataCopyWithImpl(ChangePasswordHashData<T> _value,
      $Res Function(ChangePasswordHashData<T>) _then)
      : super(_value, (v) => _then(v as ChangePasswordHashData<T>));

  @override
  ChangePasswordHashData<T> get _value =>
      super._value as ChangePasswordHashData<T>;

  @override
  $Res call({
    Object data = freezed,
  }) {
    return _then(ChangePasswordHashData<T>(
      data: data == freezed ? _value.data : data as T,
    ));
  }
}

class _$ChangePasswordHashData<T> implements ChangePasswordHashData<T> {
  const _$ChangePasswordHashData({@required this.data}) : assert(data != null);

  @override
  final T data;

  @override
  String toString() {
    return 'AuthenticationState<$T>.changePasswordHashData(data: $data)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ChangePasswordHashData<T> &&
            (identical(other.data, data) ||
                const DeepCollectionEquality().equals(other.data, data)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(data);

  @override
  $ChangePasswordHashDataCopyWith<T, ChangePasswordHashData<T>> get copyWith =>
      _$ChangePasswordHashDataCopyWithImpl<T, ChangePasswordHashData<T>>(
          this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result idle(),
    @required Result loginLoading(),
    @required Result loginHashData(T data),
    @required Result errorLogin(NetworkExceptions error),
    @required Result googleLoading(),
    @required Result googleHashData(T data),
    @required Result errorGoogle(NetworkExceptions error),
    @required Result facebookLoading(),
    @required Result facebookHashData(T data),
    @required Result errorFacebook(NetworkExceptions error),
    @required Result registerLoading(),
    @required Result registerHashData(T data),
    @required Result errorRegist(NetworkExceptions error),
    @required Result otpLoading(),
    @required Result otpHashData(T data),
    @required Result resendOtpHashData(T data),
    @required Result sendOtpHashData(T data),
    @required Result errorOtp(NetworkExceptions error),
    @required Result errorResendOtp(NetworkExceptions error),
    @required Result errorSendOtp(NetworkExceptions error),
    @required Result forgotPassLoading(),
    @required Result forgotPassHashData(T data),
    @required Result errorForgotPass(NetworkExceptions error),
    @required Result changePasswordHashData(T data),
    @required Result errorChangePassword(NetworkExceptions error),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return changePasswordHashData(data);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result idle(),
    Result loginLoading(),
    Result loginHashData(T data),
    Result errorLogin(NetworkExceptions error),
    Result googleLoading(),
    Result googleHashData(T data),
    Result errorGoogle(NetworkExceptions error),
    Result facebookLoading(),
    Result facebookHashData(T data),
    Result errorFacebook(NetworkExceptions error),
    Result registerLoading(),
    Result registerHashData(T data),
    Result errorRegist(NetworkExceptions error),
    Result otpLoading(),
    Result otpHashData(T data),
    Result resendOtpHashData(T data),
    Result sendOtpHashData(T data),
    Result errorOtp(NetworkExceptions error),
    Result errorResendOtp(NetworkExceptions error),
    Result errorSendOtp(NetworkExceptions error),
    Result forgotPassLoading(),
    Result forgotPassHashData(T data),
    Result errorForgotPass(NetworkExceptions error),
    Result changePasswordHashData(T data),
    Result errorChangePassword(NetworkExceptions error),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (changePasswordHashData != null) {
      return changePasswordHashData(data);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result idle(Idle<T> value),
    @required Result loginLoading(LoginLoading<T> value),
    @required Result loginHashData(LoginHashData<T> value),
    @required Result errorLogin(ErrorLogin<T> value),
    @required Result googleLoading(GoogleLoading<T> value),
    @required Result googleHashData(GoogleHashData<T> value),
    @required Result errorGoogle(ErrorGoogle<T> value),
    @required Result facebookLoading(FacebookLoading<T> value),
    @required Result facebookHashData(FacebookHashData<T> value),
    @required Result errorFacebook(ErrorFacebook<T> value),
    @required Result registerLoading(RegisterLoading<T> value),
    @required Result registerHashData(RegisterHashData<T> value),
    @required Result errorRegist(ErrorRegist<T> value),
    @required Result otpLoading(OtpLoading<T> value),
    @required Result otpHashData(OtpHashData<T> value),
    @required Result resendOtpHashData(ResendOtpHashData<T> value),
    @required Result sendOtpHashData(SendOtpHashData<T> value),
    @required Result errorOtp(ErrorOtp<T> value),
    @required Result errorResendOtp(ErrorResendOtp<T> value),
    @required Result errorSendOtp(ErrorSendOtp<T> value),
    @required Result forgotPassLoading(ForgotPassLoading<T> value),
    @required Result forgotPassHashData(ForgotPassHashData<T> value),
    @required Result errorForgotPass(ErrorForgotPass<T> value),
    @required Result changePasswordHashData(ChangePasswordHashData<T> value),
    @required Result errorChangePassword(ErrorChangePassword<T> value),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return changePasswordHashData(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result idle(Idle<T> value),
    Result loginLoading(LoginLoading<T> value),
    Result loginHashData(LoginHashData<T> value),
    Result errorLogin(ErrorLogin<T> value),
    Result googleLoading(GoogleLoading<T> value),
    Result googleHashData(GoogleHashData<T> value),
    Result errorGoogle(ErrorGoogle<T> value),
    Result facebookLoading(FacebookLoading<T> value),
    Result facebookHashData(FacebookHashData<T> value),
    Result errorFacebook(ErrorFacebook<T> value),
    Result registerLoading(RegisterLoading<T> value),
    Result registerHashData(RegisterHashData<T> value),
    Result errorRegist(ErrorRegist<T> value),
    Result otpLoading(OtpLoading<T> value),
    Result otpHashData(OtpHashData<T> value),
    Result resendOtpHashData(ResendOtpHashData<T> value),
    Result sendOtpHashData(SendOtpHashData<T> value),
    Result errorOtp(ErrorOtp<T> value),
    Result errorResendOtp(ErrorResendOtp<T> value),
    Result errorSendOtp(ErrorSendOtp<T> value),
    Result forgotPassLoading(ForgotPassLoading<T> value),
    Result forgotPassHashData(ForgotPassHashData<T> value),
    Result errorForgotPass(ErrorForgotPass<T> value),
    Result changePasswordHashData(ChangePasswordHashData<T> value),
    Result errorChangePassword(ErrorChangePassword<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (changePasswordHashData != null) {
      return changePasswordHashData(this);
    }
    return orElse();
  }
}

abstract class ChangePasswordHashData<T> implements AuthenticationState<T> {
  const factory ChangePasswordHashData({@required T data}) =
      _$ChangePasswordHashData<T>;

  T get data;
  $ChangePasswordHashDataCopyWith<T, ChangePasswordHashData<T>> get copyWith;
}

abstract class $ErrorChangePasswordCopyWith<T, $Res> {
  factory $ErrorChangePasswordCopyWith(ErrorChangePassword<T> value,
          $Res Function(ErrorChangePassword<T>) then) =
      _$ErrorChangePasswordCopyWithImpl<T, $Res>;
  $Res call({NetworkExceptions error});

  $NetworkExceptionsCopyWith<$Res> get error;
}

class _$ErrorChangePasswordCopyWithImpl<T, $Res>
    extends _$AuthenticationStateCopyWithImpl<T, $Res>
    implements $ErrorChangePasswordCopyWith<T, $Res> {
  _$ErrorChangePasswordCopyWithImpl(ErrorChangePassword<T> _value,
      $Res Function(ErrorChangePassword<T>) _then)
      : super(_value, (v) => _then(v as ErrorChangePassword<T>));

  @override
  ErrorChangePassword<T> get _value => super._value as ErrorChangePassword<T>;

  @override
  $Res call({
    Object error = freezed,
  }) {
    return _then(ErrorChangePassword<T>(
      error: error == freezed ? _value.error : error as NetworkExceptions,
    ));
  }

  @override
  $NetworkExceptionsCopyWith<$Res> get error {
    if (_value.error == null) {
      return null;
    }
    return $NetworkExceptionsCopyWith<$Res>(_value.error, (value) {
      return _then(_value.copyWith(error: value));
    });
  }
}

class _$ErrorChangePassword<T> implements ErrorChangePassword<T> {
  const _$ErrorChangePassword({@required this.error}) : assert(error != null);

  @override
  final NetworkExceptions error;

  @override
  String toString() {
    return 'AuthenticationState<$T>.errorChangePassword(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ErrorChangePassword<T> &&
            (identical(other.error, error) ||
                const DeepCollectionEquality().equals(other.error, error)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(error);

  @override
  $ErrorChangePasswordCopyWith<T, ErrorChangePassword<T>> get copyWith =>
      _$ErrorChangePasswordCopyWithImpl<T, ErrorChangePassword<T>>(
          this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result idle(),
    @required Result loginLoading(),
    @required Result loginHashData(T data),
    @required Result errorLogin(NetworkExceptions error),
    @required Result googleLoading(),
    @required Result googleHashData(T data),
    @required Result errorGoogle(NetworkExceptions error),
    @required Result facebookLoading(),
    @required Result facebookHashData(T data),
    @required Result errorFacebook(NetworkExceptions error),
    @required Result registerLoading(),
    @required Result registerHashData(T data),
    @required Result errorRegist(NetworkExceptions error),
    @required Result otpLoading(),
    @required Result otpHashData(T data),
    @required Result resendOtpHashData(T data),
    @required Result sendOtpHashData(T data),
    @required Result errorOtp(NetworkExceptions error),
    @required Result errorResendOtp(NetworkExceptions error),
    @required Result errorSendOtp(NetworkExceptions error),
    @required Result forgotPassLoading(),
    @required Result forgotPassHashData(T data),
    @required Result errorForgotPass(NetworkExceptions error),
    @required Result changePasswordHashData(T data),
    @required Result errorChangePassword(NetworkExceptions error),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return errorChangePassword(error);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result idle(),
    Result loginLoading(),
    Result loginHashData(T data),
    Result errorLogin(NetworkExceptions error),
    Result googleLoading(),
    Result googleHashData(T data),
    Result errorGoogle(NetworkExceptions error),
    Result facebookLoading(),
    Result facebookHashData(T data),
    Result errorFacebook(NetworkExceptions error),
    Result registerLoading(),
    Result registerHashData(T data),
    Result errorRegist(NetworkExceptions error),
    Result otpLoading(),
    Result otpHashData(T data),
    Result resendOtpHashData(T data),
    Result sendOtpHashData(T data),
    Result errorOtp(NetworkExceptions error),
    Result errorResendOtp(NetworkExceptions error),
    Result errorSendOtp(NetworkExceptions error),
    Result forgotPassLoading(),
    Result forgotPassHashData(T data),
    Result errorForgotPass(NetworkExceptions error),
    Result changePasswordHashData(T data),
    Result errorChangePassword(NetworkExceptions error),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (errorChangePassword != null) {
      return errorChangePassword(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result idle(Idle<T> value),
    @required Result loginLoading(LoginLoading<T> value),
    @required Result loginHashData(LoginHashData<T> value),
    @required Result errorLogin(ErrorLogin<T> value),
    @required Result googleLoading(GoogleLoading<T> value),
    @required Result googleHashData(GoogleHashData<T> value),
    @required Result errorGoogle(ErrorGoogle<T> value),
    @required Result facebookLoading(FacebookLoading<T> value),
    @required Result facebookHashData(FacebookHashData<T> value),
    @required Result errorFacebook(ErrorFacebook<T> value),
    @required Result registerLoading(RegisterLoading<T> value),
    @required Result registerHashData(RegisterHashData<T> value),
    @required Result errorRegist(ErrorRegist<T> value),
    @required Result otpLoading(OtpLoading<T> value),
    @required Result otpHashData(OtpHashData<T> value),
    @required Result resendOtpHashData(ResendOtpHashData<T> value),
    @required Result sendOtpHashData(SendOtpHashData<T> value),
    @required Result errorOtp(ErrorOtp<T> value),
    @required Result errorResendOtp(ErrorResendOtp<T> value),
    @required Result errorSendOtp(ErrorSendOtp<T> value),
    @required Result forgotPassLoading(ForgotPassLoading<T> value),
    @required Result forgotPassHashData(ForgotPassHashData<T> value),
    @required Result errorForgotPass(ErrorForgotPass<T> value),
    @required Result changePasswordHashData(ChangePasswordHashData<T> value),
    @required Result errorChangePassword(ErrorChangePassword<T> value),
  }) {
    assert(idle != null);
    assert(loginLoading != null);
    assert(loginHashData != null);
    assert(errorLogin != null);
    assert(googleLoading != null);
    assert(googleHashData != null);
    assert(errorGoogle != null);
    assert(facebookLoading != null);
    assert(facebookHashData != null);
    assert(errorFacebook != null);
    assert(registerLoading != null);
    assert(registerHashData != null);
    assert(errorRegist != null);
    assert(otpLoading != null);
    assert(otpHashData != null);
    assert(resendOtpHashData != null);
    assert(sendOtpHashData != null);
    assert(errorOtp != null);
    assert(errorResendOtp != null);
    assert(errorSendOtp != null);
    assert(forgotPassLoading != null);
    assert(forgotPassHashData != null);
    assert(errorForgotPass != null);
    assert(changePasswordHashData != null);
    assert(errorChangePassword != null);
    return errorChangePassword(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result idle(Idle<T> value),
    Result loginLoading(LoginLoading<T> value),
    Result loginHashData(LoginHashData<T> value),
    Result errorLogin(ErrorLogin<T> value),
    Result googleLoading(GoogleLoading<T> value),
    Result googleHashData(GoogleHashData<T> value),
    Result errorGoogle(ErrorGoogle<T> value),
    Result facebookLoading(FacebookLoading<T> value),
    Result facebookHashData(FacebookHashData<T> value),
    Result errorFacebook(ErrorFacebook<T> value),
    Result registerLoading(RegisterLoading<T> value),
    Result registerHashData(RegisterHashData<T> value),
    Result errorRegist(ErrorRegist<T> value),
    Result otpLoading(OtpLoading<T> value),
    Result otpHashData(OtpHashData<T> value),
    Result resendOtpHashData(ResendOtpHashData<T> value),
    Result sendOtpHashData(SendOtpHashData<T> value),
    Result errorOtp(ErrorOtp<T> value),
    Result errorResendOtp(ErrorResendOtp<T> value),
    Result errorSendOtp(ErrorSendOtp<T> value),
    Result forgotPassLoading(ForgotPassLoading<T> value),
    Result forgotPassHashData(ForgotPassHashData<T> value),
    Result errorForgotPass(ErrorForgotPass<T> value),
    Result changePasswordHashData(ChangePasswordHashData<T> value),
    Result errorChangePassword(ErrorChangePassword<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (errorChangePassword != null) {
      return errorChangePassword(this);
    }
    return orElse();
  }
}

abstract class ErrorChangePassword<T> implements AuthenticationState<T> {
  const factory ErrorChangePassword({@required NetworkExceptions error}) =
      _$ErrorChangePassword<T>;

  NetworkExceptions get error;
  $ErrorChangePasswordCopyWith<T, ErrorChangePassword<T>> get copyWith;
}
