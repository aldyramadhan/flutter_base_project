import 'package:core/network/network.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'authentication_state.freezed.dart';

@freezed
abstract class AuthenticationState<T> with _$AuthenticationState<T> {
  const factory AuthenticationState.idle() = Idle<T>;

  //-- login
  const factory AuthenticationState.loginLoading() = LoginLoading<T>;
  const factory AuthenticationState.loginHashData({@required T data}) = LoginHashData<T>;
  const factory AuthenticationState.errorLogin({@required NetworkExceptions error}) = ErrorLogin<T>;

  //-- login google
  const factory AuthenticationState.googleLoading() = GoogleLoading<T>;
  const factory AuthenticationState.googleHashData({@required T data}) = GoogleHashData<T>;
  const factory AuthenticationState.errorGoogle({@required NetworkExceptions error}) = ErrorGoogle<T>;

  //-- login facebook
  const factory AuthenticationState.facebookLoading() = FacebookLoading<T>;
  const factory AuthenticationState.facebookHashData({@required T data}) = FacebookHashData<T>;
  const factory AuthenticationState.errorFacebook({@required NetworkExceptions error}) = ErrorFacebook<T>;

  //-- register
  const factory AuthenticationState.registerLoading() = RegisterLoading<T>;
  const factory AuthenticationState.registerHashData({@required T data}) = RegisterHashData<T>;
  const factory AuthenticationState.errorRegist({@required NetworkExceptions error}) = ErrorRegist<T>;

  //-- otp
  const factory AuthenticationState.otpLoading() = OtpLoading<T>;
  const factory AuthenticationState.otpHashData({@required T data}) = OtpHashData<T>;
  const factory AuthenticationState.resendOtpHashData({@required T data}) = ResendOtpHashData<T>;
  const factory AuthenticationState.sendOtpHashData({@required T data}) = SendOtpHashData<T>;
  const factory AuthenticationState.errorOtp({@required NetworkExceptions error}) = ErrorOtp<T>;
  const factory AuthenticationState.errorResendOtp({@required NetworkExceptions error}) = ErrorResendOtp<T>;
  const factory AuthenticationState.errorSendOtp({@required NetworkExceptions error}) = ErrorSendOtp<T>;

  //-- forgot password
  const factory AuthenticationState.forgotPassLoading() = ForgotPassLoading<T>;
  const factory AuthenticationState.forgotPassHashData({@required T data}) = ForgotPassHashData<T>;
  const factory AuthenticationState.errorForgotPass({@required NetworkExceptions error}) = ErrorForgotPass<T>;

  //-- change password
  const factory AuthenticationState.changePasswordHashData({@required T data}) = ChangePasswordHashData<T>;
  const factory AuthenticationState.errorChangePassword({@required NetworkExceptions error}) = ErrorChangePassword<T>;
}